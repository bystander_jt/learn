package cn.lee.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author lijiangtao
 * 
 */

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Result<T> {

    private Integer code;

    private String message;

    private String error;

    private T data;

    public Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


    public static Result ok() {
        return new Result<>(200, "", null);
    }

    public static <T> Result<T> ok(T data) {
        return new Result<>(200, "", data);
    }

    public static <T> Result<T> ok(int code, T data, String message) {
        return new Result<>(code, message, data);
    }

    public static Result fail(){
        return new Result(-1, "", null);
    }

    public static <T> Result<T> fail(int code, T data, String message){
        return new Result<>(code, message, data);
    }

}
