package com.lee.distribution.grpc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParallelRequests {

    private static final int PARALLEL_REQUESTS = 5;

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(PARALLEL_REQUESTS);

        for (int i = 0; i < 100000; i++) {
            sendRequest("http://localhost:8180/");
        }

        executor.shutdown();
    }

    private static void sendRequest(String urlString) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            System.out.println(responseCode + " " + urlString);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
