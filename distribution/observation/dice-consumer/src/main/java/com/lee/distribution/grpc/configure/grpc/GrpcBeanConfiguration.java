package com.lee.distribution.grpc.configure.grpc;

import com.lee.distribution.dice.proto.DiceGrpc;
import com.lee.distribution.grpc.service.DiceConsumeService;
import com.lee.distribution.grpc.service.IDiceConsumeService;
import net.devh.boot.grpc.client.inject.GrpcClient;
import net.devh.boot.grpc.client.inject.GrpcClientBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@GrpcClientBean(clazz = DiceGrpc.DiceBlockingStub.class,
        beanName = "diceGrpcService",
        client = @GrpcClient("GLOBAL")
)
public class GrpcBeanConfiguration {


    @Bean
    public IDiceConsumeService diceConsumeService(@Autowired DiceGrpc.DiceBlockingStub diceGrpcService) {
        return  new DiceConsumeService(diceGrpcService);
    }


}
