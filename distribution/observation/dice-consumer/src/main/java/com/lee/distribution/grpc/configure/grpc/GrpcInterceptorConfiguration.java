package com.lee.distribution.grpc.configure.grpc;


import io.grpc.ClientInterceptor;
import io.grpc.ServerInterceptor;
import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.instrumentation.grpc.v1_6.GrpcTelemetry;
import net.devh.boot.grpc.client.interceptor.GrpcGlobalClientInterceptor;
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrpcInterceptorConfiguration {


    @GrpcGlobalServerInterceptor
    public ServerInterceptor openTelemetryServerInterceptor(OpenTelemetry openTelemetry) {
        return GrpcTelemetry.create(openTelemetry).newServerInterceptor();
    }


    @GrpcGlobalClientInterceptor
    public ClientInterceptor openTelemetryClientInterceptor(OpenTelemetry openTelemetry) {
        return GrpcTelemetry.create(openTelemetry).newClientInterceptor();
    }



}
