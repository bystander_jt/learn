package com.lee.distribution.grpc.service;

import com.google.protobuf.Empty;
import com.lee.distribution.dice.proto.DiceGrpc;
import io.grpc.Context;
import io.grpc.stub.StreamObserver;
import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiceConsumeService implements IDiceConsumeService {

    private static final Logger log = LoggerFactory.getLogger(DiceConsumeService.class);
    private final DiceGrpc.DiceBlockingStub diceService;

    public DiceConsumeService(DiceGrpc.DiceBlockingStub diceService) {
        this.diceService = diceService;
    }

    @Override
    @WithSpan(kind = SpanKind.CLIENT)
    public String dice() {
        log.info("try retrieve dice.");
        log.info("span id: {}, trace id: {}", Span.current().getSpanContext().getSpanId(), Span.current().getSpanContext().getTraceId());
        return diceService.random(Empty.newBuilder().build()).getNum();
    }
}
