package com.lee.distribution.grpc.controller;

import com.lee.distribution.grpc.service.IDiceConsumeService;
import io.opentelemetry.api.baggage.Baggage;
import io.opentelemetry.context.Context;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class DiceController {

    private final IDiceConsumeService consumeService;

    public DiceController(IDiceConsumeService consumeService) {
        this.consumeService = consumeService;
    }

    @GetMapping
    public String dice() {
        Baggage build = Baggage.builder().put("method", "dice").build();
        Context context = build.storeInContext(Context.current());
        context.makeCurrent();
        return consumeService.dice();
    }

}
