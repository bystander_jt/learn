package com.lee.distribution.grpc.grpc.configure;

import com.codahale.metrics.MetricRegistry;
import com.lee.distribution.grpc.grpc.interceptor.LogInterceptor;
import com.lee.distribution.grpc.grpc.interceptor.ObservationInterceptor;
import io.grpc.ClientInterceptor;
import io.grpc.ServerInterceptor;
import io.micrometer.core.instrument.MeterRegistry;
import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.instrumentation.grpc.v1_6.GrpcTelemetry;
import io.opentelemetry.sdk.metrics.SdkMeterProvider;
import net.devh.boot.grpc.client.interceptor.GrpcGlobalClientInterceptor;
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.springframework.boot.actuate.autoconfigure.tracing.OpenTelemetryTracingAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrpcInterceptorConfiguration {


    @GrpcGlobalServerInterceptor
    public ServerInterceptor loggingServerInterceptor(MeterRegistry metricRegistry) {
        return new LogInterceptor(metricRegistry);
    }

    @GrpcGlobalServerInterceptor
    public ServerInterceptor observationInterceptor(MeterRegistry metricRegistry) {
        return new ObservationInterceptor(metricRegistry);
    }

    @GrpcGlobalServerInterceptor
    public ServerInterceptor openTelemetryServerInterceptor(OpenTelemetry openTelemetry) {
        return GrpcTelemetry.create(openTelemetry).newServerInterceptor();
    }


    @GrpcGlobalClientInterceptor
    public ClientInterceptor openTelemetryClientInterceptor(OpenTelemetry openTelemetry) {
        return GrpcTelemetry.create(openTelemetry).newClientInterceptor();
    }

}
