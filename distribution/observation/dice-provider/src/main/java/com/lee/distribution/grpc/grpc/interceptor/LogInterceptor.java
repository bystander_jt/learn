package com.lee.distribution.grpc.grpc.interceptor;

import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.opentelemetry.api.trace.Span;
import jakarta.annotation.Priority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Priority(Integer.MAX_VALUE)
public class LogInterceptor implements ServerInterceptor {

    private final MeterRegistry meterRegistry;

    private static final Logger log = LoggerFactory.getLogger(LogInterceptor.class);

    public LogInterceptor(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        log.info("called: {}", serverCall.getMethodDescriptor().getBareMethodName());
        return serverCallHandler.startCall(serverCall, metadata);
    }
}
