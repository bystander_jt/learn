package com.lee.distribution.grpc.service;

import com.google.protobuf.Empty;
import com.lee.distribution.dice.proto.DiceGrpc;
import com.lee.distribution.dice.proto.RandomResp;
import io.grpc.stub.StreamObserver;
import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.baggage.Baggage;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanKind;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import net.devh.boot.grpc.server.service.GrpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;


@GrpcService
public class GreeterService extends DiceGrpc.DiceImplBase {

    private static final Logger log = LoggerFactory.getLogger(GreeterService.class);

    @Override
    @WithSpan(kind = SpanKind.SERVER)
    public void random(Empty request, StreamObserver<RandomResp> responseObserver) {
        String random = UUID.randomUUID().toString();

        log.info("span id: {}, trace id: {}", Span.current().getSpanContext().getSpanId(), Span.current().getSpanContext().getTraceId());
        log.info("generate random dice: {}", random);

        log.info("baggage: {}", Baggage.current().asMap());

        RandomResp resp = RandomResp.newBuilder().setNum(random).buildPartial();
        responseObserver.onNext(resp);
        responseObserver.onCompleted();
    }
}
