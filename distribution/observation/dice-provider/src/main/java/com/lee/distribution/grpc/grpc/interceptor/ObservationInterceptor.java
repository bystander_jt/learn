package com.lee.distribution.grpc.grpc.interceptor;

import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObservationInterceptor implements ServerInterceptor {

    private final MeterRegistry meterRegistry;

    private static final Logger log = LoggerFactory.getLogger(LogInterceptor.class);

    public ObservationInterceptor(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        Timer timer = Timer.builder("grpc.service")
                .tag("tier", "backend")
                .tag("method", serverCall.getMethodDescriptor().getFullMethodName())
                .description("grpc service call time duration.")
                .register(meterRegistry);
        Timer.Sample start = Timer.start();
        try {
            return serverCallHandler.startCall(serverCall, metadata);
        } finally {
            start.stop(timer);
        }
    }
}
