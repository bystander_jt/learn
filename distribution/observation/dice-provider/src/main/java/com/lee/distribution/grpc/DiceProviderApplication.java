package com.lee.distribution.grpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class DiceProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiceProviderApplication.class, args);
    }

}
