package com.lee.distribution.observation;

import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

public class RateAggregationExample {


    public static void main(String[] args) {
        // 创建一个 MeterRegistry，通常在 Spring Boot 中它是自动配置的
        MeterRegistry meterRegistry = new SimpleMeterRegistry();

        meterRegistry.timer("http.request", Tags.of("http", "request"));
    }

}
