package com.lee.distribution.observation;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import java.net.Proxy;
import java.util.concurrent.TimeUnit;

public class TimerExample {

    public static void main(String[] args) {
        new TimerExample().timer();
    }

    void timer() {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
        Timer timer = Timer.builder("http.request.timer")
                .register(registry);

        for (int i = 0; i < 5; i++) {

            int finalI = i;
            timer.record(() -> {
                System.out.println("start timer");
                try {
                    Thread.sleep(finalI * 100);
                } catch (InterruptedException e) {

                }
            });
        }
//Timer.builder().publishPercentiles()

        System.out.println("total time: " + timer.totalTime(TimeUnit.MILLISECONDS));
        System.out.println("max time: " + timer.max(TimeUnit.MILLISECONDS));
        System.out.println("total task: " + timer.count());
    }


    /**
     * sample 是先创建 Timer 并立即开始计时.
     *
     * 在 {@code stop()} 时绑定 Timer 的 tag 和 name 等信息.
     */
    void sample() {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
        Timer.Sample sample = Timer.start(registry);

        // do sth

        sample.stop(Timer.builder("xxxxx").tags("xxxx", "xxxx").register(registry));
        sample.stop(registry.timer("xxxxx", "http", "response", "status", "200"));

    }

    void more() {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
//        registry.more().timer("http-timer", Tags.of(Tag.of("a", "b"), Tag.of("foo", "bar")))
    }

}
