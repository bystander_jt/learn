package com.lee.distribution.observation;

import io.micrometer.core.instrument.*;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import java.util.*;

public class GaugeExample {

    public static void main(String[] args) {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();

        new GaugeExample().gauge(registry);
    }

    void gauge(MeterRegistry registry) {

        List<String> cache = new ArrayList<>(10);
        // 不用担心强引用导致 cache 无法回收. 其底层会使用 WeakReference 包裹.
        // 如果手动声明了 {@code strongReference(true)} 则保留强引用.
        Gauge gauge = Gauge.builder("xxx.cache.size", cache, List::size)
                .tags("dev", "cache")
                .strongReference(false)
                .description("cache size gauge")
                .register(registry);

        System.out.println("current cache size: " + gauge.value());

        for (int i = 0; i < 10; i++) {
            cache.add(new String(UUID.randomUUID().toString()));
        }

        System.out.println("current cache size: " + gauge.value());

    }

    void multiGauge(MeterRegistry registry) {

        MultiGauge register = MultiGauge.builder("multi.gauge")
                .tag("gauge.multi", "cache")
                .description("multi gauges")
                .register(registry);

        Map<String, String> cache = new HashMap<>();

        register.register(
                    cache.entrySet().stream().map(Map.Entry::getKey).map(d -> MultiGauge.Row.of(Tags.of("cache.key", d), 0)).toList()
        );

    }

}
