package com.lee.distribution.observation;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Random;

public class CounterExample {

    void counter() {
        Random rand = new Random();
        MeterRegistry registry = new SimpleMeterRegistry();
        Counter counter = registry.counter("counter");
        int loopSize = rand.nextInt(100) + 4;
        System.out.println("loopSize: " + loopSize);
        for (int i = 0; i < loopSize; i++) {
            int i1 = rand.nextInt();
            counter.increment();
        }
        System.out.println("counter: " + counter.count());
        System.out.println("measurement: " + counter.measure());
    }

    void counterWithBuilder() {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
        Counter counter = Counter.builder("http.request")
                .baseUnit("QPS")
                .tags(Tags.of("http", "request"))
                .description("每秒HTTP请求量")
                // register 则是将当前 tag 进行过滤, 过滤后提供一个 Counter.
//                .register(registry)
                // withRegistry 可以额外增加 TAG, 且该 TAG 不会被 TagFilter 过滤.
                .withRegistry(registry)
                .withTag("http", "request");


    }
}
