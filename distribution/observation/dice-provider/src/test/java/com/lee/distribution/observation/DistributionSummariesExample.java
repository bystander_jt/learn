package com.lee.distribution.observation;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import java.util.concurrent.TimeUnit;


public class DistributionSummariesExample {

    public static void main(String[] args) {
        // 创建一个 MeterRegistry，通常在 Spring Boot 中它是自动配置的
        MeterRegistry meterRegistry = new SimpleMeterRegistry();

        // 创建一个 DistributionSummary 用于监控 HTTP 请求体积（字节数）
        DistributionSummary requestSizeSummary = meterRegistry.summary("http.requests.size");

        // 创建一个 Counter 用于监控 HTTP 请求的发生频率（事件速率）
        Counter requestCounter = meterRegistry.counter("http.requests.count");

        // 模拟记录请求
        simulateHttpRequests(requestSizeSummary, requestCounter);

        // 休眠 1 秒钟，模拟请求发生的过程
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 输出聚合的结果
        System.out.println("Total requests count: " + requestCounter.count());
        System.out.println("Total request size: " + requestSizeSummary.totalAmount());
        System.out.println("Total request : " + requestSizeSummary.count());
        System.out.println("Max request size: " + requestSizeSummary.max());
        System.out.println("Average: " + requestSizeSummary.mean());
    }

    // 模拟 HTTP 请求，并记录请求体积和请求频率
    private static void simulateHttpRequests(DistributionSummary requestSizeSummary, Counter requestCounter) {
        for (int i = 0; i < 100; i++) {
            // 假设每次请求体积随机，模拟 100 到 1000 字节的请求体积
            double requestSize = 100 + Math.random() * 900;
            // 记录请求体积
            requestSizeSummary.record(requestSize);

            // 记录每次请求发生
            requestCounter.increment();
        }
    }

}
