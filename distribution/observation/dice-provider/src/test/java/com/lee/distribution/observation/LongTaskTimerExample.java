package com.lee.distribution.observation;

import io.micrometer.core.instrument.LongTaskTimer;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import java.util.ArrayList;
import java.util.List;

public class LongTaskTimerExample {

    public static void main(String[] args) throws InterruptedException {

        MeterRegistry registry = new SimpleMeterRegistry();

        LongTaskTimerExample example = new LongTaskTimerExample();
        example.longTimerExample(registry);
    }


    void longTimerExample(MeterRegistry registry) throws InterruptedException {

        // 获取或创建一个 LongTaskTimer 实例
        LongTaskTimer longTaskTimer = LongTaskTimer.builder("longTask")
                .description("A long running task timer")
                .register(registry == null ? Metrics.globalRegistry : registry);

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(() -> {
                // 启动一个长时间任务
                LongTaskTimer.Sample sample = longTaskTimer.start();

                try {
                    // 模拟一个耗时任务
                    Thread.sleep(5000); // 模拟长时间运行的任务
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    // 结束任务并记录持续时间
                    sample.stop();
                }
            });
            threads.add(thread);
            thread.start();

            // 查看当前活动任务的数量
            System.out.println("Active tasks: " + longTaskTimer.activeTasks());
        }

        for (Thread thread : threads) {
            if (thread.isAlive()) {
                // 查看当前活动任务的数量
                System.out.println("Active tasks: " + longTaskTimer.activeTasks());
            }
        }

        for (Thread thread : threads) {
            thread.join();
        }


        // 查看当前活动任务的数量
        System.out.println("Active tasks: " + longTaskTimer.activeTasks());


    }

}
