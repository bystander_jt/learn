package cn.lee.xueqi.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * @since VERSION
 */

@Getter
@Setter
@ToString
public class WebResponseDTO implements Serializable {

    private Data data;

    @Getter
    @Setter
    @ToString
    static public class Data {
        private List<QuestionAnswerDTO> questions;
    }


}
