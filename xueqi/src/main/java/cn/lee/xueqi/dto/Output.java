package cn.lee.xueqi.dto;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * 
 * 
 * @since VERSION
 */

public class Output {

    public static void main(String[] args) {
        Output output = new Output();
        String fileContent = output.readFile();
        WebResponseDTO webResponseDTO = JSON.parseObject(fileContent, WebResponseDTO.class);
        List<QuestionAnswerDTO> questions = webResponseDTO.getData().getQuestions();
        for (QuestionAnswerDTO question : questions) {
            System.out.println("============================");
            String answer = output.getAnswer(question);
            System.out.println("Q:" + question.getQuestion() + ". \nA: " + answer);
        }

    }

    private String getAnswer(QuestionAnswerDTO answerDTO) {

        String answerId = answerDTO.getAnswer().getId();
        QuestionAnswerDTO.AnswerArea area = answerDTO.getAnswerArea().get(0);
        QuestionAnswerDTO.AnswerArea.AnswerList result = area.getOptionList().stream().filter(d -> d.getId().equals(answerId)).findFirst().orElse(null);
        if (result == null) return "结果未知";
        return result.getContent().replace("<span>", "")
                .replace("</span>", "");
    }

    private String readFile() {
        StringBuilder result = new StringBuilder();
        try {
            File file = new File("C:\\Users\\Bystander\\Desktop\\file");
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String tmp = null;

            while ((tmp = bufferedReader.readLine()) != null) {
                result.append(tmp);
            }
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
