package cn.lee.xueqi.dto;


import com.alibaba.fastjson.annotation.JSONField;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * @since VERSION
 */

@Getter
@Setter
@ToString
public class QuestionAnswerDTO implements Serializable {

    @JSONField(name = "stem")
    @SerializedName(value = "stem")
    private String question;

    /**
     * 选择内容
     */
    private List<AnswerArea> answerArea;

    /**
     * 答案
     */
    private Answer answer;


    @Getter
    @Setter
    @ToString
    static public class AnswerArea {
        private List<AnswerList> optionList;

        @Getter
        @Setter
        @ToString
        static public class AnswerList {
            /**
             * 当前问题ID
             */
            private String id;

            /**
             * 选择项
             */
            private String content;
        }
    }

    @Getter
    @Setter
    @ToString
    static public class Answer {
        private String id;
    }

}
