#!/bin/bash


function replace_yaml(){
    curl -s -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
    sed -i -e '/mirrors.cloud.aliyuncs.com/d' -e '/mirrors.aliyuncs.com/d' /etc/yum.repos.d/CentOS-Base.repo
    yum clean all > /dev/null
    yum makecache > /dev/null
}

function install_docker(){

    yum install -y -q yum-utils
    yum-config-manager -q --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
    yum clean all && yum makecache

    sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y -q

    sudo mkdir -p /etc/docker
    sudo tee /etc/docker/daemon.json <<-'EOF'
{
    "registry-mirrors": ["https://lcteppb4.mirror.aliyuncs.com"],
    "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF

    systemctl enable docker
    sudo systemctl daemon-reload
    sudo systemctl restart docker

}

function cri_dockerd_install(){
    git clone https://github.com/Mirantis/cri-dockerd.git
    wget https://storage.googleapis.com/golang/getgo/installer_linux
    chmod +x ./installer_linux
    ./installer_linux
    source ~/.bash_profile
    go get && go build -o bin/cri-dockerd
    mkdir -p /usr/local/bin
    install -o root -g root -m 0755 bin/cri-dockerd /usr/local/bin/cri-dockerd
    cp -a packaging/systemd/* /etc/systemd/system
    sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
    systemctl daemon-reload
    systemctl enable cri-docker.service
    systemctl enable --now cri-docker.socket
    systemctl start cri-docker.service

}

function install_k8s(){

    cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
    setenforce 0
    yum install -y kubelet kubeadm kubectl --nogpgcheck
    systemctl enable kubelet && systemctl start kubelet
    swapoff -a
     firewall-cmd --add-port=6443/tcp --permanent
     firewall-cmd --add-port=10250/tcp --permanent

}

function k8s_check(){

    systemctl disable firewalld
    systemctl stop firewalld
    

    modprobe br_netfilter
    echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables
    echo 1 > /proc/sys/net/ipv4/ip_forward
}

function k8s_init(){
    kubeadm init --image-repository=registry.aliyuncs.com/google_containers
}

function downloadImage() {



}

replace_yaml

