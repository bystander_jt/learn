package com.lee.netty.leak_detection;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.ResourceLeakDetector;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NettyServerBootstrap {

    public static void main(String[] args) throws InterruptedException {

        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.PARANOID);

        NioEventLoopGroup listenerGroup = new NioEventLoopGroup(1);
        NioEventLoopGroup processorGroup = new NioEventLoopGroup();

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(listenerGroup, processorGroup)
                .localAddress(801)
                .channel(NioServerSocketChannel.class)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline()
                                .addLast(new CChannelHandler())
                                .addLast(new DChannelHandler());
                    }
                }).bind().sync();

    }

}
