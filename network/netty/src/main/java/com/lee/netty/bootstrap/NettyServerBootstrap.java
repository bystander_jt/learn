package com.lee.netty.bootstrap;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import io.netty.handler.traffic.GlobalChannelTrafficCounter;
import io.netty.handler.traffic.GlobalChannelTrafficShapingHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NettyServerBootstrap {

    public static void main(String[] args) throws InterruptedException {

        NioEventLoopGroup listenerGroup = new NioEventLoopGroup(1);
        NioEventLoopGroup processorGroup = new NioEventLoopGroup();

        ChannelTrafficShapingHandler handler = new ChannelTrafficShapingHandler(10, 10, 10);

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(listenerGroup, processorGroup)
                .localAddress(801)
                .channel(NioServerSocketChannel.class)
                .handler(handler)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(handler);
                    }
                }).bind().sync();

    }

}
