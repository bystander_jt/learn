package com.lee.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelProgressiveFuture;
import io.netty.channel.ChannelProgressiveFutureListener;
import io.netty.channel.group.ChannelGroupFuture;
import io.netty.channel.group.ChannelGroupFutureListener;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

@Slf4j
public class ManualClientSocket {

    public static void main(String[] args) {
        ManualClientSocket client = new ManualClientSocket();
        new Thread(client::client).start();
    }

    public void nettyClient(){
        Bootstrap bootstrap = new Bootstrap();
        ChannelFuture localhost = bootstrap.connect(new InetSocketAddress("localhost", 801));

//        localhost.addListener();
        localhost.removeListeners();
        localhost.addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future) throws Exception {

            }
        });

        localhost.addListener(new ChannelProgressiveFutureListener() {
            @Override
            public void operationProgressed(ChannelProgressiveFuture channelProgressiveFuture, long l, long l1) throws Exception {

            }

            @Override
            public void operationComplete(ChannelProgressiveFuture channelProgressiveFuture) throws Exception {

            }
        });

        localhost.addListener(new ChannelGroupFutureListener() {
            @Override
            public void operationComplete(ChannelGroupFuture future) throws Exception {

            }
        });

        localhost.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

            }
        });
    }

    public void client() {
        try (SocketChannel socket = SocketChannel.open()) {
            socket.connect(new InetSocketAddress("localhost", 801));
            log.info("client connect");
            Scanner scanner = new Scanner(System.in);

            ByteBuffer readBuffer = ByteBuffer.allocateDirect(1024);
            while (socket.isConnected()) {
                String inputs = scanner.nextLine();
                socket.write(StandardCharsets.UTF_8.encode(inputs));
                log.info("client send data: {}", inputs);
            }
            log.info("client closed");
        } catch (IOException e) {
            throw new RuntimeException(e);

        }
    }
}
