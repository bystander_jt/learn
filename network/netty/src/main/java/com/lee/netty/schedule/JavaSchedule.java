package com.lee.netty.schedule;

import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
public class JavaSchedule {

    public static void main(String[] args) throws InterruptedException {

        ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(5);

        threadPool.schedule(() ->
                        log.info("java scheduled delay 60 seconds executor {}", System.currentTimeMillis()),
                60,
                TimeUnit.SECONDS);

        threadPool.scheduleAtFixedRate(() ->
                        log.info("java scheduled fixed rate 10 seconds executor {}", System.currentTimeMillis()),
                0, 10, TimeUnit.SECONDS);

        threadPool.scheduleWithFixedDelay(() ->
                log.info("java scheduled fixed delay 10 seconds executor {}", System.currentTimeMillis()), 0, 10, TimeUnit.SECONDS
                );

        Thread.sleep(Duration.ofMillis(3).toMillis());


        threadPool.shutdown();
    }

}
