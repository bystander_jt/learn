package com.lee.netty.schedule;

import io.netty.channel.DefaultEventLoopGroup;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Slf4j
public class EventLoopScheduled {

    public static void main(String[] args) throws InterruptedException {

        DefaultEventLoopGroup executors = new DefaultEventLoopGroup(2);

        executors.schedule(() ->
                        log.info("netty event loop delay 60 seconds executor {}", System.currentTimeMillis()),
                60,
                TimeUnit.SECONDS);

        executors.scheduleAtFixedRate(() ->
                        log.info("netty event loop fixed rate 10 seconds executor {}", System.currentTimeMillis()),
                0, 10, TimeUnit.SECONDS);

        executors.scheduleWithFixedDelay(() ->
                log.info("netty event loop fixed delay 10 seconds executor {}", System.currentTimeMillis()), 0, 10, TimeUnit.SECONDS
        );

        Thread.sleep((long) Duration.ofSeconds(3).getSeconds());


    }


}
