package com.lee.netty.writer_and_flush;

import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/8/16 16:12
 * @since VERSION
 */

@Slf4j
public class CustomDuplexHandler extends ChannelDuplexHandler {

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        ByteBufAllocator channelAlloc = ctx.channel().alloc();
        channelAlloc.buffer();

        ByteBufAllocator ctxAlloc = ctx.alloc();
        ctxAlloc.buffer();


        log.warn("custom duplex handler write method invoke");
        super.write(ctx, msg, promise);
    }

    @Override
    public void flush(ChannelHandlerContext ctx) throws Exception {
        log.warn("custom duplex handler flush method invoke");
        super.flush(ctx);
    }
}
