package com.lee.netty.reference;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/8/17 13:33
 * @since VERSION
 */

@Slf4j
@ChannelHandler.Sharable
public class IncreaseReferenceCount extends SimpleChannelInboundHandler<ByteBuf> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        // 试试看把下方这个代码注释掉, 然后再执行看看结果
//        msg.retain();
        log.info("increase reference count 1");
        ctx.fireChannelRead(msg);
    }
}
