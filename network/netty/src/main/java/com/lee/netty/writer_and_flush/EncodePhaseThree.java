package com.lee.netty.writer_and_flush;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@ChannelHandler.Sharable
public class EncodePhaseThree extends ChannelOutboundHandlerAdapter {

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        log.warn("Encode Phase Two invoke");

        ByteBuf writeData = (ByteBuf) msg;
        writeData.writeBytes( CharsetUtil.UTF_8.encode("3"));
        ctx.write(msg, promise);
    }
}
