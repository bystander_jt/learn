package com.lee.netty.fire_event;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.kqueue.KQueueEventLoopGroup;
import io.netty.channel.kqueue.KQueueServerSocketChannel;
import io.netty.channel.socket.SocketChannel;

import java.util.concurrent.locks.LockSupport;

public class FireEvent {

    public static void main(String[] args) throws InterruptedException {
        ServerBootstrap bootstrap = new ServerBootstrap();
        ChannelFuture future = bootstrap.channel(KQueueServerSocketChannel.class)
                .group(new KQueueEventLoopGroup(1), new KQueueEventLoopGroup(Runtime.getRuntime().availableProcessors() << 1))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast(new FireEventLoggingHandler());
                    }
                }).bind(8888).sync();


        LockSupport.park();


    }
}
