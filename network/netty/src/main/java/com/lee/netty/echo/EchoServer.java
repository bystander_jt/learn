package com.lee.netty.echo;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.DefaultThreadFactory;

import java.net.InetSocketAddress;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/8/15 14:28
 * @since VERSION
 */

public class EchoServer {
    private final int port;

    public EchoServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) throws Exception {
        int port = 801;
        new EchoServer(port).start();
    }

    public void start() throws Exception {
        final EchoServerHandler serverHandler = new EchoServerHandler();
        final EchoServerHandler2 serverHandler2 = new EchoServerHandler2();
        final EchoServerSimpleHandler simpleHandler = new EchoServerSimpleHandler();
        EventLoopGroup bossGroup = new NioEventLoopGroup(1, new DefaultThreadFactory("BOSS_GROUP"));
        EventLoopGroup workerGroup = new NioEventLoopGroup(1, new DefaultThreadFactory("WORKERS_GROUP"));

        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(new InetSocketAddress(port))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addFirst(new LoggingHandler(LogLevel.DEBUG))
                                    .addLast(new ChannelInboundHandlerAdapter() {
                                        @Override
                                        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                            ctx.channel().attr(AttributeKey.newInstance("code")).set("AAAA");
                                            ByteBuf buf = (ByteBuf) msg;
                                            byte b1 = buf.readByte();
                                            ctx.writeAndFlush(buf);
                                        }
                                    });
//                                    .addLast(serverHandler)
//                                    .addLast(simpleHandler)
//                                    .addLast(serverHandler2)
                        }
                    });

            ChannelFuture f = b.bind().sync();
            f.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully().sync();
        }
    }
}
