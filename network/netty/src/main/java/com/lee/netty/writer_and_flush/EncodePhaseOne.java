package com.lee.netty.writer_and_flush;

import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@ChannelHandler.Sharable
public class EncodePhaseOne extends ChannelOutboundHandlerAdapter {

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        log.warn("Encode Phase One invoke");
        ByteBuf writeData = (ByteBuf) msg;
        writeData.writeBytes(CharsetUtil.UTF_8.encode("1"));
        ctx.write(msg, promise);
    }
}
