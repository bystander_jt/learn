package com.lee.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.nio.NioEventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/8/15 13:10
 * @since VERSION
 */

public class NettyCoreTest {

    public static void main(String[] args) {

        NioEventLoopGroup eventPool = new NioEventLoopGroup(1);
        NioSocketChannel channel = new NioSocketChannel();

        eventPool.register(channel);

        ChannelFuture channelFuture = channel.connect(new InetSocketAddress("localhost", 801));
        channelFuture.addListeners(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                if (channelFuture.isSuccess()) {
                    ByteBuf buffer = Unpooled.copiedBuffer("Hello", Charset.defaultCharset());
                    ChannelFuture wf = channelFuture.channel().writeAndFlush(buffer);
                } else {
                    Throwable cause = channelFuture.cause();
                    cause.printStackTrace();
                }
            }
        });
    }
}
