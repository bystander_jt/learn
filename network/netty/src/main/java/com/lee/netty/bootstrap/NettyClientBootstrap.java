package com.lee.netty.bootstrap;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

@Slf4j
public class NettyClientBootstrap {

    public static void main(String[] args) throws InterruptedException {
        Bootstrap client = new Bootstrap();
        client.group(new NioEventLoopGroup(1))
                .channel(NioSocketChannel.class)
                .remoteAddress(new InetSocketAddress("localhost", 801))
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {

                    }
                })
                .connect().sync();
    }
}
