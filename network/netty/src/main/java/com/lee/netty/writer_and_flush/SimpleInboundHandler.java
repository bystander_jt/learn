package com.lee.netty.writer_and_flush;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/8/15 14:29
 * @since VERSION
 */
@ChannelHandler.Sharable
public class SimpleInboundHandler extends SimpleChannelInboundHandler<ByteBuf> {


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        ByteBuf buffer1 = Unpooled.buffer(10, 50);
        ByteBuf buffer2 = Unpooled.buffer(10, 50);

        buffer1.writeBytes("TX".getBytes());
        buffer2.writeBytes("TX".getBytes());

        // 从当前 handler 直接返回, 跳过后续 handler
        ctx.writeAndFlush(buffer1);

        // 从 tail handler 执行写入操作.
        ctx.channel().writeAndFlush(buffer2);
//        ctx.flush();
    }
}
