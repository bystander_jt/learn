# ChannelInbound 连接顺序

```mermaid
flowchart TB

REGISTERED --> ACTIVE 
```

# ChannelOutBound 连接被动中断顺序

```mermaid
flowchart TB

INACTIVE --> UNREGISTERED
```

# ChannelOutBound 主动关闭连接顺序

```mermaid
flowchart TB
CLOSE --> INACTIVE --> UNREGISTERED
```