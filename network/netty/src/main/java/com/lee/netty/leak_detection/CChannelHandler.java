package com.lee.netty.leak_detection;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/8/17 16:56
 * @since VERSION
 */

public class CChannelHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        ByteBuf byteBuf = ctx.alloc().directBuffer(10);
        byteBuf.writeByte(10);
        byteBuf.retain();


        ctx.fireChannelRead(msg);
    }
}
