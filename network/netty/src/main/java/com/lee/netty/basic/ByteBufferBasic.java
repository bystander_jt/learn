package com.lee.netty.basic;

import lombok.extern.slf4j.Slf4j;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;


@Slf4j
public class ByteBufferBasic {

    public static void main(String[] args) throws IllegalAccessException {
        ByteBuffer buf = ByteBuffer.allocate(100);
        write(buf);

        read((ByteBuffer) buf.flip());

        buf.compact();
        log(buf);
    }

    /**
     * 写入数据, 如果 {@code buf} 已经被 flip(), 则会从 index = 0 开始对数据进行覆盖式写入.
     *
     * @param buf
     * @throws IllegalAccessException 无法对 readOnly 数据做写入操作.
     */
    public static void write(ByteBuffer buf) throws IllegalAccessException {
        if (buf.isReadOnly()) throw new IllegalAccessException();
        // 写入元素
        buf.put("abcdefg".getBytes(StandardCharsets.UTF_8));
        log(buf);
    }

    /**
     * 读取当前 buf 数据
     *
     * @param buf
     */
    public static void read(ByteBuffer buf) {
        log(buf);

        int length = buf.remaining();
        byte[] bytes = new byte[length];
        buf.get(bytes);

        log.info("read : {}", new String(bytes));
        log(buf);

    }

    public static void log(ByteBuffer buf) {
        log.info("position: {}, capacity: {}, limit: {}", buf.position(), buf.capacity(), buf.limit());
    }


}
