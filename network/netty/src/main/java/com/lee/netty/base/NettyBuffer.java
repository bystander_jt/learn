package com.lee.netty.base;

import io.netty.buffer.*;
import io.netty.channel.ChannelOutboundBuffer;
import io.netty.handler.codec.ByteToMessageCodec;
import io.netty.util.ReferenceCountUtil;
import io.netty.util.ReferenceCounted;
import lombok.extern.slf4j.Slf4j;

/**
 * @link  <a href=https://www.notion.so/leejt/ByteBuf-ByteBuffer-ab02882f90db4708b77571add7c84f31>文档</a>
 */
@Slf4j
public class NettyBuffer {

    public static void main(String[] args) {
        ByteBuf directBuffer = Unpooled.directBuffer(20);
        directBuffer.writeBytes("1234567890".getBytes());

        log.info("original buffer");
        print(directBuffer);

        copy(directBuffer);

        print(directBuffer);

    }

    /**
     * 浅复制 buffer, 延用原始 buf 内存地址, 自身维护一套 readerIndex 和 writerIndex 指针.</br>
     * 在原始 buffer 更改指针, 复制 buffer 不受影响.</br>
     * 原始 buffer 修改数据时, 复制 buffer 采用的浅拷贝内存地址, 所以也会受影响.
     *
     * @param buf
     */
    public static void duplicate(ByteBuf buf) {
        ByteBuf duplicate = buf.duplicate();
        log.info("duplicate buffer");


        duplicate.writeByte(0xFF);
        duplicate.writerIndex(0);
        duplicate.writeByte(0xF0);

        buf.writerIndex(buf.writerIndex() - 1);
        buf.writeByte(0xF0);
        print(duplicate);
    }

    public static void slice(ByteBuf buf) {
        ByteBuf slice = buf.retainedSlice(buf.readerIndex(), 3);
        slice.writerIndex(0);
        slice.writeByte(0x0F);
        slice.retain(3);
        print(slice);
    }

    public static void unmodified(ByteBuf buf){
        ByteBuf unmodifiableBuffer = Unpooled.wrappedUnmodifiableBuffer(buf);

        // throw ReadOnlyBufferException
        unmodifiableBuffer.writeByte(1);


    }

    public static void copy(ByteBuf buf){
        ByteBuf copy = buf.copy();

        ByteBuf byteBuf = Unpooled.wrappedUnmodifiableBuffer(buf);

        ByteBuf copy1 = byteBuf.copy();
        print(copy1);
    }

    public static void createByteBuf(){

    }

    public static void print(ByteBuf buf) {
        ReferenceCounted count = buf;
        String indexInfo = String.format("readerIndex: %d, writerIndex: %d, capacity: %d, refCnt: %d", buf.readerIndex(), buf.writerIndex(), buf.capacity(), count.refCnt());
        System.out.println(indexInfo);
        System.out.println(ByteBufUtil.prettyHexDump(buf));
        System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
    }

    public void compatibleType() {
        CompositeByteBuf byteBufs = Unpooled.compositeBuffer();
    }


}
