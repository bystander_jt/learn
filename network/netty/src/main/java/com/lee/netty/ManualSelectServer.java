package com.lee.netty;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;

@Slf4j
public class ManualSelectServer {

    public static void main(String[] args) throws IOException {
        ManualSelectServer server = new ManualSelectServer();
        server.select();
    }


    public void select() throws IOException {
        Selector selector = Selector.open();

        // 数据接受缓冲区
        ByteBuffer buffer = ByteBuffer.allocateDirect(1024);

        // 服务器端监听 Channel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        // 设置非阻塞模式
        serverSocketChannel.configureBlocking(false);
        // 设定监听端口
        serverSocketChannel.bind(new InetSocketAddress(801));
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        //循环获取 Selector 中的事件
        while (true) {

            // 有未处理的事件会退出阻塞, 如果没有事件则会被阻塞
            selector.select();
            // 获取所有事件
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();
            // 循环事件
            while (iterator.hasNext()) {
                // 处理连接事件
                SelectionKey key = iterator.next();
                handlerConnect(selector, key);
                handlerRead(key);
                iterator.remove();
            }


        }
    }

    private void handlerConnect(Selector selector, SelectionKey key) throws IOException {
        // 如果是连接事件
        if (key.isAcceptable()) {
            // 获取服务端对应 Channel
            ServerSocketChannel channel = (ServerSocketChannel) key.channel();
            // 接受当前 Channel 请求, 并获取客户端 Channel
            SocketChannel socketChannel = channel.accept();
            // 设置非阻塞 channel
            socketChannel.configureBlocking(false);

            ByteBuffer buffer = ByteBuffer.allocateDirect(16);


            // 将客户端 channel 注册到 Selector 中
            socketChannel.register(selector, SelectionKey.OP_READ, buffer);
            log.info("[CONNECT] socket connect : {}", channel);

            // 设置这个可以对 channel 设置只接受特定的事件（连接, 接受, 读，写)
//            key.interestOps(SelectionKey.OP_READ);
        }
    }

    private void handlerRead(SelectionKey key) throws IOException {
        if (key.isReadable()) {
            SocketChannel channel = (SocketChannel) key.channel();
            try {
                ByteBuffer buffer = (ByteBuffer) key.attachment();
                int readCount = channel.read(buffer);
                if (readCount < 0){
                    SocketAddress remoteAddress = ((SocketChannel) key.channel()).getRemoteAddress();
                    String address = remoteAddress.toString();
                    log.warn("[CLOSE] "+address + " is closed by client side");
                    channel.close();
                    key.cancel();
                    return;
                }

                buffer.flip();
                byte[] read = new byte[readCount];
                buffer.get(read);

                if (buffer.capacity() == buffer.position()){
                    ByteBuffer newBuffer = ByteBuffer.allocateDirect(buffer.limit() << 1);
                    buffer.flip();
                    newBuffer.put(buffer);
                    key.attach(newBuffer);
                    return;
                }

                String s = new String(read, StandardCharsets.UTF_8);
                log.info("[RECEIVE] server receive msg: {}", s);
                buffer.compact();
            } catch (IOException e) {
                SocketAddress remoteAddress = ((SocketChannel) key.channel()).getRemoteAddress();
                String address = remoteAddress.toString();
                log.error("[CLOSE] "+ address + " " + e.getLocalizedMessage());
                key.cancel();
            }
        }
    }

}
