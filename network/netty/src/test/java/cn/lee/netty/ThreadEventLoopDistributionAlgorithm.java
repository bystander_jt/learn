package cn.lee.netty;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

public class ThreadEventLoopDistributionAlgorithm {

    @Benchmark()
    @BenchmarkMode({Mode.Throughput})
    @Measurement(iterations = 3)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Fork(value = 1, warmups = 3)
    public void generic() {
        int len = 8;

        int[] distributionStatus = new int[len];
        Algorithm genericEventExecutor = (counter, length) -> Math.abs(counter % length);
        for (int i = 0; i < Math.pow(8, 2); i++) {
            int val = distributionStatus[genericEventExecutor.calculate(i, len)];
            ++val;
            distributionStatus[genericEventExecutor.calculate(i, len)] = val;
        }

//        System.out.println(Arrays.toString(distributionStatus));

    }

    @Benchmark()
    @BenchmarkMode({Mode.Throughput})
    @Measurement(iterations = 3)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Fork(value = 1, warmups = 3)
    public void powerOfTwo() {
        int len = 8;

        int[] distributionStatus = new int[len];
        Algorithm powerOfTwoEventExecutor = (counter, length) -> counter & length - 1;
        for (int i = 0; i < (Math.pow(8, 2)); i++) {
            int val = distributionStatus[powerOfTwoEventExecutor.calculate(i, len)];
            ++val;
            distributionStatus[powerOfTwoEventExecutor.calculate(i, len)] = val;
        }

//        System.out.println(Arrays.toString(distributionStatus));


    }

    @FunctionalInterface
    private interface Algorithm {
        int calculate(int counter, int length);
    }

}
