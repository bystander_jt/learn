package cn.lee.multi.datasource.configuration;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;


@AutoConfigureBefore
@Configuration
public class MultipleDatasourceConfiguration {


    @Bean(name = "primaryDatasourceProperties")
    @ConfigurationProperties(prefix = "spring.datasource.main")
    public DataSourceProperties primaryDatasourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    public DataSource primaryDatasource(DataSourceProperties primaryDatasourceProperties) {
        return primaryDatasourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean
    public PlatformTransactionManager primaryTransactionManager(DataSource primaryDatasource) {
        return new DataSourceTransactionManager(primaryDatasource);
    }




    @Bean(name = "secondDatasourceProperties")
    @ConfigurationProperties(prefix = "spring.datasource.second")
    public DataSourceProperties secondDatasourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public DataSource backupDatasource(DataSourceProperties secondDatasourceProperties) {
        return secondDatasourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean
    public PlatformTransactionManager secondTransactionManager(DataSource backupDatasource) {
        return new DataSourceTransactionManager(backupDatasource);
    }

}
