#!/bin/bash

_MYSQL_VERSION=8.0.24

run_server(){

  docker run -d --name #{INSTANCE_NAME} --privileged --restart always \
  -e 'MYSQL_ROOT_PASSWORD=root' \
  -p ${_PORT}:3306 \

  mysql:${_MYSQL_VERSION}
}