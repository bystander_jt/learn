# 基于 Sharding JDBC 的读写分离实例

## `MySQL` 配置

### 配置简介

| Node    | `port` | 数据目录              |
| ------- | ------ | --------------------- |
| master  | 13306  | `/root/mysql/master`  |
| slave-0 | 13307  | `/root/mysql/slave-0` |
| slave-1 | 13308  | `/root/mysql/slave-1` |
| slave-2 | 13309  | `/root/mysql/slave-2` |

### 初始化

- 创建文件夹

  ```bash
  mkdir -p /root/mysql/master/data
  mkdir -p /root/mysql/slave-0/data
  mkdir -p /root/mysql/slave-1/data
  mkdir -p /root/mysql/slave-2/data
  
  mkdir -p /root/mysql/socket
  ```

- 初始化数据库

  ```bash
  mysqld --defaults-file=./mysql-master.cnf --user=root --initialize
  mysqld --defaults-file=./mysql-slave-0.cnf --user=root --initialize
  mysqld --defaults-file=./mysql-slave-1.cnf --user=root --initialize
  mysqld --defaults-file=./mysql-slave-2.cnf --user=root --initialize
  ```

  > `--user` 指定选择何种用户操作数据库, 如果使用 `root` 账户, 那会报错, 通过指定 `--user=root` 解决
  >
  > 在初始化时会打印密码, 请勿直接清空控制台. 如果不慎遗失, 通过清空数据目录, 重新生成数据目录.

- 修改默认账户密码

  ```mysql
  mysql -uroot -p -P<port> --protocol=socket -S=/root/mysql/socket/<master or slave>.sock
  
  ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
  # 创建远程连接账户
  create user root@'%' identified by 'root';
  # 赋予全部全县
  grant all privileges on *.* to root@'%' WITH grant option;
  ```

  > 默认 `MySQL` 使用 `caching_sha2_password` 做密码加密,
  > 在后续进行主从连接时就会提示 `Authentication requires secure connection. Error_code: MY-002061`
  >
  > 这时候有两种解决方案
  >
  > - 修改账户的密码加密插件为 `mysql_native_password`
      >
      >    ```mysql
  >    create user root@'%' identified with 'mysql_native_password' by '<password>;'
  >    ALTER USER <username>@'<scope>' IDENTIFIED WITH 'mysql_native_password' by '<password>'
  >    ```
      >
      >
  - 通过服务器的 `RSA` 公钥加密密码
    >
    >    `mysql -P3307 --get-server-public-key`

重复上方步骤, 配置所有的 master slave 节点

### 创建 slave 节点登入用户, 并赋予权限

```mysql
CREATE USER 'replica'@'%' IDENTIFIED BY '<password>'

GRANT REPLICATION SLAVE ON *.* TO 'replica'@'%';
GRANT REPLICATION_APPLIER ON *.* TO 'replica'@'%';
GRANT FILE ON *.* TO 'replica'@'%';
```

### 开始 `replica`

```mysql
CHANGE REPLICATION SOURCE TO SOURCE_HOST="127.0.0.1", SOURCE_PORT=13306, SOURCE_USER='replica', SOURCE_PASSWORD='root' FOR CHANNEL 'channel1';

# 指定需要备份的数据库
CHANGE REPLICATION FILTER REPLICATE_DO_DB=(dbs) FOR CHANNEL 'channel1';

# 设定完成后启动 replica
START REPLICA FOR CHANNEL 'channel1';

# 然后查看状态是否正常
SHOW replica status\G

*************************** 1. row ***************************
             Replica_IO_State: Waiting for source to send event
                  Source_Host: 127.0.0.1
                  Source_User: replica
                  Source_Port: 13306
                Connect_Retry: 60
              Source_Log_File: replica_binlog_.000003
          Read_Source_Log_Pos: 3021
               Relay_Log_File: localhost-relay-bin-rp.000005
                Relay_Log_Pos: 3249
        Relay_Source_Log_File: replica_binlog_.000003
           Replica_IO_Running: Yes
          Replica_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
           Replicate_Do_Table:
       Replicate_Ignore_Table:
      Replicate_Wild_Do_Table:
  Replicate_Wild_Ignore_Table:
                   Last_Errno: 0
                   Last_Error:
                 Skip_Counter: 0
          Exec_Source_Log_Pos: 3021
              Relay_Log_Space: 3559
              Until_Condition: None
               Until_Log_File:
                Until_Log_Pos: 0
           Source_SSL_Allowed: No
           Source_SSL_CA_File:
           Source_SSL_CA_Path:
              Source_SSL_Cert:
            Source_SSL_Cipher:
               Source_SSL_Key:
        Seconds_Behind_Source: 0
Source_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error:
               Last_SQL_Errno: 0
               Last_SQL_Error:
  Replicate_Ignore_Server_Ids:
             Source_Server_Id: 1
                  Source_UUID: 32cc916b-b15a-11ed-9629-0800277f8c41
             Source_Info_File: mysql.slave_master_info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
    Replica_SQL_Running_State: Replica has read all relay log; waiting for more updates
           Source_Retry_Count: 86400
                  Source_Bind:
      Last_IO_Error_Timestamp:
     Last_SQL_Error_Timestamp:
               Source_SSL_Crl:
           Source_SSL_Crlpath:
           Retrieved_Gtid_Set: 32cc916b-b15a-11ed-9629-0800277f8c41:1-15
            Executed_Gtid_Set: 32cc916b-b15a-11ed-9629-0800277f8c41:1-15,
9ae131e5-b15a-11ed-9d77-0800277f8c41:1-10
                Auto_Position: 0
         Replicate_Rewrite_DB:
                 Channel_Name: rp
           Source_TLS_Version:
       Source_public_key_path:
        Get_Source_public_key: 0
            Network_Namespace:
1 row in set (0.00 sec)


# 当确认 Replica_IO_RUNNING = YES 和 Replica_SQL_RUNNING=Yes 且没有提示任何 WARN 则代表成功
# 之后插入数据都会自动下发到 slave 节点中.
```

[更多内容请查阅官方文档](https://dev.mysql.com/doc/refman/8.0/en/replication.html)

[REPLICA FILTER 更多参数详解](https://dev.mysql.com/doc/refman/8.0/en/change-replication-filter.html)

[建立主从指令详解](https://dev.mysql.com/doc/refman/8.0/en/change-replication-source-to.html)

### 重置 `REPLICATION`

重置 replication 指删除指定的 replication 配置或全部配置.

```mysql
# 重置所有的 replica 设置
RESET REPLICA ALL
# 重置指定的 replica 设置
RESET REPLICA FOR CHANNEL 'channel1'
```

### 对 Slave 开启只读模式

为了保证 slave 同步的都是 master 节点上的数据, 我们需要将 slave 节点设置为只读.

```mysql
SET @@GLOBAL.read_ony=ON
# 解锁
SET @@GLOBAL.read_ony=OFF

# 上方只对普通用户生效, 对于 ROOT 超级用户是不生效的.
# 如果要锁定 ROOT 超级用户则需要使用下方指令
flush tables with read lock;

# 解锁
UNLOCK TABLES;

```

