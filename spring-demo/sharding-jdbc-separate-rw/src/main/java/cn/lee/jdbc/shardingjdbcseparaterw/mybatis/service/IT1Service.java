package cn.lee.jdbc.shardingjdbcseparaterw.mybatis.service;

import cn.lee.jdbc.shardingjdbcseparaterw.mybatis.entity.T1;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author L
 * @since 2023-03-25
 */
public interface IT1Service extends IService<T1> {

}
