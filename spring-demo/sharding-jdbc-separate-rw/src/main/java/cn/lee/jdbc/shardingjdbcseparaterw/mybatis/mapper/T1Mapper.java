package cn.lee.jdbc.shardingjdbcseparaterw.mybatis.mapper;

import cn.lee.jdbc.shardingjdbcseparaterw.mybatis.entity.T1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author L
 * @since 2023-03-25
 */
public interface T1Mapper extends BaseMapper<T1> {

}
