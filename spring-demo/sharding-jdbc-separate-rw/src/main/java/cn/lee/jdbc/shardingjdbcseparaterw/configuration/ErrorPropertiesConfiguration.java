package cn.lee.jdbc.shardingjdbcseparaterw.configuration;

import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ErrorPropertiesConfiguration {


    @Bean
    public ErrorProperties errorProperties() {
        ErrorProperties properties = new ErrorProperties();
        properties.setIncludeStacktrace(ErrorProperties.IncludeAttribute.ALWAYS);
        return properties;
    }


}
