package cn.lee.jdbc.shardingjdbcseparaterw;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@ComponentScan(basePackages = {"cn.lee.jdbc.shardingjdbcseparaterw.mybatis.service.impl"})
@MapperScan(basePackages = {"cn.lee.jdbc.shardingjdbcseparaterw.mybatis.mapper"})
public class ShardingJdbcSeparateRwApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcSeparateRwApplication.class, args);
    }

}



