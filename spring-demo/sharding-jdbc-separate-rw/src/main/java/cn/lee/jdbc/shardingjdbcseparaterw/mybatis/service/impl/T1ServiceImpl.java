package cn.lee.jdbc.shardingjdbcseparaterw.mybatis.service.impl;

import cn.lee.jdbc.shardingjdbcseparaterw.mybatis.entity.T1;
import cn.lee.jdbc.shardingjdbcseparaterw.mybatis.mapper.T1Mapper;
import cn.lee.jdbc.shardingjdbcseparaterw.mybatis.service.IT1Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author L
 * @since 2023-03-25
 */
@Service
public class T1ServiceImpl extends ServiceImpl<T1Mapper, T1> implements IT1Service {

}
