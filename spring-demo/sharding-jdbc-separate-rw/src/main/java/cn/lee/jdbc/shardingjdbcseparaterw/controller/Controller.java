package cn.lee.jdbc.shardingjdbcseparaterw.controller;

import cn.lee.jdbc.shardingjdbcseparaterw.mybatis.entity.T1;
import cn.lee.jdbc.shardingjdbcseparaterw.mybatis.service.impl.T1ServiceImpl;
import cn.lee.util.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    private T1ServiceImpl it1Service;

    @GetMapping("/{id}")
    public Result findById(@PathVariable Long id) {
        LambdaQueryWrapper<T1> wrapper = new LambdaQueryWrapper<T1>().eq(T1::getId, id);
        return Result.ok(it1Service.getOne(wrapper));
    }

    @GetMapping("/all")
    public Result<List<T1>> list() {
        return Result.ok(it1Service.list());
    }

    @GetMapping("/insert/{count}")
    public Result insertCount(@PathVariable Long count) {
        count = Optional.ofNullable(count).orElse(100L);

        ArrayList<T1> batch = new ArrayList<>(count.intValue());
        T1 t;
        for (Long i = 0L; i < count; i++) {
            t = new T1();
            t.setName(UUID.randomUUID().toString());
            batch.add(t);

            if (batch.size() == 100) {
                it1Service.saveBatch(batch);
                batch.clear();
            }
        }

        return Result.ok("total: " + count);

    }

}
