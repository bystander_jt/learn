package cn.lee.jpa.repository;

import cn.lee.jpa.entity.Simple;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SimpleRepository extends CrudRepository<Simple, Long> {
}
