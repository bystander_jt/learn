package cn.lee.jpa.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "entity")
@Table(name = "simple",
        schema = "${database.name}",
        indexes = {
                @Index(name = "id_index", columnList = "id")
        })
public class Simple implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

}
