package cn.lee.jdbc.druid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class  DruidApplication {


    public static void main(String[] args) {
        SpringApplication.run(DruidApplication.class)
                .start();
    }

}
