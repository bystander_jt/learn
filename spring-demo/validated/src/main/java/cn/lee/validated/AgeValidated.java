package cn.lee.validated;


import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import java.lang.annotation.*;

@NotBlank
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CustomConstraintValidator.class)
public @interface AgeValidated {

    Class<?> validateClass();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
