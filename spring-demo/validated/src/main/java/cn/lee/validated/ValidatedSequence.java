package cn.lee.validated;

import cn.lee.validated.constant.ValidatedGroup;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import java.util.ArrayList;
import java.util.List;

public class ValidatedSequence implements DefaultGroupSequenceProvider<RequestDTO> {


    @Override
    public List<Class<?>> getValidationGroups(RequestDTO requestDTO) {
        List<Class<?>> res = new ArrayList<>(1);
        res.add(RequestDTO.class);
        if (requestDTO == null) return res;
        int age = requestDTO.getAge();
        if (age < 18){
            res.add(ValidatedGroup.MinisEighteen.class);
        } else if (age < 40){
            res.add(ValidatedGroup.MinisForty.class);
        } else if (age < 70){
            res.add(ValidatedGroup.MinisSeventy.class);
        } else {
            res.add(ValidatedGroup.MinisNinety.class);
        }

        return res;
    }



}
