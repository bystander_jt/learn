package cn.lee.validated;

import cn.lee.validated.constant.ValidatedGroup;
import org.hibernate.validator.group.GroupSequenceProvider;

import javax.validation.GroupSequence;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

@GroupSequenceProvider(ValidatedSequence.class)
public class RequestDTO {


    private int age;

    /**
     * <ul>
     *     <li>if age < 18 then type = 0</li>
     *     <li>if age < 40 then type = 1</li>
     *     <li>if age < 70 then type = 2</li>
     *     <li>if age < 90 then type = 4</li>
     * </ul>
     */
    @Max(value = 0, groups = {ValidatedGroup.MinisEighteen.class})
    @Size(min = 1, max = 1, groups = {ValidatedGroup.MinisForty.class})
    @Size(min = 2, max = 2, groups = {ValidatedGroup.MinisSeventy.class})
    @Size(min = 3, max = 3, groups = {ValidatedGroup.MinisNinety.class})
    private int type;

    public RequestDTO(){}

    public RequestDTO(int age, int type) {
        this.age = age;
        this.type = type;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
