package cn.lee.validated;

import org.junit.jupiter.api.Test;
import org.springframework.validation.ValidationUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.bootstrap.GenericBootstrap;
import java.util.Set;

public class ValidationTest {

    @Test
    public void ageTest(){
        RequestDTO requestDTO = new RequestDTO(0, 1);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<RequestDTO>> validate = validator.validate(requestDTO);
        for (ConstraintViolation<RequestDTO> res : validate) {
            System.out.println(res.getMessage());
        }

    }


}
