package cn.lee.observe.pojo.listener;

import cn.lee.observe.pojo.dto.Order;
import cn.lee.observe.pojo.enums.OrderState;
import cn.lee.observe.pojo.enums.OrderStateChangeAction;
import org.springframework.messaging.Message;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;
import org.springframework.stereotype.Component;

@Component
@WithStateMachine
public class OrderStateListener {

    @OnTransition(source = {"WAIT_TO_PAY"}, target = {"WAIT_SEND"})
    public boolean payToSend(Message<OrderStateChangeAction> message){
        Order order = (Order) message.getHeaders().get("order");
        order.setOrderState(OrderState.WAIT_SEND);
        return true;
    }


    @OnTransition(source = {"WAIT_SEND"}, target = {"WAIT_RECEIVE"})
    public void sendToReceive(Message<OrderStateChangeAction> message){
        Order order = (Order) message.getHeaders().get("order");
        order.setOrderState(OrderState.WAIT_RECEIVE);
    }



    @OnTransition(source = {"WAIT_RECEIVE"}, target = {"FINISH"})
    public void receiveToFinish(Message<OrderStateChangeAction> message){
        Order order = (Order) message.getHeaders().get("order");
        order.setOrderState(OrderState.FINISH);
    }



}
