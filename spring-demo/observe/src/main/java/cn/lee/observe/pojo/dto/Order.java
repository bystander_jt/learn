package cn.lee.observe.pojo.dto;

import cn.lee.observe.pojo.enums.OrderState;
import cn.lee.observe.pojo.enums.OrderStateChangeAction;

public class Order {

    private Integer orderId;

    private OrderState orderState;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }
}
