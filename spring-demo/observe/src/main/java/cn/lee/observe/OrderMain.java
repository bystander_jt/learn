package cn.lee.observe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.statemachine.config.EnableStateMachine;

@SpringBootApplication
public class OrderMain {


    public static void main(String[] args) {
        SpringApplication.run(OrderMain.class, args).start();
    }


}
