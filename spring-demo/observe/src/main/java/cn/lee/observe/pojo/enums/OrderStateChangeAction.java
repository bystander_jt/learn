package cn.lee.observe.pojo.enums;

public enum OrderStateChangeAction {

    PAY_ORDER,

    /**
     * 发货
     */
    SEND_ORDER,


    /**
     * 收货
     */
    RECEIVE_ORDER,


}
