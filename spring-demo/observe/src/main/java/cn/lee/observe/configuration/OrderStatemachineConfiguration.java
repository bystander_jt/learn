package cn.lee.observe.configuration;


import cn.lee.observe.pojo.dto.Order;
import cn.lee.observe.pojo.enums.OrderState;
import cn.lee.observe.pojo.enums.OrderStateChangeAction;
import org.springframework.context.annotation.Bean;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;

import java.util.EnumSet;

@EnableStateMachine
public class OrderStatemachineConfiguration extends StateMachineConfigurerAdapter<OrderState, OrderStateChangeAction> {

    @Override
    public void configure(StateMachineStateConfigurer<OrderState, OrderStateChangeAction> states) throws Exception {
        states.withStates().initial(OrderState.WAIT_TO_PAY).states(EnumSet.allOf(OrderState.class));
    }


    @Override
    public void configure(StateMachineTransitionConfigurer<OrderState, OrderStateChangeAction> transitions) throws Exception {
        transitions.withExternal()
                .source(OrderState.WAIT_TO_PAY).target(OrderState.WAIT_SEND).event(OrderStateChangeAction.PAY_ORDER)
                .and()
                .withExternal()
                .source(OrderState.WAIT_SEND).target(OrderState.WAIT_RECEIVE).event(OrderStateChangeAction.SEND_ORDER)
                .and()
                .withExternal()
                .source(OrderState.WAIT_RECEIVE).target(OrderState.FINISH).event(OrderStateChangeAction.RECEIVE_ORDER);
    }

    @Bean
    public DefaultStateMachinePersister persister(){
        return new DefaultStateMachinePersister<OrderState, OrderStateChangeAction, Order>(new StateMachinePersist<OrderState, OrderStateChangeAction, Order>() {
            @Override
            public void write(StateMachineContext<OrderState, OrderStateChangeAction> context, Order contextObj) throws Exception {
                // 数据持久化
                // Redis | MySQL | MongoDB | ...
            }

            @Override
            public StateMachineContext<OrderState, OrderStateChangeAction> read(Order contextObj) throws Exception {
                return new DefaultStateMachineContext<>(contextObj.getOrderState(), null, null, null);
            }
        });
    }
}
