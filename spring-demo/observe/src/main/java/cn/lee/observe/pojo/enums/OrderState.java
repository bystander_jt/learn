package cn.lee.observe.pojo.enums;

public enum OrderState {

    WAIT_TO_PAY,

    WAIT_SEND,

    WAIT_RECEIVE,

    FINISH


}
