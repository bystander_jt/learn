package cn.lee.mybatis.configuration;

import cn.lee.mybatis.interceptor.CustomInterceptor;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

//@Configuration
public class MyBatisConfiguration {

    @Bean
    public MybatisPlusInterceptor interceptor(){
        MybatisPlusInterceptor interceptors = new MybatisPlusInterceptor();
        interceptors.addInnerInterceptor(new TenantLineInnerInterceptor());
        return interceptors;
    }


    @Bean
    public List<Interceptor> configure() {
        List<Interceptor> provider = new ArrayList<>(1);
        provider.add(new CustomInterceptor());
        return provider;
    }


}
