package cn.lee.mybatis.plus.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.keywords.MySqlKeyWordsHandler;

import java.util.HashMap;
import java.util.Map;

public class MyBatisGenerator {

    private static final Map<OutputFile, String> PATH_INFO;

    static {
        PATH_INFO = new HashMap<>(OutputFile.values().length);

        String userDir = System.getProperty("user.dir");
        PATH_INFO.put(OutputFile.mapper, "E:\\Users\\developer\\code\\learn\\spring-demo\\mybatis_demo\\src\\main\\java\\cn\\lee\\mybatis\\mapper");
        PATH_INFO.put(OutputFile.entity, "E:\\Users\\developer\\code\\learn\\spring-demo\\mybatis_demo\\src\\main\\java\\cn\\lee\\mybatis\\entity");
        PATH_INFO.put(OutputFile.service, "E:\\Users\\developer\\code\\learn\\spring-demo\\mybatis_demo\\src\\main\\java\\cn\\lee\\mybatis\\service");
        PATH_INFO.put(OutputFile.serviceImpl, "E:\\Users\\developer\\code\\learn\\spring-demo\\mybatis_demo\\src\\main\\java\\cn\\lee\\mybatis\\impl");
        PATH_INFO.put(OutputFile.xml, "E:\\Users\\developer\\code\\learn\\spring-demo\\mybatis_demo\\src\\main\\resources\\META-INF\\mybatis\\ext");
        PATH_INFO.put(OutputFile.controller, "");
        PATH_INFO.put(OutputFile.other, "");
    }

    private DataSourceConfig datasource() {
        return new DataSourceConfig.Builder("jdbc:mysql://10.0.0.157:3306/mybatis_test", "root", "root")
                .dbQuery(new MySqlQuery())
                .schema("mybatis_test")
                .typeConvert(new MySqlTypeConvert())
                .keyWordsHandler(new MySqlKeyWordsHandler())
                .build();
    }

    private GlobalConfig config() {
        return new GlobalConfig.Builder()
                .outputDir("E:\\Users\\developer\\code\\learn\\spring-demo\\mybatis_demo\\src\\main\\java\\")
                .author("L").fileOverride()

//                .enableKotlin()
//                .enableSwagger()
                .dateType(DateType.TIME_PACK)
                .commentDate("yyyy-MM-dd")
                .build();
    }

    private PackageConfig packageConfig() {
        return new PackageConfig.Builder()
                .xml("ExtMapper.xml")
                .parent("cn.lee.mybatis")
                .pathInfo(PATH_INFO)
                .build();
    }

    public void createEntity() {
        AutoGenerator generator = new AutoGenerator(datasource())
                .global(config())
                .packageInfo(packageConfig());

        generator.execute();
    }


    public static void main(String[] args) {
        new MyBatisGenerator().createEntity();
    }

}
