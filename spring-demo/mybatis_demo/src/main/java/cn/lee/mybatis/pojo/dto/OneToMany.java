package cn.lee.mybatis.pojo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@ToString
public class OneToMany implements Serializable {

    private int a1;

    private int a2;

    private List<Integer> a3;

//    public OneToMany(int a1) {
//        this.a1 = a1;
//    }


}
