package cn.lee.mybatis;

import cn.lee.mybatis.entity.Table;
import cn.lee.mybatis.mapper.TableMapper;
import cn.lee.mybatis.result.TableListResultHandler;
import cn.lee.mybatis.result.TableResultHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@MapperScans(
        @MapperScan(basePackages = "cn.lee.mybatis.mapper")
)
@SpringBootApplication
public class MyBatisApplication {


    public static void main(String[] args) {
        SpringApplication.run(MyBatisApplication.class, args).start();
    }

    @Configuration
    public class Boot implements ApplicationListener<ApplicationReadyEvent> {

        @Autowired
        private TableMapper tableMapper;

        @Override
        public void onApplicationEvent(ApplicationReadyEvent event) {
            TableResultHandler handle = new TableResultHandler();
            tableMapper.selectOneWithResultHandler( 10, handle);


            TableListResultHandler handler = new TableListResultHandler();
            tableMapper.queryWithResultHandler(0, 10, handler);

        }
    }


}
