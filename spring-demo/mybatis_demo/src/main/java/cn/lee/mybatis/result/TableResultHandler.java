package cn.lee.mybatis.result;

import cn.lee.mybatis.entity.Table;
import cn.lee.mybatis.mapper.TableMapper;
import com.baomidou.mybatisplus.core.override.MybatisMapperMethod;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;

/**
 * <p> {@link ResultHandler} 是对结果自定义处理然后再做返回. 我们可以根据 {@link ResultContext} 获取到当前的返回结果.
 * </p>
 * <ul>
 *     <li><b>使用 {@link ResultHandler} 必须无返回值</b></li>
 *     <li><b>使用 {@link ResultHandler} 必须设定 ResultHandler 在方法中</b></li>
 * </ul>
 *
 * more detail {@link MybatisMapperMethod#execute(SqlSession, Object[])} case SELECT 中.
 *
 *
 * @see TableMapper#queryWithResultHandler(int, int, ResultHandler)
 * @see TableMapper#selectOneWithResultHandler(long, ResultHandler)
 *
 *
 */
public class TableResultHandler implements ResultHandler<Table> {


    @Override
    public void handleResult(ResultContext<? extends Table> resultContext) {
        System.out.println(this.getClass().getSimpleName() + " has triggered.");
        int total = resultContext.getResultCount();
        System.out.println("receive total: " + total + " results");
        System.out.println(resultContext.getResultObject());
    }
}
