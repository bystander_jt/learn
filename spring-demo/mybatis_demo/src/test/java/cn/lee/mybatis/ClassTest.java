package cn.lee.mybatis;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ClassTest {

    public interface C1 extends InvocationHandler {

        void method1();

        default void method2() {

        }

    }



    public static void main(String[] args) {
        Method[] methods = C1.class.getMethods();

        Class<C1> clzz = C1.class;



        System.out.println("interfaces: " + Arrays.stream(clzz.getInterfaces()).map(Class::getSimpleName).reduce("", (a,b) -> a + "," + b));

        for (Method method : methods) {
            System.out.print("method:" + method.getName() );
            System.out.print(" isDefault: " + method.isDefault());
            System.out.print(" isBridge: " + method.isBridge());
            System.out.print(" isSynthetic: " + method.isSynthetic());
            System.out.print(" isVarArgs: " + method.isVarArgs());
            System.out.println();
        }
    }

}
