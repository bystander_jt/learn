package com.lee.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.lee.pojo.dto.Car;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Convert<T> {

    public String getJson(T object, ObjectMapper mapper) throws JsonProcessingException {
        String json = mapper.writeValueAsString(object);
        log.info("input json: {}", json);
        return json;
    }

    public T stringConvert(T object, ObjectMapper mapper) throws JsonProcessingException {
        Class<T> clazz = (Class<T>) object.getClass();

        String json = getJson(object, mapper);

        return getObject(json, mapper, clazz);
    }

    public T getObject(String object, ObjectMapper mapper, Class<T> clazz) throws JsonProcessingException {

        boolean b = mapper.canDeserialize(SimpleType.constructUnsafe(Car.class));
        log.warn("{}.class {} support deserialize", clazz.getSimpleName(), b ? "" : "NOT");

        boolean supportSerialize = mapper.canSerialize(TypeFactory.rawClass(Car.class));
        log.warn("{}.class {} support serialize", clazz.getSimpleName(), supportSerialize ? "" : "NOT");

        T data = (T) mapper.readValue(object, clazz);
        log.info("deserialize success!");
        return data;
    }


}
