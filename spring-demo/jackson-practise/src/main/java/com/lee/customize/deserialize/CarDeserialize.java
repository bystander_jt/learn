package com.lee.customize.deserialize;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.lee.pojo.dto.Car;

import java.io.IOException;
import java.math.BigDecimal;

public class CarDeserialize extends StdDeserializer<Car> {
    public CarDeserialize(Class<?> vc) {
        super(vc);
    }

    @Override
    public Car deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JacksonException {
        Car car = new Car();
        while (!parser.isClosed()) {
            JsonToken jsonToken = parser.nextToken();

            if (JsonToken.FIELD_NAME.equals(jsonToken)) {
                String fieldName = parser.getCurrentName();

                jsonToken = parser.nextToken();


                if ("color".equals(fieldName)) {
                    car.setColor(parser.getText());
                } else if ("type".equals(fieldName)) {
                    car.setType(parser.getValueAsString());
                } else if ("doors".equals(fieldName)) {
                    car.setDoors(parser.getValueAsInt());
                } else if ("wheels".equals(fieldName)) {
                    car.setWheels(parser.getValueAsInt());
                } else if ("price".equals(fieldName)) {
                    car.setPrice(parser.getDecimalValue());
                }
            }
        }
        return car;
    }
}
