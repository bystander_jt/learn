package com.lee.pojo.dto;

import lombok.Data;

import java.math.BigDecimal;


@Data
public class Car {

    private String color;

    private String type;

    private Integer doors;

    private int wheels;

    private BigDecimal price;


}
