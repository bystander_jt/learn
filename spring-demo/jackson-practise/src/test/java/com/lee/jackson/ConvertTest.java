package com.lee.jackson;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonFactoryBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.core.json.JsonWriteFeature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.lee.customize.deserialize.CarDeserialize;
import com.lee.demo.Convert;
import com.lee.pojo.dto.Car;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.mockito.junit.MockitoRule;

import java.math.BigDecimal;

@Slf4j
public class ConvertTest {

    private JsonFactoryBuilder builder = (JsonFactoryBuilder) JsonFactory.builder()
            .configure(JsonFactory.Feature.CANONICALIZE_FIELD_NAMES, true)
            .configure(JsonFactory.Feature.INTERN_FIELD_NAMES, true)
            .configure(JsonFactory.Feature.FAIL_ON_SYMBOL_HASH_OVERFLOW, true)
            .enable(JsonReadFeature.ALLOW_MISSING_VALUES)
            .enable(JsonWriteFeature.WRITE_HEX_UPPER_CASE)
            .enable(JsonWriteFeature.WRITE_NAN_AS_STRINGS)
            .disable(JsonWriteFeature.ESCAPE_NON_ASCII);

    private final ObjectMapper mapper = new ObjectMapper(builder.build());

    @Test
    public void test() throws JsonProcessingException {
        Convert<Car> convert = new Convert<>();
        Car car = new Car();
        car.setType("轿车");
        car.setColor("RED");
        car.setWheels(4);
        car.setPrice(BigDecimal.valueOf(2.0E05));
        car.setDoors(4);


        Car c = convert.stringConvert(car, mapper);

    }

    /**
     * 允许原生数据类型 (int, double) 为空.
     * 在传入为空的情况会默认使用 0, 0.0 做替换.
     */
    @Test
    public void primitiveTypeCanBeNull() throws JsonProcessingException {
        Convert<Car> convert = new Convert<>();
        ObjectMapper mapper = new ObjectMapper(builder.build());
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

        String nullWheels = "{\"color\":\"RED\",\"type\":\"轿车\",\"doors\":4,\"wheels\":null,\"price\":200000.0}";
        Car object = convert.getObject(nullWheels, mapper, Car.class);
        log.info("wheel field is null: {}", object);

        String filedNotExist = "{\"color\":\"RED\",\"type\":\"轿车\",\"doors\":4,\"price\":200000.0}";
        object = convert.getObject(filedNotExist, mapper, Car.class);
        log.info("missing wheel field: {}", object);



//        Car car = convert.stringConvert(mock, mapper);


    }

    @Test
    public void customizeDeserialize() throws JsonProcessingException {
        String json = "{\"color\":\"RED\",\"type\":\"轿车\",\"doors\":4,\"wheels\":1,\"price\":200000.0}";

        SimpleModule module =
                new SimpleModule("CarDeserializer", new Version(0, 0, 1, null, null, null));
        module.addDeserializer(Car.class, new CarDeserialize(Car.class));

        ObjectMapper mapper = new ObjectMapper(builder.build());
        mapper.registerModule(module);
        Car car = mapper.readValue(json, Car.class);

        log.info("customize deserialize: {}", car);

    }


}
