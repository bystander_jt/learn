package cn.lee.spring.bean;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Role;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/16 12:02
 * @since VERSION
 */

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Role(value = BeanDefinition.ROLE_INFRASTRUCTURE)
    @Bean
    public Object A(){
        return new Integer("10");
    }

}
