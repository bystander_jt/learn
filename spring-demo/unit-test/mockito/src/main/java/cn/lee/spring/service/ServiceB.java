package cn.lee.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/16 12:02
 * @since VERSION
 */

@Service
public class ServiceB {

    @Autowired
    private ServiceA a;
    @Autowired
    private Object A;


    public void test(){
        System.out.println(A);
    }

}
