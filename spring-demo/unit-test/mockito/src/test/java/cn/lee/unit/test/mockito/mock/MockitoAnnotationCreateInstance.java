package cn.lee.unit.test.mockito.mock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * 
 * 
 * @since VERSION
 */
@RunWith(MockitoJUnitRunner.class)
public class MockitoAnnotationCreateInstance {

    @Mock
    private List<String> mockList;

    @Test
    public void mockInstance(){
        mockList.add("one");
        boolean verify = Mockito.verify(mockList).add("one");
        assertEquals(0, mockList.size());

        Mockito.when(mockList.size()).thenReturn(100);
        assertEquals(100, mockList.size());
    }
}
