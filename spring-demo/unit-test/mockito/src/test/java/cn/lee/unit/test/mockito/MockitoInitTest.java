package cn.lee.unit.test.mockito;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

/**
 * 
 * 
 * @since VERSION
 */

public class MockitoInitTest {

    @Before
    public void init(){
        MockitoAnnotations.openMocks(MockitoInitTest.class);
    }

}
