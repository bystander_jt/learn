package cn.lee.consumer.configuration;

import org.apache.dubbo.config.ApplicationConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.apache.dubbo.common.constants.CommonConstants.EXECUTOR_MANAGEMENT_MODE_ISOLATION;

@Configuration
public class DubboConfiguration {


//    @Bean
    public ApplicationConfig applicationConfig(){

        ApplicationConfig config = new ApplicationConfig();
        config.setExecutorManagementMode(EXECUTOR_MANAGEMENT_MODE_ISOLATION);
        return config;
    }


}
