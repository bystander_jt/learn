package cn.lee.consumer.controller;

import cn.lee.consumer.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;

    @GetMapping("/run")
    public void run() {
        consumerService.consumer();
    }


}
