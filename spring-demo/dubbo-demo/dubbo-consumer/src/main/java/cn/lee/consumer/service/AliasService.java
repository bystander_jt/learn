package cn.lee.consumer.service;

import cn.lee.dubbo.api.IDubboDemo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

@Service
public class AliasService {


    @DubboReference(interfaceClass = IDubboDemo.class, loadbalance = "leastactive")
    private IDubboDemo dubbo;

}
