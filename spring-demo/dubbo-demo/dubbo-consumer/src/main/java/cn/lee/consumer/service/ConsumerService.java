package cn.lee.consumer.service;


import cn.lee.dubbo.api.IDubboDemo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {

    private static final Logger log = LoggerFactory.getLogger(ConsumerService.class);

    @DubboReference(interfaceClass = IDubboDemo.class, loadbalance = "leastactive")
    private IDubboDemo dubboDemo;

    @EventListener(ApplicationReadyEvent.class)
    public void consumer() {
        log.info("consumer service : {}", dubboDemo.returnBoolean(true));
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
        log.info("consumer weight : {}", dubboDemo.weight());
    }


}
