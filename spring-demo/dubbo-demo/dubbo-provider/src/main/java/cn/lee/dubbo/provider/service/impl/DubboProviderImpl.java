package cn.lee.dubbo.provider.service.impl;

import cn.lee.dubbo.api.IDubboDemo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;


@Slf4j
@DubboService(loadbalance = "leastactive", weight = 1000)
public class DubboProviderImpl implements IDubboDemo {

    private static final int weight = 1000;

    @Override
    public Boolean returnBoolean(Boolean boo) {
        log.info("revoke weight : {}", weight);
        return boo;
    }

    @Override
    public String weight() {
        return "current weight : " + weight;
    }

}
