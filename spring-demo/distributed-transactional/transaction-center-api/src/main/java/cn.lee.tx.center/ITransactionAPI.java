package cn.lee.tx.center;

public interface ITransactionAPI {


    boolean start(String txId);

    boolean committed(String txId);

    boolean rollback(String txId);

    TransactionStatus query(String txId);

}
