package cn.lee.tx.center;

import jdk.nashorn.internal.objects.annotations.Getter;

import java.io.Serializable;



public enum TransactionStatus implements Serializable {

    PENDING, COMMITTED, ROLLBACK, EXCEPTION,

}
