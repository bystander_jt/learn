# 分布式事务解决方案 - 本地消息表

本地消息表的分布式方案并非强一致性! 而是最终一致性, 其最核心的就是 "消息表".

对于消息表我们可以在调用方通过中间件发送 "事务消息" , 也可以通过主动调用

假设有 `A` `B` 两个服务, `A` 服务需要调用 `B` 服务, 且必须保证数据一致性. 