package cn.lee.distributed.transaction.lmt.pojo.dto.balance;

import java.io.Serializable;
import java.math.BigDecimal;

public class BalanceDTO implements Serializable {

    private String account;

    private BigDecimal amount;

    private String transferToAccount;

    public String getTransferToAccount() {
        return transferToAccount;
    }

    public void setTransferToAccount(String transferToAccount) {
        this.transferToAccount = transferToAccount;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
