package cn.lee.distributed.transaction.lmt.pojo.annotation;

import java.lang.annotation.*;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LocalTransactional {

    String rollback() default "rollback";

    boolean onExceptionRollback() default false;

    /**
     * 超时时间, 单位:ms
     * @return
     */
    int timeout() default 500;

}
