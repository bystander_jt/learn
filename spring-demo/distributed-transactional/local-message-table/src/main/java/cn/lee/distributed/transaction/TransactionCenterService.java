package cn.lee.distributed.transaction;

import cn.lee.distributed.transaction.lmt.pojo.listener.TransactionStatusChangeEvent;
import cn.lee.tx.center.ITransactionAPI;
import cn.lee.tx.center.TransactionStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
public class TransactionCenterService implements ITransactionAPI, ApplicationEventPublisherAware {


    private static final ConcurrentHashMap<String, TransactionStatus> TRANSACTION = new ConcurrentHashMap<>();

    private ApplicationEventPublisher publisher = null;

    @Override
    public boolean start(String txId) {
        if (TRANSACTION.containsKey(txId)) return false;
        TRANSACTION.put(txId, TransactionStatus.PENDING);
        return true;
    }

    @Override
    public boolean committed(String txId) {
        if (!TRANSACTION.containsKey(txId)) {
            log.error("Missing Transaction [{}]", txId);
            return false;
        }
        TransactionStatus status = TRANSACTION.get(txId);
        if (status != TransactionStatus.PENDING) {
            log.error("[{}] transaction already done with {} status", txId, status.toString());
            return false;
        }

        TRANSACTION.put(txId, TransactionStatus.COMMITTED);

        publisher.publishEvent(new TransactionStatusChangeEvent(txId, TransactionStatus.COMMITTED));
        return false;
    }

    @Override
    public boolean rollback(String txId) {
        if (!TRANSACTION.containsKey(txId)) {
            log.error("Missing Transaction [{}]", txId);
            return false;
        }
        TransactionStatus status = TRANSACTION.get(txId);
        if (status != TransactionStatus.PENDING) {
            log.error("[{}] transaction already done with {} status", txId, status.toString());
            return false;
        }

        TRANSACTION.put(txId, TransactionStatus.ROLLBACK);

        publisher.publishEvent(new TransactionStatusChangeEvent(txId, TransactionStatus.ROLLBACK));
        return false;
    }

    @Override
    public TransactionStatus query(String txId) {
        return TRANSACTION.get(txId);
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }
}
