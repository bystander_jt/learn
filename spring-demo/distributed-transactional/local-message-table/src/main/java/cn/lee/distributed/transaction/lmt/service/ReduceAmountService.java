package cn.lee.distributed.transaction.lmt.service;

import cn.lee.distributed.transaction.TransactionCenterService;
import cn.lee.distributed.transaction.lmt.pojo.annotation.LocalTransactional;
import cn.lee.distributed.transaction.lmt.pojo.dto.balance.BalanceDTO;
import cn.lee.distributed.transaction.lmt.pojo.listener.TransactionEvent;
import cn.lee.distributed.transaction.lmt.pojo.listener.TransactionStatusChangeEvent;
import cn.lee.distributed.transaction.lmt.pojo.tx.TransactionDO;
import cn.lee.tx.center.TransactionStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;

@Slf4j
@Service
public class ReduceAmountService implements ApplicationListener<ApplicationEvent> {

    /**
     * 假设 DB
     */
    private static final HashMap<String, BigDecimal> DB = new HashMap<>(2);


    @Autowired
    private TransactionCenterService centerService;

    @LocalTransactional(rollback = "rollback")
    public void update(BalanceDTO balance) {

    }


    public void rollback(TransactionDO<BalanceDTO> transaction) {


    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof TransactionEvent) {
            transactionEvent((TransactionEvent) event);
        } else if (event instanceof TransactionStatusChangeEvent) {
            statusChangeEvent((TransactionStatusChangeEvent) event);
        }
    }

    private void transactionEvent(TransactionEvent event) {
        String txId = event.getTxId();

        TransactionStatus status = centerService.query(txId);
        if (TransactionStatus.ROLLBACK == status) {
            rollback((TransactionDO<BalanceDTO>) event.getSource());
        } else if (TransactionStatus.PENDING == status) {
            update((BalanceDTO) event.getSource());
            centerService.committed(txId);
        } else {
            log.info("receive [{}] transaction, with fatal {} status", txId, status);
        }

        // 检查 txId 是否在本地执行过
        // 检查 txId 事务已经回滚
        // 设置 txId 本地为执行状态
        //
    }

    private void statusChangeEvent(TransactionStatusChangeEvent event) {
        log.info("receive transaction status change event, changing to {}", event.getStatus().toString());
        switch (event.getStatus()) {
            case ROLLBACK:
            case EXCEPTION:
                rollback((TransactionDO<BalanceDTO>) event.getSource());
        }
    }
}
