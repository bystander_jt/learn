package cn.lee.distributed.transaction.center.service;

import cn.lee.tx.center.ITransactionAPI;
import cn.lee.tx.center.TransactionStatus;
import org.springframework.stereotype.Service;

@Service
public class TransactionCenterImplService implements ITransactionAPI {

    @Override
    public boolean start(String txId) {

        return false;
    }

    @Override
    public boolean committed(String txId) {

        return false;
    }

    @Override
    public boolean rollback(String txId) {

        return false;
    }

    @Override
    public TransactionStatus query(String txId) {
        return null;
    }


}
