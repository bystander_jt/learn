package cn.lee.distributed.transaction.lmt.pojo.holder;

import cn.lee.distributed.transaction.lmt.pojo.tx.TransactionDO;

public class TransactionHolder {

    private static final ThreadLocal<TransactionDO<?>> LOCAL = new ThreadLocal<>();


    public static void setTransaction(TransactionDO<?> transaction) {
        LOCAL.set(transaction);
    }

    public static void pending() {
        TransactionDO<?> t = getTransaction();
        t.setStatus(TransactionDO.Status.PENDING);
        setTransaction(t);
    }


    public static void finish() {
        TransactionDO<?> t = getTransaction();
        t.setStatus(TransactionDO.Status.FINISH);
        setTransaction(t);
    }


    public static void committed() {
        TransactionDO<?> t = getTransaction();
        t.setStatus(TransactionDO.Status.COMMITTED);
        setTransaction(t);
    }

    public static void rollback() {
        TransactionDO<?> t = getTransaction();
        t.setStatus(TransactionDO.Status.ROLLBACK);
        setTransaction(t);
    }


    public static TransactionDO<?> getTransaction() {
        return LOCAL.get();
    }

    public static void remove() {
        LOCAL.remove();
    }

}
