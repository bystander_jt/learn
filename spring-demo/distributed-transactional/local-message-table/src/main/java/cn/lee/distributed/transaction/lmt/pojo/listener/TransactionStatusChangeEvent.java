package cn.lee.distributed.transaction.lmt.pojo.listener;

import cn.lee.tx.center.TransactionStatus;
import org.springframework.context.ApplicationEvent;

public class TransactionStatusChangeEvent extends ApplicationEvent {


    private String txId;


    private TransactionStatus status;

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public TransactionStatusChangeEvent(String txId, TransactionStatus status) {
        super(txId);
        this.txId = txId;
        this.status = status;
    }
}
