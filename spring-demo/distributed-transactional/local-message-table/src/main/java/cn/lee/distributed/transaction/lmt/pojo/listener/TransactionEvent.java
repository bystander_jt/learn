package cn.lee.distributed.transaction.lmt.pojo.listener;

import cn.lee.distributed.transaction.lmt.pojo.dto.balance.BalanceDTO;
import org.springframework.context.ApplicationEvent;

import java.time.Clock;

public class TransactionEvent  extends ApplicationEvent {

    private String txId;

    public TransactionEvent(BalanceDTO source) {
        super(source);
    }


    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }
}
