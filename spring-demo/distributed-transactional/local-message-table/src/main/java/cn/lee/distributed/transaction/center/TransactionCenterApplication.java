package cn.lee.distributed.transaction.center;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy
@SpringBootApplication(scanBasePackages = {
        "cn.lee.distributed.transaction.center"
})
public class TransactionCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionCenterApplication.class, args).start();
    }

}
