package cn.lee.distributed.transaction.lmt.service;

import cn.lee.distributed.transaction.TransactionCenterService;
import cn.lee.distributed.transaction.lmt.pojo.annotation.LocalTransactional;
import cn.lee.distributed.transaction.lmt.pojo.dto.balance.BalanceDTO;
import cn.lee.distributed.transaction.lmt.pojo.holder.AccountHolder;
import cn.lee.distributed.transaction.lmt.pojo.listener.TransactionEvent;
import cn.lee.distributed.transaction.lmt.pojo.listener.TransactionStatusChangeEvent;
import cn.lee.distributed.transaction.lmt.pojo.tx.TransactionDO;
import cn.lee.tx.center.TransactionStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
public class IncreaseAmountService implements ApplicationListener<ApplicationEvent> {

    /**
     * 假设 DB
     */
    private static final ConcurrentHashMap<String, BigDecimal> DB = AccountHolder.get();


    @Autowired
    private TransactionCenterService centerService;

    @LocalTransactional(rollback = "rollback")
    public boolean update(BalanceDTO balanceDTO) {
        BigDecimal balance = DB.computeIfAbsent(balanceDTO.getAccount(), (key) -> BigDecimal.ZERO);

        // 账户余额不足, 事务不执行.
        if (balance.compareTo(BigDecimal.ZERO) < 0) {
            return false;
        }

        DB.put(balanceDTO.getAccount(), balance.subtract(balanceDTO.getAmount()));
        return true;
    }


    public void rollback(TransactionDO<BalanceDTO> transaction) {
        TransactionDO.Status status = transaction.getStatus();
        if (status != TransactionDO.Status.ROLLBACK) return;

        BalanceDTO context = transaction.getContext();
        DB.computeIfPresent(context.getAccount(), (key, oldVal) -> oldVal.add(context.getAmount()));

    }


    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof TransactionEvent) {
            transactionEvent((TransactionEvent) event);
        } else if (event instanceof TransactionStatusChangeEvent) {
            statusChangeEvent((TransactionStatusChangeEvent) event);
        }
    }

    private void transactionEvent(TransactionEvent event) {
        String txId = event.getTxId();

        TransactionStatus status = centerService.query(txId);
        if (TransactionStatus.ROLLBACK == status) {
            rollback((TransactionDO<BalanceDTO>) event.getSource());
        } else if (TransactionStatus.PENDING == status) {
            try {
                update((BalanceDTO) event.getSource());
                centerService.committed(txId);
            } catch (Exception e) {
                centerService.rollback(txId);
            }
        } else {
            log.info("receive [{}] transaction, with fatal {} status", txId, status);
        }

        // 检查 txId 是否在本地执行过
        // 检查 txId 事务已经回滚
        // 设置 txId 本地为执行状态
        //
    }

    private void statusChangeEvent(TransactionStatusChangeEvent event) {
        log.info("receive transaction status change event, changing to {}", event.getStatus().toString());
        switch (event.getStatus()) {
            case ROLLBACK:
            case EXCEPTION:
                rollback((TransactionDO<BalanceDTO>) event.getSource());
        }
    }
}
