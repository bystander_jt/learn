package cn.lee.distributed.transaction.lmt.pojo.holder;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class AccountHolder {

    private static final AccountHolder SINGLETON = new AccountHolder() ;

    private AccountHolder() {

    }

    public static ConcurrentHashMap<String, BigDecimal> get(){
        return SINGLETON.ACCOUNT;
    }

    private static final ConcurrentHashMap<String, BigDecimal> ACCOUNT = new ConcurrentHashMap<>(2);




}
