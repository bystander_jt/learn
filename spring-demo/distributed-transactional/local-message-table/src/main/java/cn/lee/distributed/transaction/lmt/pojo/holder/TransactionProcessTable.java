package cn.lee.distributed.transaction.lmt.pojo.holder;


import cn.lee.distributed.transaction.lmt.pojo.tx.TransactionDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;

@Slf4j
@Service
public class TransactionProcessTable implements BeanFactoryAware {

    private ListableBeanFactory beanFactory = null;

    private final ConcurrentHashMap<String, TransactionDO> TRANSACTION_TABLE = new ConcurrentHashMap<>(4);

    private final DelayQueue<TransactionDO> QUEUE = new DelayQueue<>();


    public void saveTransaction(TransactionDO transactionDO) {
        TransactionDO oldTransaction = TRANSACTION_TABLE.putIfAbsent(transactionDO.getTxId(), transactionDO);
        if (oldTransaction != null) {

            // ROLLBACK

        }
        QUEUE.offer(transactionDO);
        Optional.ofNullable(oldTransaction).ifPresent(QUEUE::remove);
    }

    public void committed(String txId) {
        TransactionDO transaction = TRANSACTION_TABLE.get(txId);
        if (transaction == null) return;
        transaction.setStatus(TransactionDO.Status.COMMITTED);
        QUEUE.remove(transaction);
    }

    public void rollback(String txId) {
        TransactionDO transaction = TRANSACTION_TABLE.get(txId);
        TransactionDO.Metadata metadata = transaction.getMetadata();
        QUEUE.remove(transaction);

        Method[] methods = metadata.getTargetClass().getMethods();
        Object service = beanFactory.getBean(metadata.getTargetClass());
        Method method = Arrays.stream(methods).filter(d -> d.getName().equals(metadata.getRollbackMethod())).findFirst().orElse(null);
        try {
            if (method != null)
                method.invoke(service, transaction.getContext());

        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error("transaction txId: [{}] rollback failure ", transaction.getTxId());
        } finally {
        }
    }

    public TransactionDO peek(){
        return QUEUE.peek();
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = (ListableBeanFactory) beanFactory;
    }
}
