package cn.lee.distributed.transaction.lmt.task;

import cn.lee.distributed.transaction.lmt.pojo.holder.TransactionProcessTable;
import cn.lee.distributed.transaction.lmt.pojo.tx.TransactionDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SynchronizeMessageTask {

    @Autowired
    private TransactionProcessTable table;

    @Scheduled(fixedRate = 5)
    public void sync(){

        TransactionDO peek = table.peek();

    }

}
