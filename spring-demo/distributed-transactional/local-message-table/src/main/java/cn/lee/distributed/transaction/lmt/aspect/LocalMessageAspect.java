package cn.lee.distributed.transaction.lmt.aspect;

import cn.lee.distributed.transaction.lmt.pojo.annotation.LocalTransactional;
import cn.lee.distributed.transaction.lmt.pojo.holder.TransactionHolder;
import cn.lee.distributed.transaction.lmt.pojo.holder.TransactionProcessTable;
import cn.lee.distributed.transaction.lmt.pojo.listener.TransactionEvent;
import cn.lee.distributed.transaction.lmt.pojo.listener.TransactionStatusChangeEvent;
import cn.lee.distributed.transaction.lmt.pojo.tx.TransactionDO;
import cn.lee.tx.center.TransactionStatus;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;


@Slf4j
@Aspect
@Component
public class LocalMessageAspect implements ApplicationEventPublisherAware {

    private static final ConcurrentHashMap<Class, List<Method>> REFLECTION_CACHE = new ConcurrentHashMap<>(12);

    @Autowired
    private TransactionProcessTable processTable;

    private ApplicationEventPublisher publisher;


    @Pointcut("@annotation(cn.lee.distributed.transaction.lmt.pojo.annotation.LocalTransactional)")
    public void pointCut() {

    }

    @Before("@annotation(transactional) && pointCut()")
    public void before(LocalTransactional transactional, ProceedingJoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();

        TransactionDO<Object> transaction = new TransactionDO<>(transactional.timeout(), args);
        Class<?> clzz = joinPoint.getTarget().getClass();
        Method rollbackMethod = getRollbackMethod(clzz, transactional.rollback(), () -> retrieveRollbackMethod(clzz));

        // 如果没有 rollbackMethod 就不会执行.
        if (rollbackMethod == null) {
            log.error("[LocalTransaction]- tx [{}] Missing Rollback method in [{}]", transaction.getTxId(), clzz.getName());
            transaction.setStatus(TransactionDO.Status.FINISH);
            return;
        }

        transaction.setMetadata(new TransactionDO.Metadata(clzz, rollbackMethod.getName()));
        TransactionHolder.setTransaction(transaction);

        // DURATION ON DISK OR DB
        processTable.saveTransaction(transaction);


    }

    @Around("@annotation(transactional) && pointCut()")
    public Object process(LocalTransactional transactional, ProceedingJoinPoint joinPoint) throws Throwable {

        Object result = null;
        result = joinPoint.proceed();
        return result;
    }

    @AfterReturning("@annotation(transactional) && pointCut()")
    public void normalReturn(LocalTransactional transactional) {
        publisher.publishEvent(new TransactionEvent(TransactionHolder.getTransaction().getContext()));
    }

    @AfterThrowing("pointCut()")
    public void afterThrowing() {
        TransactionDO<?> transaction = TransactionHolder.getTransaction();
        TransactionHolder.remove();
        processTable.rollback(transaction.getTxId());
        publisher.publishEvent(new TransactionStatusChangeEvent(transaction.getTxId(), TransactionStatus.ROLLBACK));
    }

    private List<Method> retrieveRollbackMethod(Class<?> targetClass) {
        Method[] methods = targetClass.getMethods();
        return Arrays.asList(methods);
    }

    private Method getRollbackMethod(Class<?> targetClass, String method, Callable<List<Method>> cacheOperation) {
        List<Method> methods = REFLECTION_CACHE.computeIfAbsent(targetClass, (clzz) -> {
            try {
                return cacheOperation.call();
            } catch (Exception e) {
                return null;
            }
        });
        Method rollbackMethod = methods.stream().filter(d -> d.getName().equals(method)).findFirst().orElse(null);
        if (rollbackMethod == null) {
            return null;
        }
        rollbackMethod.setAccessible(true);
        return rollbackMethod;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }
}
