package cn.lee.distributed.transaction.lmt;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableAspectJAutoProxy
@SpringBootApplication(scanBasePackages = {
        "cn.lee.distributed.transaction.lmt"
})
public class LocalMessageTableApplication {

    public static void main(String[] args) {
        SpringApplication.run(LocalMessageTableApplication.class, args).start();
    }

}
