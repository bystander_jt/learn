package cn.lee.distributed.transaction.lmt.pojo.tx;

import java.io.Serializable;
import java.util.UUID;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class TransactionDO<T> implements Serializable, Delayed {


    public static enum Status implements Serializable {
        NOT_STARTED, PENDING, ROLLBACK, COMMITTED, FINISH;
    }

    public static class Metadata {


        private final Class targetClass;
        private final String rollbackMethod;


        public Metadata(Class targetClass, String rollbackMethod) {
            this.targetClass = targetClass;
            this.rollbackMethod = rollbackMethod;
        }

        public Class getTargetClass() {
            return targetClass;
        }


        public String getRollbackMethod() {
            return rollbackMethod;
        }

    }


    private final String txId;

    /**
     * 事务状态
     */
    private Status status;

    private int timeout;

    private Metadata metadata;

    public Metadata getMetadata() {
        return metadata;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    private int startTime;

    private Object[] context;


    public String getTxId() {
        return txId;
    }


    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public <T> T getContext() {
        try {
            return (T) context[0];
        } catch (ClassCastException e) {

        }
        return null;
    }

    public void setContext(Object[] context) {
        this.context = context;
    }


    public TransactionDO(int timeout, Object[] args) {
        this.timeout = timeout;
        this.txId = UUID.randomUUID().toString();
        context = args;
        status = Status.PENDING;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.toMillis(timeout);
    }

    @Override
    public int compareTo(Delayed o) {
        return o.getDelay(TimeUnit.MICROSECONDS) < timeout ? -1 : o.getDelay(TimeUnit.MICROSECONDS) == timeout ? 0 : 1;
    }
}
