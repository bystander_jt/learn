package cn.lee.spring.tx.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
public class TxApplicationConfiguration {

    @Bean
    public PlatformTransactionManager txManager(DataSource dataSource){
        JdbcTransactionManager manager = new JdbcTransactionManager(dataSource);

        return manager;
    }


}
