package cn.lee.spring.tx.utils;

import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.transaction.support.TransactionSynchronizationUtils;

import java.util.Collections;


/**
 * https://www.bilibili.com/video/BV168411h7PU
 */
public class TransactionUtils {


    public static void after(Runnable runnable) {

        boolean transactionActive = TransactionSynchronizationManager.isActualTransactionActive();

        if (transactionActive) {
            TransactionSynchronizationUtils.invokeAfterCommit(Collections.singletonList(new TransactionD(runnable)));
        }
    }

    static class TransactionD implements TransactionSynchronization {

        private Runnable runnable;

        public TransactionD(Runnable runnable) {

        }

        @Override
        public void afterCompletion(int status) {
            if (STATUS_ROLLED_BACK == status) {
                // DO STH
            } else if (STATUS_COMMITTED == status) {
                runnable.run();
            }
        }
    }
}
