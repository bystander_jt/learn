package cm.lee.ha.limite;


import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class WindowNode<T> {

    /**
     * 当前节点统计数量
     */
    private volatile int count = 0;

    private volatile int limited;

    private int timestamp;

    private List<T> object;

    private ReentrantLock lock = new ReentrantLock();

    public boolean isFull(){

        return limited < count;
    }

}
