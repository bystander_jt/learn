package cn.lee.lettuce.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLettuceRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLettuceRedisApplication.class, args);
    }

}
