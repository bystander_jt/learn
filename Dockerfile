FROM maven:3-eclipse-temurin-17 AS build

ENV JAR_NAME=app.jar

RUN mkdir -p /app

COPY . /app
WORKDIR /app

# if you have private repository register, add `-s .mvn-settings.xml` args end of command.
# .mvn-settings.xml is a file that contains the settings for your private repository.
# If you don't have a private repository, you can skip this step.
RUN mvn package -Dmaven.test.skip=true -Dmaven.source.skip=true

# unzip jar files. if your jar file doesn't contain library files, you need change `--class-path`.
RUN jar xf target/${JAR_NAME}
RUN jdeps --ignore-missing-deps -q \
    --recursive \
    --multi-release \
    --print-module-deps \
    --class-path 'BOOT-INF/lib/*' \
    target/${JAR_NAME} > deps.info

RUN jlink \
    --add-modules $(cat deps.info) \
    --strip-debug \
    --no-header-files \
    --compress 2 \
    --no-man-pages \
    --output /17-jre



FROM eclipse-temurin:17-jre-alpine

ENV JAVA_HOME /usr/lib/java/jdk17
ENV PATH $PATH:$JAVA_HOME/bin

# add user group and user.
RUN addgroup --system limit-privileges && \
    adduser --system --ingroup limit-privileges --disabled-password --home /app -S -s /bin/false rookie

COPY --from=build /17-jre ${JAVA_HOME}
RUN mkdir /app
WORKDIR /app

RUN chown -R rookie:limit-privileges /app

COPY --from=build /app/target/${JAR_NAME} .

USER rookie
ENTRYPOINT java -jar ${JAR_NAME}




