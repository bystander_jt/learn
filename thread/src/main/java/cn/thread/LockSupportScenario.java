package cn.thread;

import java.util.concurrent.locks.LockSupport;

@SuppressWarnings("unused")
public class LockSupportScenario {


    public static Runnable t1 = () -> {
        output("thread started");
        output("thread benn park by LockSupport");
        LockSupport.park();
        output("thread resume");
    };
    public static Runnable timeout = () -> {
        output("thread started");
        output("thread park by LockSupport with 100ns timeout");
        LockSupport.parkNanos(100);
        output("thread resume");
    };

    public static void main(String[] args) throws InterruptedException {
//        System.out.println("==============================================================");
//        resumeByInterrupt();

//        System.out.println("==============================================================");
//        resumeByManual();
//
//        System.out.println("==============================================================");
//        resumeByInterrupt();
//
//        System.out.println("==============================================================");
//        differentThreadsWaiting();

        System.out.println("==============================================================");
        testDeadLock();

        System.out.println("==============================================================");
        testParkUnParkUnordered();
    }

    public static void resumeByInterrupt() throws InterruptedException {
        Thread thread = new Thread(t1, "LockSupport-Resume-By-Interrupt");
        thread.start();

        thread.join(200);

        output("interrupt thread");
        thread.interrupt();
    }

    public static void resumeByTimeout() {
        Thread thread = new Thread(timeout, "LockSupport-Resume-By-Timeout");
        thread.start();
    }

    public static void resumeByManual() throws InterruptedException {
        Thread thread = new Thread(t1, "LockSupport-Resume-By-Manual");
        thread.start();

        output("waiting thread been parked");
        thread.join(200);

        output("unpark thread");
        LockSupport.unpark(thread);
    }

    public static void output(String msg) {
        System.out.printf("%s %s%n", Thread.currentThread().getName(), msg);
    }

    public static void differentThreadsWaiting() throws InterruptedException {

        Thread sleep = new Thread(() -> {
            try {
                Thread.sleep(60 * 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "SLEEPING THREAD");
        sleep.start();

        Thread thread = new Thread(() -> {
            try {
                Object o = new Object();
                synchronized (o) {
                    o.wait();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "WAITING THREAD");
        thread.start();


        Thread lockSupport = new Thread(LockSupport::park, "LOCK SUPPORT THREAD");
        lockSupport.start();


        sleep.join();
        thread.interrupt();
        lockSupport.interrupt();


    }

    public static void testDeadLock() throws InterruptedException {

        Object lock = new Object();

        // 由于顺序错误导致无法唤醒在锁对象上的线程, 从而引发死锁的问题.
        Thread deadLock = new Thread(() -> {
            output("start complete");
            synchronized (lock) {
                try {
                    lock.wait();
                    lock.notify();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                output("release");
            }
        }, "CLASSIC LOCK");
        deadLock.start();

    }

    public static void testParkUnParkUnordered() throws InterruptedException {

        Object lock = new Object();


        Thread lockB = new Thread(() -> {
            output("start complete");
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                output("object Lock notified");
                LockSupport.park();
                output("lockSupport park unsuccessful");
            }
        }, "LOCK_B");
        lockB.start();
        lockB.join(1000);


        Thread lockA = new Thread(() -> {
            output("start complete");
            synchronized (lock) {
                output("UNPARK LOCK_B");
                LockSupport.unpark(lockB);
                output("notified Monitoring Object");
                lock.notifyAll();
            }
        }, "LOCK_A");

        lockA.start();
    }

}
