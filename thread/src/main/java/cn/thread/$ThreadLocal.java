package cn.thread;

import java.util.Random;

/**
 * 
 * 
 * @since VERSION
 */

public class $ThreadLocal {

    // 默认创建
//    @SuppressWarnings("raw")
//    private static final ;
    // 对线程局部直接初始化
    @SuppressWarnings("raw")
    private static final ThreadLocal<Integer> INITIALIZE_LOCAL = ThreadLocal.withInitial(() -> 8);

    @SuppressWarnings("AlibabaAvoidManuallyCreateThread")
    public static void main(String[] args) {

        ThreadLocal<Integer> LOCAL = new ThreadLocal<>();

        for (int thread = 0; thread < 8; thread++) {
            int finalThread = thread;

            Thread runner = new Thread(() -> {
                LOCAL.set(finalThread);
                System.out.println(Thread.currentThread().getName() + " GET LOCAL :" + LOCAL.get());
                System.out.println(Thread.currentThread().getName() + " GET INITIALIZE :" + INITIALIZE_LOCAL.get());
                System.out.println();
            }, "thread-" + thread);
            runner.start();
        }
        ;
    }

}
