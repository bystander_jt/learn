package cn.thread;

import java.util.concurrent.Callable;

/**
 * @author lijiangtao
 * 
 */

public class  CallableTread<T> implements Callable<Integer> {

    public Integer call() throws Exception {
        Thread.sleep(1000);
        return 2;
    }
}
