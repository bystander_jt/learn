package cn.thread;

public class WaitThread extends Thread {


    public WaitThread(String threadName) {
        setName(threadName);
    }

    @Override
    public void run() {
        int i = 0;
        while (i++ < 5) {
            synchronized (WaitThread.class){
                try {
                    if (this.isInterrupted()) return;
                    WaitThread.class.notify();
                    System.out.println(getName());
                    Thread.sleep(100);
                    WaitThread.class.wait(5000);
                } catch (InterruptedException e) {
                    System.out.println(getName() + " is interrupted");
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        WaitThread a = new WaitThread("A");
        a.start();
        new WaitThread("B").start();
        new WaitThread("C").start();
        new WaitThread("D").start();

        System.out.println("interceptor thread A");
        a.interrupt();

        System.out.println("main thread is over");
    }
}
