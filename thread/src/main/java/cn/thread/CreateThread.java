package cn.thread;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * 
 * 
 * @since VERSION
 */


public class CreateThread {
    private static final Log log = LogFactory.getLog(CreateThread.class);
    /**
     * 继承 Thread 复写 run() 方法创建线程
     */
    public static class ExtendThreadClass extends Thread{
        @Override
        public void run() {
            //do sth
            log.info("thread : " + Thread.currentThread().getName());
            System.out.println("extend thread class and override run() method to create thread");
        }
    }

    /**
     * 使用 Runnable 创建线程
     */
    public static class ImplementRunnable implements Runnable {

        @Override
        public void run() {
            log.info("thread : " + Thread.currentThread().getName());
            System.out.println("implement Runnable class and override run() method to create thread");
        }
    }

    /**
     * 使用 Callable 创建线程
     */
    public static class ImplementCallable implements Callable {

        @Override
        public Object call() throws Exception {
            log.info(" callable thread : " + Thread.currentThread().getName());
            return "sth";
        }


    }

    /**
     * Executors 创建线程
     */
    public static class TheadPoolCreateThread {
        public ExecutorService returnThead(){
            return Executors.newFixedThreadPool(1);
        }
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExtendThreadClass extendThreadClass = new ExtendThreadClass();
        extendThreadClass.start();

        ImplementRunnable runnable = new ImplementRunnable();
        new Thread(runnable).start();

        FutureTask task = new FutureTask(new ImplementCallable());
        new Thread(task).start();
        task.get();

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(runnable);

        executorService.shutdownNow();

    }

}
