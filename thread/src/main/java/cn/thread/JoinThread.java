package cn.thread;

public class JoinThread extends Thread {

    int i;
    Thread previousThread; //上一个线程

    public JoinThread(Thread previousThread, int i) {
        this.previousThread = previousThread;
        this.i = i;
    }

    @Override
    public void run() {
//        try {
            //调用上一个线程的join方法，大家可以自己演示的时候可以把这行代码注释掉
//            previousThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.println("num:" + i);
    }

    public static void main(String[] args) throws InterruptedException {
        Thread previousThread = Thread.currentThread();
        for (int i = 0; i < 10; i++) {
            JoinThread joinDemo = new JoinThread(previousThread, i);
            joinDemo.start();
            joinDemo.join();
            previousThread = joinDemo;
        }
    }

}
