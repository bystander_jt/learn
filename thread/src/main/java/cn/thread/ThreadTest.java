package cn.thread;

import java.util.concurrent.*;

/**
 * @author lijiangtao
 * 
 */

@SuppressWarnings("unused")
public class ThreadTest {

    public static void main(String[] args) throws InterruptedException {

//        futureThread();
        joinThread();

    }

    /**
     * 线程状态:
     * <p>→ NEW (新建) → RUNNABLE(运行) → TERMINATED(终止) </p>
     * <p>                             ↓ </p>
     * <p>                 JOIN()      →  WAITING(等待状态) </p>
     * <p>          JOIN(timeout)      →  TIMED_WAITING(等待状态)</p>
     */
    private static void threadState() {
        // 新建状态
        Thread.State newState = Thread.State.NEW;

        // 运行状态
        Thread.State runnable = Thread.State.RUNNABLE;

        // 阻塞状态 等待 wait 指令回复至 运行状态
        Thread.State blocked = Thread.State.BLOCKED;

        // 使用 wait() join() 方法会让线程进如等待状态, 只有通过 notifyAll() 才能使线程恢复
        Thread.State waiting = Thread.State.WAITING;

        // 使用 wait(timeout) join(timeout) 方法会让线程进入等待状态
        Thread.State timeWait = Thread.State.TIMED_WAITING;

        // 线程终止状态
        Thread.State terminated = Thread.State.TERMINATED;

    }

    private static void futureThread() throws ExecutionException, InterruptedException {
        System.out.println("--- STARTED ---");
        ExecutorService service = Executors.newCachedThreadPool();
        CallableTread<Integer> thread = new CallableTread<>();

        System.out.println("--- THREAD STARTED ---");

        // 调用 submit(Callable<V> callable) 并返回参数
        Future<Integer> submit = service.submit(thread);

        System.out.println("--- RESULT : " + submit.get() + "---");

        System.out.println("--- END ---");
    }

    private static void futureTaskThread() throws ExecutionException, InterruptedException, TimeoutException {
        System.out.println("--- STARTED ---");

        ExecutorService service = Executors.newCachedThreadPool();
        FutureTask<Integer> thread = new FutureTask<>(() -> 2);

        System.out.println("--- THREAD STARTED ---");

        // 调用 submit(Runnable ) 不返回参数
        service.submit(thread);

        //阻塞主线程, 在一定时间内 (timeout) 无法回结果抛出 TimeoutException
        System.out.println("--- RESULT : " + thread.get(10, TimeUnit.MINUTES) + "---");

        //阻塞主线程直到结果返回或者异常退出
        System.out.println("--- RESULT : " + thread.get() + "---");

        System.out.println("--- END ---");
    }

    private static void joinThread() throws InterruptedException {
        for (int i = 0; i < 5; i++) {
            int finalI = i;
            Thread thread = new Thread(() -> {
                System.out.println("thread " + finalI + " started");
                System.out.println("thread " + finalI + " end");

            });
            thread.start();
            thread.join();
        }
    }

    private static void waitThread(){

    }

}
