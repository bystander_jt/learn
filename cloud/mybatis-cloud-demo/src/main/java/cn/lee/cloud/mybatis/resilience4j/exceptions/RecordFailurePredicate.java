package cn.lee.cloud.mybatis.resilience4j.exceptions;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Predicate;

@Slf4j
public class RecordFailurePredicate implements Predicate<Throwable> {

    private static final String NAME = RecordFailurePredicate.class.getSimpleName();

    @Override
    public boolean test(Throwable throwable) {
        log.debug(NAME + " test exception: " + throwable.getClass().getSimpleName());
        return false;
    }

    @Override
    public Predicate<Throwable> and(Predicate<? super Throwable> other) {
        log.debug(NAME + " and Predicate: " + other.getClass().getSimpleName());

        return Predicate.super.and(other);
    }

    @Override
    public Predicate<Throwable> negate() {
        return Predicate.super.negate();
    }

    @Override
    public Predicate<Throwable> or(Predicate<? super Throwable> other) {
        return Predicate.super.or(other);
    }

}
