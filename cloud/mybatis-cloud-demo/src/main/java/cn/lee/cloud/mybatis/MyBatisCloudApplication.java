package cn.lee.cloud.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@EnableDiscoveryClient
@SpringBootApplication
public class MyBatisCloudApplication {


    public static void main(String[] args) {
        SpringApplication.run(MyBatisCloudApplication.class, args);
    }

}
