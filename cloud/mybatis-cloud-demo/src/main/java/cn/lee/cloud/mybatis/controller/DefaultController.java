package cn.lee.cloud.mybatis.controller;

import cn.lee.util.Result;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class DefaultController {


    @GetMapping
    public Result get(){
        return Result.ok("service running on " + System.getProperty("PID"));
    }


}
