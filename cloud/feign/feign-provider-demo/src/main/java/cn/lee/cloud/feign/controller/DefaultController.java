package cn.lee.cloud.feign.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/default")
public class DefaultController {


    @GetMapping("/pid")
    public String returnPid() {
        return System.getProperty("PID");
    }

}
