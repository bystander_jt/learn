package cn.lee.cloud.feign.listener;

import cn.lee.cloud.feign.controller.DefaultController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

@Service
public class RequestProvider implements ApplicationRunner {

    @Autowired
    private DefaultController controller;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        for (int i = 0; i < 10; i++) {
            String pid = controller.returnPid();
            System.out.printf("pid: %s\n", pid);

            Thread.sleep(1000);
        }
    }

}
