package cn.lee.cloud.feign.configuration.lb;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.DefaultResponse;
import org.springframework.cloud.client.loadbalancer.EmptyResponse;
import org.springframework.cloud.client.loadbalancer.Request;
import org.springframework.cloud.client.loadbalancer.Response;
import org.springframework.cloud.loadbalancer.core.NoopServiceInstanceListSupplier;
import org.springframework.cloud.loadbalancer.core.ReactorServiceInstanceLoadBalancer;
import org.springframework.cloud.loadbalancer.core.SelectedInstanceCallback;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StopWatch;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.net.InetAddress;
import java.util.List;

public class ShortestPingLoadBalancer implements ReactorServiceInstanceLoadBalancer {

    private static final ServiceInstance NOOP = new DefaultServiceInstance();
    private final String serviceId;
    private final ObjectProvider<ServiceInstanceListSupplier> serviceInstanceProvider;


    public ShortestPingLoadBalancer(ConfigurableApplicationContext context) {
        this.serviceId = context.getEnvironment().getProperty(LoadBalancerClientFactory.PROPERTY_NAME);
        this.serviceInstanceProvider = context.getBeanProvider(ServiceInstanceListSupplier.class);
    }

    public ShortestPingLoadBalancer(String serviceId, ObjectProvider<ServiceInstanceListSupplier> serviceInstanceProvider) {
        this.serviceId = serviceId;
        this.serviceInstanceProvider = serviceInstanceProvider;
    }


    @Override
    public Mono<Response<ServiceInstance>> choose(Request request) {
        // 获取服务提供者
        ServiceInstanceListSupplier supplier = serviceInstanceProvider.getIfAvailable(NoopServiceInstanceListSupplier::new);
        return supplier.get(request).next().map(instance -> processAlgorithm(supplier, instance));
    }


    private Response<ServiceInstance> processAlgorithm(ServiceInstanceListSupplier supplier, List<ServiceInstance> instanceList) {
        ServiceInstance instance = NOOP;
        int minimumResponseTime = Integer.MAX_VALUE;
        for (ServiceInstance serviceInstance : instanceList) {
            int ping = getPing(serviceInstance.getHost(), serviceInstance.getPort());
            if (ping > 0 && ping < minimumResponseTime) {
                minimumResponseTime = ping;
                instance = serviceInstance;
            }
        }
        if (instance == NOOP) {
            return new EmptyResponse();
        }
        if (supplier instanceof SelectedInstanceCallback) {
            ((SelectedInstanceCallback) supplier).selectedServiceInstance(instance);
        }
        return new DefaultResponse(instance);
    }

    private int getPing(String host, int port) throws RuntimeException {
        StopWatch stopWatch = new StopWatch("ping-test");
        try {
            InetAddress address = null;
            address = InetAddress.getByName(host);
            stopWatch.start();
            address.isReachable(500);
        } catch (IOException e) {
            return -1;

        } finally {
            if (stopWatch.isRunning()) stopWatch.stop();
        }
        return (int) stopWatch.getTotalTimeNanos();
    }
}
