package cn.lee.cloud.feign.configuration.lb;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.loadbalancer.config.LoadBalancerZoneConfig;
import org.springframework.cloud.loadbalancer.core.*;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import static org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory.PROPERTY_NAME;

public class StaticLoadBalancerConfiguration {

    public static class RoundRobinLoadBalancerConfiguration {


        @Bean
        ReactorLoadBalancer<ServiceInstance> ribbonLoadBalancer(Environment environment,
                                                                LoadBalancerClientFactory loadBalancerClientFactory) {
            String name = environment.getProperty(PROPERTY_NAME);
            return new RoundRobinLoadBalancer(
                    loadBalancerClientFactory.getLazyProvider(name, ServiceInstanceListSupplier.class),
                    name
            );
        }

    }

    public static class RandomLoadBalancerConfiguration {

        @Bean
        ReactorLoadBalancer<ServiceInstance> randomLoadBalancer(Environment environment,
                                                                LoadBalancerClientFactory loadBalancerClientFactory) {
            String name = environment.getProperty(PROPERTY_NAME);
            return new RandomLoadBalancer(
                    loadBalancerClientFactory.getLazyProvider(name, ServiceInstanceListSupplier.class),
                    name
            );
        }

    }

    public static class ShortestPingLoadBalancerConfiguration {

        @Bean
        public ReactorLoadBalancer<ServiceInstance> shortestPing(ConfigurableApplicationContext context) {
            return new ShortestPingLoadBalancer(context);
        }


    }

    public static class ComplicatedLoadBalanceConfiguration {

        //        @Bean
        public ServiceInstanceListSupplier build(ConfigurableApplicationContext context) {
            
            String individual = "";
            return ServiceInstanceListSupplier.builder()
                    .withZonePreference()
                    .withBlockingDiscoveryClient()
                    .withSameInstancePreference()
                    .build(context);
        }

        @Bean
        ReactorLoadBalancer<ServiceInstance> randomLoadBalancer(Environment environment,
                                                                LoadBalancerClientFactory loadBalancerClientFactory) {
            String name = environment.getProperty(PROPERTY_NAME);
            return new RandomLoadBalancer(
                    loadBalancerClientFactory.getLazyProvider(name, ServiceInstanceListSupplier.class),
                    name
            );
        }


    }

}
