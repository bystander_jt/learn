package cn.lee.cloud.feign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
public class DefaultController {

    @Autowired
    private IDefaultFeignService feignService;

    @GetMapping("/pid")
    public String returnPid() {
        return feignService.getProviderServicePid();
    }

}
