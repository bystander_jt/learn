package cn.lee.cloud.feign.controller;

import cn.lee.cloud.feign.configuration.lb.StaticLoadBalancerConfiguration;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;


@FeignClient(value = "spring-cloud-feign-provider-demo", path = "/default")
//@LoadBalancerClient(name = "spring-cloud-feign-provider-demo", configuration = StaticLoadBalancerConfiguration.RandomLoadBalancerConfiguration.class)
@LoadBalancerClient(name = "spring-cloud-feign-provider-demo", configuration = StaticLoadBalancerConfiguration.ComplicatedLoadBalanceConfiguration.class)
@LoadBalancerClients
public interface IDefaultFeignService {

    @GetMapping("/pid")
    String getProviderServicePid();

}
