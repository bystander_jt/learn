package cn.lee.cloud.feign.algorithm;


import cn.lee.cloud.feign.configuration.lb.ShortestPingLoadBalancer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.DefaultRequest;
import org.springframework.cloud.client.loadbalancer.DefaultResponse;
import org.springframework.cloud.client.loadbalancer.EmptyResponse;
import org.springframework.cloud.client.loadbalancer.Response;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.MapPropertySource;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import static org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory.PROPERTY_NAME;


public class ShortestPingLoadBalancerTest {

    @Test
    public void returnNormal() {
        String serviceId = "normal-test";

        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(factory);

        factory.registerSingleton("service-provider", SimpleServiceProvider.createLocalInstance(serviceId));

        context.getEnvironment().getPropertySources().addFirst(new MapPropertySource(PROPERTY_NAME,
                Collections.singletonMap(PROPERTY_NAME, serviceId)));
        context.refresh();

        ShortestPingLoadBalancer balancer = new ShortestPingLoadBalancer(context);
        Mono<Response<ServiceInstance>> choose = balancer.choose(new DefaultRequest());
        Response<ServiceInstance> response = choose.block();

        assert response instanceof DefaultResponse;

        ServiceInstance server = response.getServer();

        assert server.getHost().equals("0.0.0.0");
    }

    @Test
    public void unreachable() {
        String serviceId = "unreachable";

        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(factory);

        factory.registerSingleton("service-provider", SimpleServiceProvider.createUnReachableProviders(serviceId));

        context.getEnvironment().getPropertySources().addFirst(new MapPropertySource(PROPERTY_NAME,
                Collections.singletonMap(PROPERTY_NAME, serviceId)));
        context.refresh();

        ShortestPingLoadBalancer balancer = new ShortestPingLoadBalancer(context);
        Mono<Response<ServiceInstance>> choose = balancer.choose(new DefaultRequest());
        Response<ServiceInstance> response = choose.block();

        assert response instanceof EmptyResponse;
    }

    public static class SimpleServiceProvider implements ServiceInstanceListSupplier {

        private final String serviceId;

        private final Supplier<List<ServiceInstance>> supplier;

        private SimpleServiceProvider(String serviceId, Supplier<List<ServiceInstance>> supplier) {
            this.serviceId = serviceId;
            this.supplier = supplier;
        }

        public static SimpleServiceProvider createLocalInstance(String serviceId) {
            return new SimpleServiceProvider(serviceId,
                    () -> {
                        List<ServiceInstance> instances = new ArrayList<>(3);
                        for (int i = 0; i < 3; i++) {
                            instances.add(new DefaultServiceInstance(UUID.randomUUID().toString(), serviceId, "0.0.0.0", 0, false));
                        }
                        return instances;
                    });
        }


        public static SimpleServiceProvider createUnReachableProviders(String serviceId) {
            return new SimpleServiceProvider(serviceId,
                    () -> {
                        List<ServiceInstance> instances = new ArrayList<>(3);

                        for (int i = 0; i < 3; i++) {
                            instances.add(new DefaultServiceInstance(UUID.randomUUID().toString(), serviceId, "999.999.999.999", 0, false));
                        }
                        return instances;
                    });
        }

        @Override
        public String getServiceId() {
            return serviceId;
        }

        @Override
        public Flux<List<ServiceInstance>> get() {
            return Flux.just(supplier.get());
        }
    }


}
