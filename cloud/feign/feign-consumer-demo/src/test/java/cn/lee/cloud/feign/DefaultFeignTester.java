package cn.lee.cloud.feign;


import cn.lee.cloud.feign.controller.DefaultController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootTest(classes = CloudFeignConsumerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DefaultFeignTester {

    @Autowired
    private DefaultController controller;


    @Test
    public void getPid() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            String pid = controller.returnPid();
            System.out.printf("pid: %s\n", pid);

            Thread.sleep(1000);
        }
    }

}
