package com.exercise.jvm;

import java.util.ArrayList;

public class StrongReference {

    public static void main(String[] args) {
        Object o1 = new Object();
        Object o2 = o1;

        o1 = null;
        System.gc();

        System.out.println(o1);
        System.out.println(o2);
        ArrayList<Object> objects = new ArrayList<>();
        System.out.println(objects.size());
    }

}
