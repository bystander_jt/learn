package cn.lee.memory.layout;

import org.openjdk.jol.info.ClassLayout;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/12/15 10:58
 * @since VERSION
 */

public class CalcObjectMemoryLayout {

    public static void main(String[] args) {
        System.out.println(ClassLayout.parseClass(ExampleObject.class).toPrintable());
    }

}
