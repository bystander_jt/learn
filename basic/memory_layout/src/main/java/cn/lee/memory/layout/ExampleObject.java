package cn.lee.memory.layout;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/12/15 10:47
 * @since VERSION
 */


public class ExampleObject {

    private Integer filed1;

    private Long field2;

    public Integer getFiled1() {
        return filed1;
    }

    public void setFiled1(Integer filed1) {
        this.filed1 = filed1;
    }

    public Long getField2() {
        return field2;
    }

    public void setField2(Long field2) {
        this.field2 = field2;
    }
}
