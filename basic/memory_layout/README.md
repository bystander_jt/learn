# Memory Layout

该项目主要用于演示如何获取对象的内存结构. 

计算对象
  ```java
public class ExampleObject {

    private Integer filed1;

    private Long field2;

    public Integer getFiled1() {
        return filed1;
    }

    public void setFiled1(Integer filed1) {
        this.filed1 = filed1;
    }

    public Long getField2() {
        return field2;
    }

    public void setField2(Long field2) {
        this.field2 = field2;
    }
}
  ```

## 手动计算
这时我们能够从结构中知道两个变量占用的大小
- `Integer filed1` , 占用 4 字节
- `Long field2`, 占用 8 字节.

## 通过 jol 获取
在复杂的类中我们很难快速计算出这个类新建一个对象所占用的大小.

这时我们通过引入 `jol-core`  依赖, 让工具帮助我们获取指定类生成实例的大小.

### `pom.xml` 引入依赖

```xml
<dependency>
	<groupId>org.openjdk.jol</groupId>
	<artifactId>jol-core</artifactId>
	<version>${jol-core.version}</version>
</dependency>
```

### 代码

```java
public class CalcObjectMemoryLayout {
    public static void main(String[] args) {
        System.out.println(ClassLayout.parseClass(ExampleObject.class).toPrintable());
    }
}
```

通过打印我们就得知了这个类创建的一个对象大小.

```tex
cn.lee.memory.layout.ExampleObject object internals:
OFF  SZ                TYPE DESCRIPTION               VALUE
  0   8                     (object header: mark)     N/A
  8   4                     (object header: class)    N/A
 12   4   java.lang.Integer ExampleObject.filed1      N/A
 16   4      java.lang.Long ExampleObject.field2      N/A
 20   4                     (object alignment gap)    
Instance size: 24 bytes
Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
```

- 包含类的 `MarkWord`

> 更多请自行参考 `ClasLayout` API
