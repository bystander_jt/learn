package cn.design.pattern;

import java.util.List;

/**
 * 线程安全双检查懒汉模式
 * 取消了 线程安全下获取都会升级到重量级锁的情况, 提升了性能
 * 对 {@code singleTon} 进行了 violated 修饰, 避免出现内存屏蔽
 */
public class DoubleCheckLazyMode {


    private DoubleCheckLazyMode() {

    }

    private static volatile DoubleCheckLazyMode singleTon = null;

    public static DoubleCheckLazyMode getSingleTon() {
        if (singleTon == null) {
            synchronized (DoubleCheckLazyMode.class  ) {
                if (singleTon == null) {
                    singleTon = new DoubleCheckLazyMode();
                }
            }
        }
        return singleTon;
    }


}
