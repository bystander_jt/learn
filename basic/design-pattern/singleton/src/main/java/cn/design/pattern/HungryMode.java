package cn.design.pattern;

/**
 * 饿汉模式
 * 如果单例对象的创建过程比较耗时，那么应用程序的启动将会比较慢。
 */
public class HungryMode {

    /**
     * 隐藏构造器
     */
    private HungryMode() { }

    /**
     * 对对象进行初始化
     */
    private  static HungryMode singleTon = new HungryMode();

    /**
     * 返回对象
     * @return
     */
    public static HungryMode getSingleTon(){
        return singleTon;
    }


}

