package cn.design.pattern;

/**
 * 线程安全懒汉模式
 * 避免多线程情况下会出现创建多个实例. 但同样会浪费性能, 无论怎么样获取都需要加重量级锁.
 */
public class ThreadSafeLazyMode {


    private ThreadSafeLazyMode() {

    }

    private static ThreadSafeLazyMode singleTon = null;

    public synchronized static ThreadSafeLazyMode getSingleTon() {
        if (singleTon == null) {
            singleTon = new ThreadSafeLazyMode();
        }
        return singleTon;
    }


}
