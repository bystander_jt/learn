package cn.design.pattern;

/**
 * 懒汉模式
 * 多线程情况下会出现创建多个实例
 * 如果线程1 获取对象发现对象不存在, 会触发新建对象的操作
 * 如果线程2 同样需要获取, 则会出现对象也不存在, 同样需要新建对象, 这样就会出现多个对象. 单例的意义就消失了
 */
public class LazyMode {


    private LazyMode() {

    }

    private static LazyMode singleTon = null;

    public static LazyMode getSingleTon() {
        if (singleTon == null) {
            singleTon = new LazyMode();
        }
        return singleTon;
    }


}
