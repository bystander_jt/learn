package cn.lee.reflection;

public class ReflectionClass {


    public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        System.out.println("direct get class");
        Class<ObjectClass> aClass = ObjectClass.class;
        System.out.println();

        System.out.println("class for name get Class");
        Class<?> aClass1 = Class.forName("cn.lee.reflection.ObjectClass");
        System.out.println();

        System.out.println("current classloader load Class");
        Class<?> classloaderGetClass = ReflectionClass.class.getClassLoader().loadClass("cn.lee.reflection.ObjectClass");
        System.out.println();
    }

}
