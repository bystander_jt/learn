package cn.lee.proxy.pojo.proxy;

import cn.lee.proxy.pojo.IDynamicProxy;

public class DynamicProxy implements IDynamicProxy {

    @Override
    public void proxy() {
        System.out.println("somebody invoke proxy method !");
    }

}
