package cn.lee.proxy.pojo.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyCreator implements InvocationHandler {


    private Object proxyObject;


    /**
     * 创建代理类 {@code proxyTarget} 为原始对象. 通过原始对象获取 ClassLoader 和 Interface
     * @param proxyTarget 被代理对象
     * @return 代理对象, 所有方法都会走下方的 invoke() 方法
     */
    public Object createProxyInstance(Object proxyTarget) {
        this.proxyObject = proxyTarget;
        return Proxy.newProxyInstance(
                proxyTarget.getClass().getClassLoader(),
                proxyTarget.getClass().getInterfaces(),
                this
        );
    }

    /**
     * 代理类对象方法调用, 自动完成, 不需要人为改变. </p>
     * 下方实现了在 方法调用前 和 方法调用后 打印数据, 可以将打印数据更改为其他业务逻辑代码
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("before proxy ...");
        Object result = method.invoke(proxyObject, args);
        System.out.println("after proxy ...");
        return result;
    }
}
