package cn.lee.proxy;

import cn.lee.proxy.pojo.IDynamicProxy;
import cn.lee.proxy.pojo.proxy.DynamicProxy;
import cn.lee.proxy.pojo.proxy.ProxyCreator;

import java.lang.reflect.Proxy;

public class StaticProxy {

    public static void main(String[] args) {
        // 开启保存生成字节码文件
        System.setProperty("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        DynamicProxy origial = new DynamicProxy();

        ProxyCreator proxyCreator = new ProxyCreator();
        IDynamicProxy proxyInstance = (IDynamicProxy)proxyCreator.createProxyInstance(origial);
        proxyInstance.proxy();
    }


}
