package cn.lee.proxy.pojo;


public class OperationDO {

    private String operation;

    public OperationDO(String operation) {
        this.operation = operation;
    }

    public OperationDO() {
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
