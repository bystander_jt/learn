package cn.lee.proxy;

import cn.lee.proxy.interceptor.Interceptor;
import cn.lee.proxy.pojo.OperationDO;
import net.sf.cglib.beans.ImmutableBean;
import net.sf.cglib.core.DefaultGeneratorStrategy;
import net.sf.cglib.proxy.Enhancer;


public class CgLib {

    public static void main(String[] args) {
//        cglibProxyObject();
        cglibImmutableBean();
    }

    private static void cglibProxyObject(){
        // 创建一个 Enhance 对象
        Enhancer enhancer = new Enhancer();
        // 设置 enhance 需要被代理的类
        enhancer.setSuperclass(OperationDO.class);
        // 创建一个拦截器
        enhancer.setCallback(new Interceptor());

        enhancer.setCallbackFilter(method -> 0);
        enhancer.setStrategy(new DefaultGeneratorStrategy());
        // 创建一个增强对象
        OperationDO o = (OperationDO) enhancer.create();
        // 调用代理类方法
        o.getOperation();
    }

    /**
     * 创建不可变对象</p>
     * 如果需要设定不可变对象已经被加强过, 会无法找到构造类.
     */
    private static void cglibImmutableBean(){

        OperationDO immutableBean = new OperationDO("Immutable Bean");
        OperationDO immutable =(OperationDO) ImmutableBean.create(immutableBean);
        immutable.setOperation("modify bean");

    }

}
