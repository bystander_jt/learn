package cn.lee.lck.synchronize;

public class SynchronizedTest {




    public static void main(String[] args) {
        SynchronizeLock lock = new SynchronizeLock();

         new Thread(lock::methodLevelObjectLock, "method thread - ").start();
//         new Thread(lock::objectLock, "object thread - ").start();
         new Thread(SynchronizeLock::staticLock, "static thread - ").start();
//        new Thread(lock::classLock, "class thread - ").start();

    }


}
