package cn.lee.lck.synchronize;

/**
 * 对象锁, 下方两个锁都会获取到同一把锁.
 * 如果任意去掉其中一个 synchronize, 去掉修饰符的线程将会放弃竞争. 对多线程开发增加了隐患
 */
public class SynchronizeLock {

    /**
     * 对象锁, 锁在方法体内.
     * 非　static 对象.
     */
    public synchronized void methodLevelObjectLock() {
        try {
            System.out.println("name = " + Thread.currentThread().getName() + ", begain");
            for (int i = 0; i < 5; i++) {
                System.out.printf(" thread name : %s , loop in : %d \n", Thread.currentThread().getName(), i);
                Thread.sleep(10);
            }
            System.out.println("name = " + Thread.currentThread().getName() + ", end");
        } catch (InterruptedException ignore) {

        }
    }

    /**
     * 对象锁, 锁在方法体内.
     * 非　static 对象.
     */
    public void objectLock() {
        synchronized (this) {
            try {
                System.out.println("name = " + Thread.currentThread().getName() + ", begain");
                for (int i = 0; i < 5; i++) {
                    System.out.printf(" thread name : %s , loop in : %d \n", Thread.currentThread().getName(), i);
                    Thread.sleep(10);
                }
                System.out.println("name = " + Thread.currentThread().getName() + ", end");
            } catch (InterruptedException ignore) {

            }
        }
    }

    /**
     * 类锁, 锁在方法体内.
     * static 对象, .
     */
    public static synchronized void staticLock() {
        try {
            System.out.println("name = " + Thread.currentThread().getName() + ", begain");
            for (int i = 0; i < 5; i++) {
                System.out.printf(" thread name : %s , loop in : %d \n", Thread.currentThread().getName(), i);
                Thread.sleep(10);
            }
            System.out.println("name = " + Thread.currentThread().getName() + ", end");
        } catch (InterruptedException ignore) {

        }
    }


    /**
     * 类锁, 锁在方法体内.
     * static 对象, .
     */
    public void classLock() {
        synchronized (SynchronizeLock.class) {
            try {
                System.out.println("name = " + Thread.currentThread().getName() + ", begain");
                for (int i = 0; i < 5; i++) {
                    System.out.printf(" thread name : %s , loop in : %d \n", Thread.currentThread().getName(), i);
                    Thread.sleep(10);
                }
                System.out.println("name = " + Thread.currentThread().getName() + ", end");
            } catch (InterruptedException ignore) {

            }
        }
    }

}
