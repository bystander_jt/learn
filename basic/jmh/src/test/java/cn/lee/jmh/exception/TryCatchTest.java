package cn.lee.jmh.exception;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;


@Threads(3)
@Warmup(iterations = 1)
@BenchmarkMode(Mode.All)
@OutputTimeUnit(TimeUnit.SECONDS)
@Measurement()
public class TryCatchTest {

    @Benchmark
    @Fork(1)
    @BenchmarkMode({Mode.All})
    public void try_catch_finally() {
        int a = 0;
        try {
            ++a;
            throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {

        } finally {
            a = 0;
        }
    }

    @Benchmark
    public void try_catch() {
        int a = 0;
        try {
            ++a;
            throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            a = 0;
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(TryCatchTest.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }

}
