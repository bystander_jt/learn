package cn.lee.algorithm.throttling.window.key;

public enum RedisKeyEnums {


    LIMIT;


    public String getKey(String... keys) {
        return this.name() + ":" + String.join(":", keys);
    }

}
