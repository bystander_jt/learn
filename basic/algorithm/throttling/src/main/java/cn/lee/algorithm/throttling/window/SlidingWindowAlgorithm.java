package cn.lee.algorithm.throttling.window;

import cn.lee.algorithm.throttling.window.key.RedisKeyEnums;
import lombok.Getter;
import lombok.Setter;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.HostAndPortMapper;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisClientConfig;

import java.time.LocalTime;
import java.util.concurrent.atomic.AtomicInteger;

public class SlidingWindowAlgorithm {


    public static void main(String[] args) throws InterruptedException {
        run();

    }

    public static void run() throws InterruptedException {
        int qps = 2, count = 20, sleep = 300, success = count * sleep / 1000 * qps;
        System.out.println(String.format("当前QPS限制为:%d,当前测试次数:%d,间隔:%dms,预计成功次数:%d", qps, count, sleep, success));
        success = 0;
        RateLimiterSlidingWindow myRateLimiter = new RateLimiterSlidingWindow(qps);
        for (int i = 0; i < count; i++) {
            Thread.sleep(sleep);
            if (myRateLimiter.tryAcquire()) {
                success++;
                if (success % qps == 0) {
                    System.out.println(LocalTime.now() + ": success, ");
                } else {
                    System.out.print(LocalTime.now() + ": success, ");
                }
            } else {
                System.out.println(LocalTime.now() + ": fail");
            }
        }
        System.out.println();
        System.out.println("实际测试成功次数:" + success);
    }

    @Getter
    @Setter
    public static class RateLimiterSlidingWindow {
        /**
         * 阈值
         */
        private int qps = 2;
        /**
         * 时间窗口总大小（毫秒）
         */
        private long windowSize = 1000;
        /**
         * 多少个子窗口
         */
        private Integer windowCount = 10;
        /**
         * 窗口列表
         */
        private WindowInfo[] windowArray = new WindowInfo[windowCount];

        public RateLimiterSlidingWindow(int qps) {
            this.qps = qps;
            long currentTimeMillis = System.currentTimeMillis();
            for (int i = 0; i < windowArray.length; i++) {
                windowArray[i] = new WindowInfo(currentTimeMillis, new AtomicInteger(0));
            }
        }

        /**
         * 1. 计算当前时间窗口
         * 2. 更新当前窗口计数 & 重置过期窗口计数
         * 3. 当前 QPS 是否超过限制
         *
         * @return
         */
        public synchronized boolean tryAcquire() {
            long currentTimeMillis = System.currentTimeMillis();
            // 1. 计算当前时间窗口
            int currentIndex = (int) (currentTimeMillis % windowSize / (windowSize / windowCount));
            // 2.  更新当前窗口计数 & 重置过期窗口计数
            int sum = 0;
            for (int i = 0; i < windowArray.length; i++) {
                WindowInfo windowInfo = windowArray[i];
                if ((currentTimeMillis - windowInfo.getTime()) > windowSize) {
                    windowInfo.getNumber().set(0);
                    windowInfo.setTime(currentTimeMillis);
                }
                if (currentIndex == i && windowInfo.getNumber().get() < qps) {
                    windowInfo.getNumber().incrementAndGet();
                }
                sum = sum + windowInfo.getNumber().get();
            }
            // 3. 当前 QPS 是否超过限制
            return sum <= qps;
        }

        @Getter
        @Setter
        private class WindowInfo {
            // 窗口开始时间
            private Long time;
            // 计数器
            private AtomicInteger number;

            public WindowInfo(long time, AtomicInteger number) {
                this.time = time;
                this.number = number;
            }
        }
    }

    public class RedisSlidWindow {

        JedisClientConfig config;

        private Jedis jedis;
        private int qps;
        private int windowSize;

        public RedisSlidWindow(int qps, int windowSize) {
            HostAndPort hostAndPort = new HostAndPort("192.168.247.128", 6379);
            config = new JedisClientConfig() {
                @Override
                public HostAndPortMapper getHostAndPortMapper() {
                    return hap -> hostAndPort;
                }

                @Override
                public String getPassword() {
                    return "";
                }

                @Override
                public int getDatabase() {
                    return 0;
                }

                @Override
                public boolean isSsl() {
                    return false;
                }
            };
            jedis = new Jedis(hostAndPort, config);
            jedis.connect();

            this.qps = qps;
            this.windowSize = windowSize;
        }

        public boolean tryAcquire(){
            long time = System.currentTimeMillis();
            int index = (int) (time % windowSize);

//            jedis.z(getLimitedKey(index));

            return false;
        }

        private String getLimitedKey(int index){
            return RedisKeyEnums.LIMIT.getKey(String.valueOf(index));
        }
    }

    public class LocalSlidingWindow{

        public class Node{

            private long time;

        }

    }


}
