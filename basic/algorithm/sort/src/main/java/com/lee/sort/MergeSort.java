package com.lee.sort;

import java.util.Arrays;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/15 17:08
 * @since VERSION
 */

public class MergeSort extends Base {


    public static void main(String[] args) {
        MergeSort mergeSort = new MergeSort();
//        int[] array = mergeSort.createArray(10);
//
//        process(array, 0, array.length - 1);
//
//        System.out.println(Arrays.toString(array));

        int[] array = new int[]{1,4,3,2,5};
        int sum = new SmallSum().sum(array);
        System.out.println(sum);

//        int[] sort = mergeSort.sort(array);
//        mergeSort.print(sort);
    }

    private static void process(int[] arr, int l, int r) {
        if (l == r) return;
        int mid = l + ((r - l) >> 1);
        process(arr, l, mid);
        process(arr, mid + 1, r);
        mergeSort(arr, l, mid, r);
    }

    public static void mergeSort(int[] arr, int l, int mid, int r) {
        int[] tempArr = new int[r - l + 1];

        int i = 0;
        int t1 = l;
        int t2 = mid + 1;

        while (t1 <= mid && t2 <= r) {
            tempArr[i++] = arr[t1] <= arr[t2] ? arr[t1++] : arr[t2++];
        }

        while (t1 <= mid) {
            tempArr[i++] = arr[t1++];
        }

        while (t2 <= r) {
            tempArr[i++] = arr[t2++];
        }

        for (int j = 0; j < tempArr.length; j++) {
            arr[l + j] = tempArr[j];
        }

        tempArr = null;

    }

    public int[] sort(int[] sourceArray) {
        // 对 arr 进行拷贝，不改变参数内容
        int[] arr = Arrays.copyOf(sourceArray, sourceArray.length);

        if (arr.length < 2) {
            return arr;
        }
        int middle = (int) Math.floor(arr.length / 2);

        int[] left = Arrays.copyOfRange(arr, 0, middle);
        int[] right = Arrays.copyOfRange(arr, middle, arr.length);

        return merge(sort(left), sort(right));
    }

    protected int[] merge(int[] left, int[] right) {
        int[] result = new int[left.length + right.length];
        int i = 0;
        while (left.length > 0 && right.length > 0) {
            if (left[0] <= right[0]) {
                result[i++] = left[0];
                left = Arrays.copyOfRange(left, 1, left.length);
            } else {
                result[i++] = right[0];
                right = Arrays.copyOfRange(right, 1, right.length);
            }
        }

        while (left.length > 0) {
            result[i++] = left[0];
            left = Arrays.copyOfRange(left, 1, left.length);
        }

        while (right.length > 0) {
            result[i++] = right[0];
            right = Arrays.copyOfRange(right, 1, right.length);
        }

        return result;
    }


    /**
     * 小和问题
     * 在一个数组中， 将每一个数左边比当前数小的数累加起来， 叫做数组的小和。
     * 如下是题解！
     * <pre>
     *     例如输入: [1,3,4,2,5]
     *     <ol>
     *         <li>对于 1 由于左边没有比它小的数, 所以无需求和, 结果 = 0</li>
     *         <li>对于 3 由于左边有比它小的数, 所以将其左边的数进行求和, 结果 = 1</li>
     *         <li>对于 4 , [1,3] 小于它, 结果 = 1 + (1 + 3)</li>
     *         <li>对于 2 , [1,3,4] 中只有 [1] 小于它, 结果 = 5 + 1</li>
     *         <li>对于 5 , [1,3,4, 2] 都小于它, 结果 = 6 + (1 + 3 + 4 + 2)</li>
     *         <li>结果 : 16</li>
     *     </ol>
     *
     *     归并排序则是将其进行拆分计算, 并不会漏算。
     *     可能会比较担心一个问题： 我把它拆分掉计算是可以，那排序完之后计算结果会不会发生变化 ？
     *     答：结果依旧， 在我们拆分成一个区域时， 我们已经将这个区域中的小和计算出来了， 那我们就还缺少了另一个区域中的小和计算，
     *     只要把另一个区域，让他们从第一个比较我们第一个， 只要我们比他大， 那我们这个区域后面都是比它大的数，那就直接比较值x 比它大的数的个数 = 小和数
     * </pre>
     * <a href="https://www.bilibili.com/video/BV13g41157hK?p=4&vd_source=426e4c134e4bfb91d86ae0d98001f263" >VIDEO 1:07:00</a>
     */
    static class SmallSum extends Base {

        public int sum(int[] arr) {
            System.out.println("input array: " + Arrays.toString(arr));
            return process(arr, 0, arr.length - 1);
        }

        private int process(int[] arr, int l, int r) {
            if (l == r) return 0;

            int mid = l + ((r - l) >> 1);
            int left = process(arr, l, mid);
            int right = process(arr, mid + 1, r);
            return left + right + merge(arr, l, mid, r);
        }

        private int merge(int[] arr, int l, int mid, int r) {
            int[] ordered = new int[r - l + 1];

            int index = 0;
            int result = 0;
            int t1 = l;
            int t2 = mid + 1;

            while (t1 <= mid && t2 <= r) {
                // 由于数组都被排列完整, 所以可以直接计算出 t1 大于还是小于 t2.
                // 如果算出 t1 < t2, 那 t2 的数列都顺序排序完成了, 直接可以通过长度计算出要多加多少个 t1
                result += arr[t1] < arr[t2] ? (r - t2 + 1) * arr[t1] : 0;
                ordered[index++] = arr[t1] < arr[t2] ? arr[t1++] : arr[t2++];
            }

            while (t1 <= mid) {
                ordered[index++] = arr[t1++];
            }

            while (t2 <= r) {
                ordered[index++] = arr[t2++];

            }

            for (int i = 0; i < ordered.length; i++) {
                arr[l + i] = ordered[i];
            }


            return result;
        }


    }


    /**
     * 逆序对
     * 在一个数组中, 左边的数如果比右边的数打, 则这两个数构成一个逆序对.
     */
    static class ReversePair extends Base {

    }


}
