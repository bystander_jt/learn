package com.lee.sort;

import java.util.Arrays;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/15 10:14
 * @since VERSION
 */

public class BucketSort extends Base {


    public static void main(String[] args) {
        BucketSort base = new BucketSort();
        int[] array = base.createArray(10);
        new BucketSortV1().sort(array);
    }

    public int[] bucketSort(int[] array) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < array.length; ++i) {
            if (array[i] > max) {
                max = array[i];
            } else if (array[i] < min) {
                min = array[i];
            }
        }

        int bucketSize = (int) (Math.floor((max - min) / 10) + 1);
        ;
        int[][] bucket = new int[bucketSize][];

        for (int i = 0; i < array.length; ++i) {

            int value = array[i];
            int index = (int) Math.floor((value - min) / bucketSize);
            int[] slot = bucket[index];

            if (slot == null) {
                slot = new int[10];
                slot[1] = value;
                slot[0] = 1;
                bucket[index] = slot;
                continue;
            }

            if (slot[0] == slot.length - 1) {
                slot = Arrays.copyOf(slot, slot.length + (slot.length >> 1));
                bucket[index] = slot;
            }

            int left = 1;
            int right = slot[0] + 1;

            while (left < right) {
                int mid = (left + right) / 2;
                if (slot[mid] <= value) {
                    left = mid + 1;
                } else {
                    right = mid;
                }
            }


            if (slot[left] == 0) {
                slot[left] = value;
                slot[0] = slot[0] + 1;
            } else {
                System.arraycopy(slot, left, slot, left + 1, slot.length - left - 1);
                slot[left] = value;
                slot[0] = slot[0] + 1;
            }


//            for (int j = left; j < slot.length; j++) {
//                if (slot[j] == 0) {
//                    slot[j] = value;
//                    slot[0] = slot[0] + 1;
//                    break;
//                }
//                if (slot[j] > value) {
//                    System.arraycopy(slot, j, slot, j + 1, slot.length - j - 1);
//                    slot[j] = value;
//                    slot[0] = slot[0] + 1;
//                    break;
//                }
//            }
        }


        int[] result = new int[array.length];
        int index = 0;
        for (int i = 0; i < bucket.length; i++) {
            int[] slot = bucket[i];
            if (slot == null) continue;
            for (int j = 1; j < slot[0] + 1; j++) {
                result[index++] = slot[j];
            }

        }


        return result;
    }

    static class BucketSortV1 extends Sort {


        @Override
        void sortAlgorithm(int[] arr) {
            int digits = findDigits(arr);
            int[] t = new int[arr.length];


            int i = 0;
            while (i < digits) {
                int[] bucket = new int[10];
                for (int j = 0; j < arr.length; j++) {
                    int a = arr[j];
                    int b = ((int) (a / Math.pow(10, i))) % 10;
                    bucket[b] = bucket[b] + 1;
                }

                for (int j = 1; j < bucket.length; j++) {
                    bucket[j] = bucket[j - 1] + bucket[j];
                }


                for (int j = arr.length - 1; j >= 0; j--) {
                    int digit = ((int) (arr[j] / Math.pow(10, i))) % 10;
                    t[bucket[digit] - 1] = arr[j];
                    bucket[digit] = bucket[digit] - 1;
                }

                for (int j = 0; j < arr.length; j++) {
                    arr[j] = t[j];
                }
                i++;
            }
        }


        private int findDigits(int[] arr) {
            int max = Integer.MIN_VALUE;
            for (int i : arr) {
                max = Math.max(max, i);
            }

            int digits = 0;
            while (max > 0) {
                max /= 10;
                ++digits;
            }
            return digits;
        }

    }


}
