package com.lee.sort;

public class QuickSort {

    public static void main(String[] args) {
        int[][] testData = new int[3][];
        testData[0] = new int[]{3, 3, 2, 2, 1, 1};
        testData[1] = new int[]{3, 2, 1, 5, 6, 4};


        QuickSortV1 qsv1 = new QuickSortV1();
        QuickSortV2 qsv2 = new QuickSortV2();
        QuickSortV3 qsv3 = new QuickSortV3();

        for (int[] array : testData) {
//            qsv1.sort(array);
            qsv2.sort(array);
//            qsv3.sort(array);
        }

    }


    static class QuickSortV1 extends Sort {
        /**
         * 快速排序 v1.0
         * 从数组尾部中获取数据, 然后进行快速排序.
         *
         * <pre>
         * -----------------------------
         * |   <= num |  > num   | num |
         * -----------------------------
         * </pre>
         * <p>
         * <ol>
         *     <li>从传入范围中的尾部获取比较值</li>
         *     <li><b>设定初始变量 i, 并将其设置为传入范围的初始点 {@code head}</b></li>
         *     <li>定义2个变量 l(<区间 [start, l]), r(大于区间 [, tail - 1])</li>
         *     <li>设定循环, 从 i 位置开始比较数组尾部(arr[tail])
         *          <pre>
         *             {@code
         *              if (arr[i] < arr[tail]) {
         *                   swap(arr, i, l); // 和最小区间的最后一个进行替换
         *                  ++i;
         *                  ++l; // 更新小区间
         *              } else if (arr[i] == arr[tail]){
         *                  ++i; // 不更新任何区间
         *              } else if (arr[i] > arr[tail]) {
         *                   swap(arr, i, --end); // 将当前值和尾部区间值进行替换, 并更新最大区间. 由于尾部替换过来的值并没有计算, 所以不更新 i.
         *              }
         *             }
         *             </pre>
         *             </li>
         *
         * </ol>
         * <p><b>时间复杂度: O(N²)</b></p>
         *
         * @param arr arr
         */
        private void sort(int[] arr, int head, int tail) {


            if (head >= tail) return;

            int l = head, end = tail;

            /* !!! 注意你的下标位置, 如果下标位置错误, 会将已经排序好的位置打乱 !!!*/
            int i = l;

            int target = arr[end];

            // 当小于区(l) == 大于区(end) 时, 代表查询结束
            while (i < end) {

                /* 如果小于， 则将该值与小于区中的最后一个值进行替换 */
                /* 并将小于区域范围进行更新 */
                if (arr[i] < target) {
                    swap(arr, i++, l++);
                }
                /* = 则不进行操作, 更新比较指针*/
                else if (arr[i] == target) {
                    ++i;
                }
                /* 大于时, 将值和大于区域中的值进行替换, 并更新区域, 由于替换过来的数并没有计算, 所以不更新 i */
                else {
                    swap(arr, i, --end);
                }
            }

            // 将最后面比较的数值替换到 > 区域的第一个.
            swap(arr, end, tail);

            /*
             * 0-4 5-9
             * 0-2 3-4 5-7 8-9
             * */

            sort(arr, head, end - 1);
            sort(arr, end + 1, tail);

        }

        @Override
        void sortAlgorithm(int[] arr) {
            sort(arr, 0, arr.length - 1);
        }
    }

    static class QuickSortV2 extends Sort {
        /**
         * quick sort version 2.0
         *
         * <p> v2.0 主要将 == 的放在中间，并部将其进行排序操作.
         * 通过设定左右区间 (<] | [==] | [>) 减少数据排序过程。
         * <p><b>时间复杂度: O(N²)</b></p>
         *
         */
        private void sort(int[] arr, int head, int tail) {
            if (head >= tail) return;

            // 定义左右区间, 区间内的值都需要进行排序
            int lBarrier = head - 1, rBarrier = tail, i = head;

            // 获取目标元素进行排序
            int target = arr[tail];

            // 定义循环范围, 当 i >= rBarrier 时即结束.
            // rBarrier 之后的都是已经进行计算过比 target 数值大的.
            while (i < rBarrier) {

                if (arr[i] < target) {
                    swap(arr, i++, ++lBarrier);
                }  else if (arr[i] > target) {
                    swap(arr, i, --rBarrier);
                } else {
                    ++i;
                }
            }

            // v2.0 区别, 将大于区域的第一个和要计算的值进行替换, 插入至数组中间
            swap(arr, rBarrier, tail);

            // 通过两个屏障实现隔离出两个待排序的数组范围.
            // 不能将当前 target 下标存在下方任意范围内.
            sort(arr, head, lBarrier);
            sort(arr, rBarrier + 1, tail);

        }

        @Override
        void sortAlgorithm(int[] arr) {
            sort(arr, 0, arr.length - 1);
        }
    }

    static class QuickSortV3 extends Sort {


        @Override
        void sortAlgorithm(int[] arr) {
            sort(arr, 0, arr.length - 1);
        }

        private void sort(int[] arr, int head, int tail) {
            if (head >= tail) return;
            swap(arr, head + (int) (Math.random() * (tail - head + 1)), tail);
            int[] partition = partition(arr, head, tail);
            sort(arr, head, partition[0] - 1);
            sort(arr, partition[1] + 1, tail);
        }

        private int[] partition(int[] arr, int head, int tail) {
            int lB = head - 1;
            int rB = tail;
            while (head < rB) {
                if (arr[head] < arr[tail]) {
                    swap(arr, ++lB, head++);
                } else if (arr[head] > arr[tail]) {
                    swap(arr, --rB, head);
                } else head++;
            }
            swap(arr, rB, tail);
            return new int[]{lB + 1, rB};
        }
    }


    static class QuickSortDutchFlag extends Sort {

        @Override
        void sortAlgorithm(int[] arr) {
            dutchFlag(arr, 5);
        }


        /**
         * 荷兰旗, 需要将数组中的元素按照指定的数字进行区分
         * < num 的放 num 左边,
         * > num 的放 num右边,
         * = num 的放中间.
         * <pre>
         *     在荷兰旗问题中. 遵循下方规律即可完成
         *     <ol>
         *         <li>when [i] < target, then [i] 和 < 区域的下一个做交换, < 区域向右侧移动, i++</li>
         *         <li>when [i] == num, then i++</li>
         *         <li>when [i] > num, i 和大于区域的前一个元素进行替换, > 区域向左收紧</li>
         *         <li>当 i == > 区域指针时, 退出</li>
         *     </ol>
         * </pre>
         *
         * @param arr    传入数据
         * @param target 目标
         */
        @SuppressWarnings("SameParameterValue")
        private void dutchFlag(int[] arr, int target) {

            // 定义左侧小于区间
            int leftSelection = 0;
            // 定义右侧大于区间
            int rightSelection = arr.length - 1;
            //
            int i = 0;
            int firstTarget = 0;

            while (i < rightSelection) {

                // 如果当前的数 < target ,
                if (arr[i] < target) {

                    if (leftSelection != i) {
                        arr[i] = arr[i] ^ arr[leftSelection];
                        arr[leftSelection] = arr[i] ^ arr[leftSelection];
                        arr[i] = arr[i] ^ arr[leftSelection];
                    }


                    ++leftSelection;
                    ++i;
                } else if (arr[i] == target) {
                    ++i;
                } else if (arr[i] > target) {
                    arr[i] = arr[i] ^ arr[rightSelection];
                    arr[rightSelection] = arr[i] ^ arr[rightSelection];
                    arr[i] = arr[i] ^ arr[rightSelection];
                    --rightSelection;
                }
            }
        }
    }


}
