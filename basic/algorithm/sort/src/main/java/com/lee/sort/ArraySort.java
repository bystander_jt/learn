package com.lee.sort;

import java.util.Arrays;
import java.util.Random;

public class ArraySort {


    public static void main(String[] args) {
        new ArraySort().unorderedArrayFindMaximum();
    }


    public void unorderedArrayFindMaximum() {

        int[] array = createArray(10);
        System.out.println(Arrays.toString(array));
        int max = findMax(array, 0, array.length - 1);

        System.out.println(max);


    }

    private int findMax(int[] arr, int l, int r) {

        if (l == r) return arr[l];

        int mid = l + ((r - l) >> 1);

        int leftMax = findMax(arr, l, mid);
        int rightMax = findMax(arr, mid + 1, r);

        return Math.max(leftMax, rightMax);
    }

    private int[] createArray(int size) {
        int[] array = new int[size];
        Random random = new Random(50);
        for (int i = 0; i < array.length; ++i) {
            array[i] = random.nextInt(1000);
        }
        return array;
    }

}
