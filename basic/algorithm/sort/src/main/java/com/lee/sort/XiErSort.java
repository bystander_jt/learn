package com.lee.sort;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/15 16:44
 * @since VERSION
 */

public class XiErSort extends Base{


    public static void main(String[] args) {
        XiErSort xiErSort = new XiErSort();
        int[] array = xiErSort.createArray(50);
        xiErSort.sort(array);

    }

    public void sort(int[] arr){
        int length = arr.length;
        int temp;
        for (int step = length / 2; step >= 1; step /= 2) {
            for (int i = step; i < length; i++) {
                temp = arr[i];
                int j = i - step;
                while (j >= 0 && arr[j] > temp) {
                    arr[j + step] = arr[j];
                    j -= step;
                }
                arr[j + step] = temp;
            }
        }
    }

}
