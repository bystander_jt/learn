package com.lee.sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/15 16:04
 * @since VERSION
 */

public class BubbleSort {

    public static void main(String[] args) {
        BubbleSort sort = new BubbleSort();
        List<Integer> array = sort.create();
        sort.bubbleSort(array);

        for (Integer integer : array) {
            System.out.print(integer + " ");
        }
    }

    private void bubbleSort(List<Integer> array) {

        for (int i = 0; i < array.size(); i++) {
            for (int j = 0; j < array.size() - 1 - i; j++) {
                if (array.get(j) > array.get(j + 1)) {
                    Integer v = array.get(j);
                    array.set(j, array.get(j + 1));
                    array.set(j + 1, v);
                }
            }
        }


    }

    public List<Integer> create() {
        List<Integer> array = new ArrayList(50);
        Random random = new Random(50);
        for (int i = 0; i < 50; ++i) {
            int i1 = random.nextInt(1000);
            array.add(i1);
            System.out.print(i1 + " ");
        }
        System.out.println();
        return array;
    }


}
