package com.lee.sort;

public class ValueSwitch {

    public static void main(String[] args) {
        int[] arr = new int[]{3,2,1,5,6,4};
        new ValueSwitch().quickSort(arr,0, arr.length - 1);
    }

    private static void xorSwitch(){

        int a = 100;
        int b = 300;

        a = a ^ b;
        b = a ^ b;
        a = a ^ b;

        System.out.println(a + " " + b);
    }



    private void quickSort(int[] arr, int head, int tail) {

        if (head >= tail) return;

        int lBarrier = head, rBarrier = tail - 1, index = head;
        int target = arr[tail];


        while (lBarrier <= rBarrier || index <= rBarrier){
            if (arr[index] < target) {
                swap(arr, lBarrier++, index++);
            } else if (arr[index] == target) {
                ++index;
            } else if (arr[index] > target) {
                swap(arr, index, rBarrier--);
            }
        }


        swap(arr, rBarrier + 1, tail);

        quickSort(arr, head, --lBarrier);
        quickSort(arr, ++rBarrier, tail);
    }

    private void swap(int[] arr, int a, int b){
        if (a == b) return;
        arr[a] = arr[a] ^ arr[b];
        arr[b] = arr[a] ^ arr[b];
        arr[a] = arr[a] ^ arr[b];
    }
}
