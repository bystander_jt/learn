package com.lee.sort;

import java.util.Arrays;
import java.util.Random;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @since VERSION
 */

public class Base {

    public int[] createArray(int size){
        return createArray(size, 10);
    }

    public int[] createArray(int size, int step) {
        int[] array = new int[size];
        Random random = new Random(step);
        for (int i = 0; i < array.length; ++i) {
            array[i] = random.nextInt(step);
        }
        System.out.println("Unordered Array: " + Arrays.toString(array));
        return array;
    }


    public void print(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");

        }
        System.out.println();
    }


    public void swap(int[] arr, int a, int b) {
        if (a == b) return;
        arr[a] = arr[a] ^ arr[b];
        arr[b] = arr[a] ^ arr[b];
        arr[a] = arr[a] ^ arr[b];
    }

}
