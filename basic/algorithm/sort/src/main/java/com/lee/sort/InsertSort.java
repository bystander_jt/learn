package com.lee.sort;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/15 16:14
 * @since VERSION
 */

public class InsertSort extends Base {

    public static void main(String[] args) {
        InsertSort sort = new InsertSort();
        int[] array = sort.createArray(50);
        sort.print(sort.insertSort(array));
    }

    public int[] insertSort(int[] data) {
        for (int i = 1; i < data.length; i++) {
            int prev = i - 1;
            // 获取需要排序的值
            int current = data[i];
            // 通过 while 循环查找存放位置.
            while (prev >= 0 && data[prev] > current) {
                data[prev + 1] = data[prev];
                prev--;
            }
            // 因为 while 最后使用 prev --, 而需要替换的值是 prev + 1
            data[prev + 1] = current;
        }
        return data;
    }


}
