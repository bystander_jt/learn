package com.lee.sort;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/15 17:21
 * @since VERSION
 */

public class SelectSort extends Base {

    public static void main(String[] args) {
        SelectSort sort = new SelectSort();
        int[] array = sort.createArray(50);
        sort.sort(array);
        sort.print(array);

    }

    public void sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {

            int replace = array[i];
            int minIndex = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            array[i] = array[minIndex];
            array[minIndex] = replace;
        }
    }

}
