package com.lee.sort;

public class HeapSort {


    public static void main(String[] args) {
        int[] arr = {3, 1, 7, 8, 3, 5, 6, 12, 3};
        new HeapSortV1().sort(arr);
    }


    static class HeapSortV1 extends Sort {

        @Override
        void sortAlgorithm(int[] arr) {
            createBigHeap(arr);
        }

        private void createBigHeap(int[] arr) {

            // 构建堆
            for (int i = 0; i < arr.length; i++) {
                heapInsert(arr, i);
            }

            int heapSize = arr.length;
            // 将头尾节点替换
            swap(arr, 0, --heapSize);

            // 将数组进行排序. 每次从堆中获取最大值, 然后与 array[heapSize--] 进行替换, 替换之后再从堆中获取最大值, 然后继续替换, 直到 heapSize <= 0 . 那整个堆也就排好序了

            while (heapSize > 0) {
                heapify(arr, 0, heapSize);
                swap(arr, 0, --heapSize);
            }

        }

        /**
         * @param arr
         * @param idx
         */
        private void heapInsert(int[] arr, int idx) {
            while (arr[idx] > arr[(idx - 1) / 2]) {
                swap(arr, idx, (idx - 1) / 2);
                idx = (idx - 1) / 2;
            }
        }

        private void heapify(int[] arr, int index, int heapSize) {
            // 左孩子下标节点
            int child = (index * 2 + 1);

            // 孩子必须存在
            while (child < heapSize) {
                int largest = index;

                // 右侧也有孩子吗? 有的话进行比较, 将最大的提取出来. 如果没有则将左孩子作为最大
                if (child + 1 < heapSize) {
                    largest = arr[child] > arr[child + 1] ? child : child + 1;
                } else {
                    largest = child;
                }

                // 父节点与子节点进行比较
                largest = arr[largest] > arr[index] ? largest : index;
                // 如果相同, 那就跳出循环
                if (index == largest) {
                    break;
                }

                // 替换
                swap(arr, index, largest);
                index = largest;
                child = index * 2 + 1;
            }
        }
    }
}
