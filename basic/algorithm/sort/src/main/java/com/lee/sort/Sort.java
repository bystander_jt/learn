package com.lee.sort;

import java.util.Arrays;

public abstract class Sort extends Base {

    public final void sort(int[] array) {
        if (array == null) return;

        int[] arr = Arrays.copyOf(array, array.length);
        int[] tmp = Arrays.copyOf(array, array.length);

        System.out.println("================== [" + this.getClass().getSimpleName() + "] ==================");

        System.out.println("unordered: " + Arrays.toString(arr));

        try {
            sortAlgorithm(arr);
        }catch (Throwable e) {
            System.out.println("order has " + e.getClass().getSimpleName() + " stack will output soon");
            e.printStackTrace();
            System.out.print(Arrays.toString(e.getStackTrace()));
        }
        System.out.println("ordered: " + Arrays.toString(arr));

        Arrays.sort(tmp);
        System.out.println("JDK ordered:" + Arrays.toString(tmp));

        boolean result = compare(tmp, arr);
        System.out.println("Compare Result: " + (result ? "TRUE" : "FALSE"));

        System.out.println("================== [" + this.getClass().getSimpleName() + "] ==================");
        System.out.println(" ");

    }

    private boolean compare(int[] a, int[] b) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) return false;
        }
        return true;
    }


    abstract void sortAlgorithm(int[] arr);


}
