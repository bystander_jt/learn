package cn.lee.question;

import java.util.*;
import java.util.stream.Collectors;

public class $49GroupingOfLetterAnagrams {


    public static void main(String[] args) {
        $49GroupingOfLetterAnagrams resolve = new $49GroupingOfLetterAnagrams();

        String[] inputs = new String[]{"eat", "tea", "tan", "ate", "nat", "bat"};
//        String[] inputs = new String[]{"", "b"};
        System.out.println(resolve.groupAnagrams(inputs));
    }


    public List<List<String>> groupAnagrams(String[] strs) {

        // k: length,  Val:(k: ascii ordered  value : inputstr set)
        Map<String, List<String>> cntMap = new HashMap<>();


        for (String str : strs) {

            String orderedString = order(str);
            List<String> cache = cntMap.compute(orderedString, (k, v) -> v != null ? v : new ArrayList<>());
            cache.add(str);
        }

        List<List<String>> result = new ArrayList<>(strs.length);
        for (List<String> value : cntMap.values()) {
            result.add(value);
        }

        result.sort(Comparator.comparingInt(List::size));

        return result;


    }


    private String order(String str) {

        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }

}
