package cn.lee.question.sort;

import java.util.Arrays;

public class InsertSort {

    private static final int[] arr = new int[]{5, 2, 2, 4, 3, 1, 5, 34};


    public static void main(String[] args) {

        for (int i = 1; i < arr.length - 1; i++) {
            for (int j = i; j > 0; j--) {

                if (arr[j] < arr[j - 1]) {
                    swap(arr, j, j - 1);
                } else break;
            }

        }

        System.out.println(Arrays.toString(arr));

    }

    private static void swap(int[] arr, int i, int i1) {

        arr[i] = arr[i] ^ arr[i1];
        arr[i1] = arr[i] ^ arr[i1];
        arr[i] = arr[i] ^ arr[i1];

    }


}
