package cn.lee.question;

/**
 * 寻找数组中的奇数或者偶数
 */
public class FindOddOrEvenNumberInArray {


    public static void main(String[] args) {
        int[] nums = new int[]{3,1,2,4,5,3};

        int duplicate = findDuplicate(nums);

    }


    /**
     * 寻找数组中出现奇数的数
     *
     * @param arr
     * @return
     */
    private static int findOddNumber(int[] arr) {

        int xor = 0;

        for (int i : arr) {
            xor ^= i;
        }

        return xor;

    }

    /**
     * 寻找数组中出现奇数的2个数字
     * <p> 首先通过异或运算得出所有奇数的 xor 变量</p>
     * <p>然后在计算好的 XOR 中, 通过二进制寻找某位为1的下标地址, 通过下标地址所对应的值区分这两个奇数的不同</p>
     * <p>在定义一个 xor 变量, 只将数组中下标位置为 1 的数进行异或运算, 得到其中的 A 或者 B</p>
     * <p>这时我们获得了一个 xor (a ^b) 和 xor'(a|b). 最后通过 xor ^ xor' 得到另一个奇数次的数</p>
     *
     * @param arr
     * @return
     */
    private static int[] findOddNumber2(int[] arr) {

        int xor = 0;

        for (int i : arr) {
            xor ^= i;
        }

        /*
         * 假设 xor = %d, 二进制表示: 101
         * 0101 & (010 + 1) = 101 & 011 = 1
         * */
        int rightOne = xor & (~xor + 1);

        int $xor = 0;
        /* 在循环一次, 找到对应二进制位中存在为 1 的数字进行异或运算*/
        /* 相同的则会被自己给异或掉, 最后得到的就是对应二进制位下为1的奇数*/
        for (int i : arr) {
            if ((i & rightOne) == 0) $xor ^= i;
        }

        /* 我们得到了 $xor 2个奇数中的一个, 那我们在将之前 (a ^ b) 的结果与 $xor 进行异或得到另一个*/
        return new int[]{$xor, ($xor ^ xor)};


    }

    private static int findFirstOneInBinary(int target) {
        int i = target & (~target + 1);

        System.out.println(String.format("~%d: ", target) + Integer.toBinaryString(~target));
        System.out.println(String.format("(~%d+1) = ", target) + Integer.toBinaryString(~target + 1));
        System.out.println(String.format("%d & (~%d + 1) = ", target, target) + Integer.toBinaryString(target) + " & " + Integer.toBinaryString(~target + 1));
        System.out.println("= " + Integer.toBinaryString(i));

        return i;
    }

    /**
     * 寻找出现次数为偶数的数
     *
     * @param arr
     * @return
     */
    private static int findEvenNumber(int[] arr) {
        int oddNum = findOddNumber(arr);
        int rightOne = oddNum & (~oddNum + 1);

        int ol = 0;
        for (int i : arr) {
            if ((i & rightOne) == 1) {
                ol ^= i;
            }
        }

        return ol;
    }


    private static int findDuplicate(int[] nums) {
        int slow = 0;
        int fast = 0;
        slow = nums[slow];
        fast = nums[nums[fast]];
        while(slow != fast){
            slow = nums[slow];
            fast = nums[nums[fast]];
        }
        int pre1 = 0;
        int pre2 = slow;
        while(pre1 != pre2){
            pre1 = nums[pre1];
            pre2 = nums[pre2];
        }
        return pre1;
    }


}
