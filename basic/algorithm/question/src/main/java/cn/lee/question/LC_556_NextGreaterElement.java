package cn.lee.question;

public class LC_556_NextGreaterElement {

    public static void main(String[] args) {
        LC_556_NextGreaterElement solution = new LC_556_NextGreaterElement();
        System.out.println(solution.nextGreaterElement(1234));
        System.out.println(solution.nextGreaterElement(136542));
        System.out.println(solution.nextGreaterElement(12));
    }

    public int nextGreaterElement(int n) {
        String num = String.valueOf(n);
        char[] array = num.toCharArray();

        int i = array.length - 2;

        // 左指针. 从末尾向左寻找第一个最大的数.
        while ( i >= 0 && array[i] >= array[i + 1]){
            // 从左向右找到最大数的前一位数的下标
            i--;
        }
        // 如果走到了头还没有, 则代表没有结果.
        if (i < 0) return -1;

        int j = array.length - 1;
        // 找到一个比 i 小的数
        while( j >= 0 && array[i] >= array[j]){
            j--;
        }

        swap(array, i, j);
        // 因为前面 i 已经判断出最大位置的小标 ( i 的下标 + 1 ), 说明 i 后面的数是从大到小递减.
        // 6 -> 5 -> 4 -> 3 -> 2 -> 1
        // 所以需要对 i 后面(不包括 i ) 的数使用双指针进行替换来满足题目要求
        // 1 -> 2 -> 3 -> 4 -> 5 -> 6
        reverse(array, i + 1);
        String value = new String(array);
        Long ans = Long.parseLong(value);
        return ans > Integer.MAX_VALUE ? -1 : (int) ans.intValue();


    }


    public void reverse(char[] nums, int begin) {
        int i = begin, j = nums.length - 1;
        while (i < j) {
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    public void swap(char[] nums, int i, int j) {
        char temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }


}
