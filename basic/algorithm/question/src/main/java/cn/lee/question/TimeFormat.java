package cn.lee.question;


import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * 时间表示为XX:XX:XX.XXX分别代表小时，分钟，秒，毫秒
 * 输入数据可能是简写模式，如1:1:1.1代表01:01:01.001，输出升序排列，遇到相等时间按输入顺序输出。输入时先输入数据量N，输入N条时间，输出为排序后时间。
 * 用例：
 * 输入格式：3
 * 02:02:45.654
 * 1:01:11.321
 * 2:2:45.654
 * 输出：
 * 1:01:11.321
 * 02:02:45.654
 * 02:02:45.654
 */

public class TimeFormat {

    private static final long SECONDS_NANO = 1000L;
    private static final long MINUS_NANO = 60 * SECONDS_NANO;
    private static final long HOUR_NANO = 60 * MINUS_NANO;

    public static void main(String[] args) {
        String[] time = {"02:02:45.654", "1:01:11.321", "2:2:45.654"};
        Long[] formatTime = new Long[time.length];

        for (int i = 0; i < time.length; i++) {
            formatTime[i] = convertToNumber(time[i]);
        }

        Arrays.sort(formatTime);
        for (Long aLong : formatTime) {
            System.out.println(reformat(aLong));
        }
    }

    private static long convertToNumber(String time) {
        String[] timeArray = time.split("\\.");

        String[] timeA = timeArray[0].split("\\:");
        int hour = Integer.valueOf(timeA[0]);
        int mins = Integer.valueOf(timeA[1]);
        int seconds = Integer.valueOf(timeA[2]);
        int nano = Integer.valueOf(timeArray[1]);

        Long nanoTime = ((long) hour * HOUR_NANO)
                + ((long) mins * MINUS_NANO)
                + (seconds * SECONDS_NANO)
                + nano;
        return nanoTime;
    }

    private static String reformat(Long nanoTime){
        long hour = nanoTime / HOUR_NANO;
//        System.out.println(log("hour", hour, nanoTime) );

        nanoTime = nanoTime - (hour * HOUR_NANO);

        long minus = nanoTime  / MINUS_NANO;
//        System.out.println(log("minus", minus, nanoTime) );

        nanoTime = nanoTime - (minus * MINUS_NANO);

        long seconds = nanoTime / SECONDS_NANO;
//        System.out.println(log("seconds", seconds, nanoTime));

        long nano = nanoTime - (seconds * 1000);
//        System.out.println(log("nano", nano, nanoTime));

        return String.format("%02d:%02d:%02d.%04d", hour, minus,seconds, nano);
    }

    private static String log(String unit,long divideTime, Long nanoTime){
        return String.format("%d - %s : %d", nanoTime, unit, divideTime);
    }


}
