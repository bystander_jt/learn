package cn.lee.question.basic;

public class BinaryNode {

    public BinaryNode left;

    public BinaryNode right;

    public int val;

    public BinaryNode(int val) {
        this.val = val;
    }

}
