package cn.lee.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomIntSort {

    public static void main(String[] args) {
//        Scanner scan = new Scanner(System.in);

//		int arraySize = scan.nextInt();
        int arraySize = 30;

        List<Integer> set = new ArrayList(arraySize);
        set.add(new Random().nextInt(500));
        A:
        for (int i = 1; i <= arraySize; ++i) {
//			int input = scan.nextInt();

            int input = new Random().nextInt(500);
            // if exist. continue next;
            if (set.contains(input)) {
                continue;
            }
            set.add(input);
        }


        for (int l = 0; l < set.size(); l++) {
            int tmp = set.get(l);
            for (int i = 0; i < l; i++) {
                if (tmp < set.get(i)){
                    set.remove(l);
                    set.add(i, tmp);
                    break;
                }
            }
        }

        set.forEach(System.out::println);

    }

}
