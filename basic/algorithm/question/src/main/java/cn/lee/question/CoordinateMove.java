package cn.lee.question;


import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * website : <a href="newcoder.com">newcoder.com</a>
 * title: 坐标移动
 * level: middle
 * <a href="https://www.nowcoder.com/practice/119bcca3befb405fbe58abe9c532eb29?tpId=37&tags=&title=&difficulty=&judgeStatus=&rp=0&sourceUrl=https%3A%2F%2Fwww.nowcoder.com%2Fexam%2Foj&gioEnter=menu">坐标移动</a>
 * </pre>
 */
public class CoordinateMove {


    private static final String VALID_REGEX = "^([ASDW](\\d)+)$";

    public static void main(String[] args) {

        Map<Integer, String> count = new HashMap<>();
        for (Map.Entry<Integer, String> entry : count.entrySet()) {

        }

        process("S87;S7;W56;S75;A8;S84;W23;W19;W40;D73;S87;A39;W97;W78;A53;D16;D15;A50;W41;S87;D47;W56;D56;A23;A91;S25;D61;D53;D58;W88;W58;S61;D69;W74;D89;A92;D39;D62;S78;W72;W73;W35;S76;W35;S36;W39;A4;");
    }

    private static void process(String input) {
        int horizontal = 0;
        int vertical = 0;

        String[] operation = input.split(";");
        for (String move : operation) {

            if (!isValid(move)) continue;
            String direction = move.substring(0, 1);
            Integer step = Integer.parseInt(move.substring(1));

            if (step == 0) continue;

            if (direction.equalsIgnoreCase("A")) {
                horizontal -= step;
            } else if (direction.equalsIgnoreCase("S")) {
                vertical -= step;
            } else if (direction.equalsIgnoreCase("W")) {
                 vertical+= step;
            } else if (direction.equalsIgnoreCase("D")) {
                horizontal += step;
            }
        }

        System.out.println(vertical + "," + horizontal);
    }


    private static boolean isValid(String locationInput) {
        return locationInput.matches(VALID_REGEX);
    }


}
