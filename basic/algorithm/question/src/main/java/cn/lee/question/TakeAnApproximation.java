package cn.lee.question;

import java.util.HashMap;
import java.util.Stack;

public class TakeAnApproximation {

    public static void main(String[] args) {
        System.out.println(100 >> 8);
        calculate(5.5);

        HashMap<Integer,Integer> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put(1,1);
        objectObjectHashMap.computeIfPresent(1, (k, v) -> v + 1);
//        objectObjectHashMap.computeIfAbsent(2, (k, v) -> 0);
        objectObjectHashMap.computeIfPresent(3, (k, v) -> v + 1);
        objectObjectHashMap.computeIfPresent(4, (k, v) -> v + 1);
        objectObjectHashMap.computeIfPresent(1, (k, v) -> v + 1);

    }


    private static void calculate(double result){

        double left = result % 1;
        if (left >= 0.5) result++;
        System.out.println((int)result);

    }
}
