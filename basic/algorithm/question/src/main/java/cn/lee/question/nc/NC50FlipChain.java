package cn.lee.question.nc;

import cn.lee.question.basic.ListNode;

public class NC50FlipChain {

    public static void main(String[] args) {
        NC50FlipChain slove = new NC50FlipChain();
        slove.reverseKGroup(slove.create(new int[]{1, 2, 3, 4, 5}), 2);
    }

    private ListNode create(int[] num) {
        ListNode pre = null;
        for (int i = num.length; i > 0; --i) {
            ListNode cur = new ListNode(num[i - 1]);
            cur.next = pre;
            pre = cur;
        }
        return pre;
    }

    public ListNode reverseKGroup(ListNode head, int k) {

        ListNode tail = head;
        // 找屁股
        for (int i = 0; i < k; ++i) {
            if (tail == null) return head;
            tail = tail.next;
        }

        ListNode r = null;
        ListNode t = head;
        while (t != tail) {
            ListNode next = t.next;
            t.next = r;
            r = t;
            t = next;
        }

        head.next = reverseKGroup(tail, k);

        return r;

    }


}
