package cn.lee.question.leetcode;

import cn.lee.question.basic.ListNode;

/**
 * <a href="https://leetcode.cn/problems/liang-ge-lian-biao-de-di-yi-ge-gong-gong-jie-dian-lcof/" >剑指 Offer 52. 两个链表的第一个公共节点</a>
 */
public class $2852_getIntersectionNode {


    ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode cur1 = headA;
        ListNode cur2 = headB;

        int remainLength = 0;
        ListNode aEnd = null;
        while (cur1.next != null) {
            ++remainLength;
            cur1 = cur1.next;
        }

        while (cur2.next != null) {
            --remainLength;
            cur2 = cur2.next;
        }

        // 不相交
        if (cur1 != cur2) return null;

        // 长的
        cur1 = remainLength > 0 ? headA : headB;
        // 短的
        cur2 = cur1 == headA ? headB : headA;

        remainLength = Math.abs(remainLength);
        while (remainLength != 0) {
            --remainLength;
            cur1 = cur1.next;
        }
        while (cur1 != cur2) {
            cur1 = cur1.next;
            cur2 = cur2.next;
        }

        return cur1;

    }

    /**
     * 通过循环，将长度长的 ListNode 与 短的 ListNode 放置在同一起跑线，然后再进行比较得到结果. <p>
     * <pre>
     * 假设有 [1,2,3,4,5,6,7] 与 [3,4,5,6,7] 这么两个 ListNode.
     * 在 b 循环了5次之后, a 循环还在 [5] 这个数组元素下.
     * 然后 b 使用了 a 的数组进行循环, 在 a 对数组遍历完成后, b 已经在 a.length - b.length 的差值上 (也就是 [3] 上)
     * 之后 a 遍历完成, 然后使用了 b 数组开始遍历, 这时双方都在 [3] 这个数组上, 通过比较得到了相同的值.
     *
     *  a: [1,2,3,4,5,6,7], b: [3,4,5,6,7]
     *              ↑                   ↑
     * - 由于 b 遍历完成, 所以使用 a 数组(或链表)
     *
     *  a: [1,2,3,4,5,6,7], b: [1,2,3,4,5,6,7]
     *                  ↑         ↑
     * - 在 a 遍历完后, b 已经遍历到 a 数组(链表) 的部分位置. 这个位置刚好是 a 与 b 之间的间隔. 然后 a 开始遍历 b 数组
     *  a: [3,4,5,6,7] , b: [1,2,3,4,5,6,7]
     *      ↑                    ↑
     * - 在遍历时就可以在后半的公共部分中寻找 == 的值.
     * </pre>
     *
     * @param headA
     * @param headB
     * @return
     */
    ListNode simpleSolution(ListNode headA, ListNode headB) {

        ListNode a = headA, b = headB;

        while (a != b) {
            a = (a == null) ? headB : a.next;
            b = (b == null) ? headA : b.next;
        }

        return a;

    }


    static class IntersectionNodeV2 {

        ListNode getIntersectionNode(ListNode headA, ListNode headB) {

            if (headA == null || headB == null) return null;

            ListNode aLoopHead = getLoopNode(headA);
            ListNode bloopHead = getLoopNode(headB);

            // 双方其中一方没有循环, 那条件就不成立.
            if (aLoopHead == null || bloopHead == null) {
                return noLoopV2(headA, headB);
            }
            if (aLoopHead != null && bloopHead != null) {
                return bothLoop(headA, aLoopHead, headB, bloopHead);
            }

            return null;
        }

        private ListNode getLoopNode(ListNode node) {

            ListNode slow = node.next;
            ListNode fast = node.next.next;

            while (slow != fast) {
                slow = slow.next;

                if (fast.next != null && fast.next.next != null)
                    fast = fast.next.next;
                else return null;
            }

            fast = node;
            while (slow != fast) {
                slow = slow.next;
                fast = fast.next;
            }
            return slow;
        }

        private ListNode noLoopV1(ListNode node1, ListNode node2) {

            ListNode cur1 = node1;
            ListNode cur2 = node2;

            while ( cur1 != cur2) {
                cur1 = cur1.next == null ? node2 : cur1.next;
                cur2 = cur2.next == null ? node1 : cur2.next;
            }

            return cur1;
        }

        private ListNode noLoopV2 (ListNode node1, ListNode node2) {
            int n = 0;

            ListNode cur = node1;
            while (cur.next != null) {
                ++n;
                cur = cur.next;
            }


            ListNode cur2 = node2;
            while (cur2.next != null) {
                --n;
                cur2 = cur2.next;
            }

            if (cur == cur2) return cur;

            cur = n > 0 ? node1 : node2;
            cur2 = cur == node1 ? node2 : node1;

            n = Math.abs(n);
            while (n > 0) {
                cur = cur.next;
                --n;
            }

            while (cur != cur2) {
                cur = cur.next;
                cur2 = cur2.next;
            }


            return cur;

        }

        private ListNode bothLoop (ListNode node1, ListNode loop1, ListNode node2, ListNode loop2) {

            ListNode cur1 = node1;
            ListNode cur2 = node2;

            // 头环相同.
            if (loop1 == loop2) {

                int n = 0;

                while (cur1 != loop1) {
                    n++;
                    cur1 = cur1.next;
                }

                while (cur2 != loop2) {
                    --n;
                    cur2 = cur2.next;
                }

                cur1 = n > 0 ? node1 : node2;
                cur2 = cur1 == node1 ? node2 : node1;

                n = Math.abs(n);
                while (n > 0) {
                    cur1 = cur1.next;
                    --n;
                }

                while (cur1 != cur2) {
                    cur1 = cur1.next;
                    cur2 = cur2.next;
                }

                return cur1;
            } else {
                cur1 = loop1.next;
                while (cur1 != loop1){
                    if (cur1 == loop2) return cur1;
                    cur1 = cur1.next;
                }
            }
            return null;
        }


    }
}
