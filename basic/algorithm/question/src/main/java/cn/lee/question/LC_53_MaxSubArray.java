package cn.lee.question;

/**
 * <p><a href="https://leetcode.cn/problems/maximum-subarray/">LeetCode 最长子数组</a></p>
 */

public class LC_53_MaxSubArray {

    public static void main(String[] args) {
        LC_53_MaxSubArray subArray = new LC_53_MaxSubArray();
        subArray.dynamicProgram(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4});
    }

    private int max(int a, int b) {
        return Math.max(a, b);
    }

    /**
     * 动态规划求解数据。
     * 由题求得最大子数组. 按照动态规划5要素
     * 1 : 设计状态. 设计 max(最大值) 和 fib[] (存放 max( fib[i - 1] + num[i], num[i]))
     * 2 : 动态转移方程. 题目需要最大子数组, 由此可以理解为 : 是 fib[i - 1] + num[i] 大, 还是当前 num[i] 大;
     * @param nums
     * @return
     */
    public int dynamicProgram(int[] nums) {

        int max = nums[0];

        int[] fib = new int[nums.length + 1];
        fib[0] = nums[0];
        for (int i = 1; i <= nums.length - 1; ++i) {
            fib[i] = max(fib[i - 1] + nums[i], nums[i]);
            max = max(fib[i], max);
        }

        return max;


    }

}
