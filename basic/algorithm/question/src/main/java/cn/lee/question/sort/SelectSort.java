package cn.lee.question.sort;

import java.util.Arrays;

public class SelectSort {

    public static void main(String[] args) {
        int[] init = new int[]{1, 10, 9, 34, 45, 234, 12, 4, 6, 8233, 45};
        selectSort(init);
        System.out.println(Arrays.toString(init));
    }

    private static void selectSort(int[] sort) {
        for (int i = 0; i < sort.length - 1; i++) {
            int max = i;
            for (int j = i; j < sort.length - 1; j++) {
                max = sort[i] < sort[j] ? j : i;
            }
            if (max != i) {
                int $0 = sort[i];
                sort[i] = sort[max];
                sort[max] = $0;
            }
        }
    }
}
