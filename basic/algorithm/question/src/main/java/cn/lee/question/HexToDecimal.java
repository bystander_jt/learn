package cn.lee.question;

/**
 * 
 * 
 * @since VERSION
 */

public class HexToDecimal {

    public static void main(String[] args) {
        run("0x2C");
        run("0x1504");
    }

    private static void run(String hex) {
        hex = hex.replace("0x", "");
//         System.out.println(Integer.parseInt(hex, 16));
        System.out.println(convert(hex));
    }


    private static Integer convert(String hex) {
        char[] hexarray = hex.toCharArray();
        Integer result = 0;
        for (int i = 0; i < hexarray.length; i++) {
            Integer position = (int) Math.pow(16, i);
            result += HToD(hexarray[hexarray.length - i - 1]) * position;
        }
        return result;
    }

    private static Integer HToD(char hex) {
        switch (hex) {
            case '0' : return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case 'A':
                return 10;
            case 'B':
                return 11;
            case 'C':
                return 12;
            case 'D':
                return 13;
            case 'E':
                return 14;
            case 'F':
                return 15;
        }
        return null;
    }


}
