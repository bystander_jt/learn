package cn.lee.question.sort;

import java.util.Arrays;

public class BubbleSort {


    public static void main(String[] args) {
        int[] init = new int[]{1, 10, 9, 34, 45, 234, 12, 4, 6, 8233, 45};
        bubbleSort(init);
        System.out.println(Arrays.toString(init));
    }

    private static void bubbleSort(int[] array) {

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int swap = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swap;
                }
            }
        }

    }
}
