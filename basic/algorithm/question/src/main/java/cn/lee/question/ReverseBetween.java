package cn.lee.question;

import cn.lee.question.basic.ListNode;

public class ReverseBetween {


    public static void main(String[] args) {
        ReverseBetween reverse = new ReverseBetween();

        int[] array = new int[]{1, 2, 3, 4, 5};

        ListNode node = new ListNode(0);

        ListNode o = node;
        for (int i : array) {
            ListNode next = new ListNode(i);
            node.next = next;
            node = next;
        }
        System.out.println(reverse.reverseBetween(o.next, 3, 4));
    }

    public ListNode reverseBetween(ListNode head, int m, int n) {

        int currentIndex = 0;
        ListNode mPre = null, nNode = null;

        ListNode r = head;

        if (m == 1) {
            mPre = new ListNode(0);
            mPre.next = head;
        }

        while (r != null) {
            if (currentIndex == m - 1) mPre = r;
            if (currentIndex == n - 1) nNode = r;
            if (mPre != null && nNode != null) break;
            r = r.next;
            currentIndex += 1;
        }

        if (mPre == null || nNode == null) return head;

        ListNode leftNode = mPre;
        ListNode rightNode = nNode;

        // 截断 ListNode
        mPre.next = null;
        nNode.next = null;

        reverse(leftNode);

        mPre.next = leftNode;
        leftNode.next = rightNode;


        return head;
    }

    private void reverse(ListNode head) {
        if (head == null) return;

        ListNode prev = null;
        ListNode curv = head;

        while (curv != null) {
            ListNode next = curv.next;
            curv.next = prev;
            prev = curv;
            curv = next;
        }
    }

}
