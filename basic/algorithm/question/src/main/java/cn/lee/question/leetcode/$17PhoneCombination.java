package cn.lee.question.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class $17PhoneCombination {

    public static void main(String[] args) {
        Solution solution = new Solution();
        List<String> strings = solution.letterCombinations("22");
        System.out.println(strings.toString());
    }


    static class Solution {
        public List<String> letterCombinations(String digits) {

            List<String> result = new ArrayList(100);
            char[] digit = digits.toCharArray();

            for (int i = 0; i < digits.length(); i++) {

                List<String> keypad = keypad(digit[i]);

                if (result.isEmpty()) {
                    result.addAll(keypad);
                    continue;
                }

                int size = result.size();
                for (int t = 0; t < size && size > 0; ++t) {
                    String res = result.get(t);
                    for (String key : keypad) {
                        result.add(res + key);
                    }
                    result.remove(res);
                    size -= 1;
                    t -= 1;
                }
            }


            return result;
        }


        private List<String> keypad(char digit) {
            switch (digit) {
                case '2':
                    return Arrays.asList("a", "b", "c");
                case '3':
                    return Arrays.asList("d", "e", "f");
                case '4':
                    return Arrays.asList("g", "h", "i");
                case '5':
                    return Arrays.asList("j", "k", "l");
                case '6':
                    return Arrays.asList("m", "n", "o");
                case '7':
                    return Arrays.asList("p", "q", "r", "s");
                case '8':
                    return Arrays.asList("t", "u", "v");
                case '9':
                    return Arrays.asList("w", "x", "y", "z");
                default:
                    break;
            }
            return new ArrayList<>();
        }
    }

}
