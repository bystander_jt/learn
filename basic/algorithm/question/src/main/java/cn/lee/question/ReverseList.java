package cn.lee.question;

import cn.lee.question.basic.ListNode;

public class ReverseList {


    public static void main(String[] args) {
        ReverseList reverse = new ReverseList();

        int[] array = new int[]{1, 2, 3, 4, 5};

        ListNode node = new ListNode(0);

        ListNode o = node;
        for (int i : array) {
            ListNode next = new ListNode(i);
            node.next = next;
            node = next;
        }
        reverse.ReverseList(o.next);
    }

    public ListNode ReverseList(ListNode head) {
        ListNode newnode = head;
        // 更新到下一个节点
        head = head.next;
        // 设置下节点为空
        newnode.next = null;

        while (head != null) {
            ListNode o = newnode;
            newnode = head;
            newnode.next = o;
            head = head.next;
        }

        return newnode;
    }

}
