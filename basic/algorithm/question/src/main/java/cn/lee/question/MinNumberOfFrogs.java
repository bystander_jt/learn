package cn.lee.question;

import java.util.HashMap;

public class MinNumberOfFrogs {

    public static void main(String[] args) {
        MinNumberOfFrogs minNumberOfFrogs = new MinNumberOfFrogs();
//        int cnt1 = minNumberOfFrogs.solution("croakcroak");
//        int cnt2 = minNumberOfFrogs.solution("crcoakroak");
        int cnt3 = minNumberOfFrogs.solution("cccrorakrcoakorakoak");
        int cnt4 = minNumberOfFrogs.solution("crakkoocaaoarkcrcrorccaooakrakoocccooarokkrraokrkkcakororcrookaaoarckrckkaarkacorrakckaroocacccaaoaakkkkorkarcoaoaaccckcaocookkckkcrkcckkracocoarkorarookccarrocraaocacarorcoorkcracaarorarroarrccrcrcaccooackcaakckokkkkoaorcckakacccorkaarrkakcakcrorkccrrrkoacorcacrkakocorroakokkrrkkakrrckokacarorckracrrrocrrcccooorcararocrcocaaoccaakorcrcckokkkcokcacrkcckakkkkcaorooaorroccrrakcrcaacaokocaokkaorocorckrkokrrcaaarokaoaaroookorrorkoarorckacoaoakkokracaokaaokarooraraaacokrkkkaakoacookcrroakrkoacockkkkoccooarcaraarckcoaaaocakrororkrkorkckokrarkacokokrroacoccaookccrakkkrkoacarr");
        int $cnt3 = minNumberOfFrogs.solutionB("cccrorakrcoakorakoak");
    }

    /**1
     *
     * @param croakOfFrogs
     * @return
     * @deprecated 无法对字符顺序做出有效判断, 如果做出判断则时间复杂度就会提升 (N²).
     */
    @Deprecated
    private int solution(String croakOfFrogs) {

        if (croakOfFrogs == null || croakOfFrogs == "" || croakOfFrogs.length() < 5 || croakOfFrogs.length() % 5 != 0)
            return -1;

        char[] croak = croakOfFrogs.toCharArray();
        if (croak[0] != 'c') return -1;

        int cnt = 0, res = 0;
        HashMap<Character, Integer> cntMap = new HashMap<>(5);

        for (Character cha : croak) {

            Integer count = cntMap.compute(cha, (k, v) -> v == null ? 1 : v + 1);

            if (cha == 'c') {
                cnt += 1;
            } else if (cha == 'k') {
                Integer cCount = cntMap.get('c');
                if ( cCount  == null || cCount < count) return -1;
                cnt -= 1;
            }



            res = Math.max(cnt, res);
        }


        int chaCnt = -1;
        boolean unequal = false;
        for (int count : cntMap.values()) {
            unequal = (chaCnt != -1 ? (chaCnt != count) : ((chaCnt = count) == -1));
        }


        if (unequal) return -1;
        return res;
    }

    /**
     * 遍历蛙鸣数组，记录每个字母的频数，c表示'c'的频数，r表示'r'的频数，o表示'o'的频数，a表示'a'的频数，k表示'k'的频数。
     * <p>
     * 一方面，由于蛙鸣的字母是按照croak的顺序出现的，所以在遍历的过程中必须要满足c>=r>=o>=a>=k，并且遍历完成后必须满足c=r=o=a=k（每个蛙鸣必须是完整的），否则就是不合法的，返回-1。
     * 另一方面, 在传入字符中可能存在部分区域出现多个🐸一起叫(字符穿插且重叠), 那我们只需要计算这重叠部分最多存在多少个, 就知道有多少只青蛙. 我们通过 C 定义语句的开始, K 定义结束, 那只要知道在完整的 'croak' 中出现了多少次 c 就知道有多少只. ( c -k )
     * 另一方面，我们其实可以把蛙鸣看成是若干区间进行重叠，遍历到croakOfFrogs的每个位置都会有一个“各个字母的频数”状态，所有位置的状态中“并行度”最大的地方就是最少需要的青蛙数量。由于k是一次蛙鸣的结尾，c是一次蛙鸣的开头，且c>=k，所以某个位置的“并行度”其实就是c-k，维护这个最大值即可。
     *
     * @param croakOfFrogs
     * @return
     */
    private int solutionB(String croakOfFrogs) {

        int size = croakOfFrogs.length(), res = 0;
        int c = 0, r = 0, o = 0, a = 0, k = 0;

        for (char frog : croakOfFrogs.toCharArray()) {

            if (frog == 'c') c++;
            if (frog == 'r') r++;
            if (frog == 'o') o++;
            if (frog == 'a') a++;
            if (frog == 'k') k++;

            if (c < r || r < o || o < a || a < k) return -1;

            res = Math.max(c - k, res);
        }

        return c == r && r == o && o == a && a == k ? res : -1;

    }


}
