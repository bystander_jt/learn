package cn.lee.question.leetcode;

import cn.lee.question.basic.ListNode;

import java.util.Stack;

/**
 * <h1>是否回文链表</h1>
 * <h3>难度等级: EASY</h3>
 * <h3><a href="https://leetcode.cn/problems/palindrome-linked-list/">Leetcode</a></h3>
 * <ul>
 *     <li><b>关联算法</b></li>
 *     <li>快慢指针</li>
 *     <li>反转链表</li>
 *     <li>堆</li>
 *     <li></li>
 * </ul>
 * <b>关键字: 链表, 快慢指针, 堆, 反转链表</b>
 *
 *
 *
 */
public class $234_EASY_IambicLinkTable {


    /**
     * 采用 <b>快慢指针</b> 和 <b>反转链表</b> 判断出是否为回文链表 <p>
     * <ol>
     *     <li>先通过快慢指针找到中心节点 (如果偶数则必须是中心虚拟节点的左侧)</li>
     *     <li>通过找到中心节点, 将中心节点之后的元素进行链表反转</li>
     *     <li>反转之后通过循环比对, 比较是否完全符合. (奇数则会中间元素不进行匹配, 偶数则都会进行)</li>
     * </ol>
     * @param head
     * @return
     */
    public boolean doubleIndexAndReverseList(ListNode head) {

        if (head.next == null) return true;

        int l1 = 0, l2 = 0;
        ListNode middle =  findMiddle(head);

        // 反转链表
        ListNode reverseNode = reverse(middle.next);
        middle.next = null;

        // 双链表判断是否相同
        ListNode check = reverseNode;
        while (head != null && check != null) {
            if (head.val != check.val) return false;
            head = head.next;
            check = check.next;
        }

        // 恢复
        reverseNode = reverse(reverseNode);
        middle.next = reverseNode;

        return true;
    }

    public boolean stack(ListNode head) {

        if (head.next == null) return true;

        ListNode middle = findMiddle(head);

        Stack<ListNode> stack = new Stack<>();

        ListNode pushStackNode = middle.next;
        while(pushStackNode != null) {
            stack.push(pushStackNode);
            pushStackNode = pushStackNode.next;
        }

        // 取消关联
        middle.next = null;

        ListNode lastStackNode = null;
        while (head.next != null) {
            ListNode pop = stack.pop();
            if (pop == null || head.val != pop.val) return false;
            lastStackNode = pop;
        }

        middle.next = lastStackNode;
        return true;




    }

    private ListNode findMiddle(ListNode head) {
        // 快慢指针
        ListNode twoStep = head;
        ListNode oneStep = head;
        while (twoStep.next != null && twoStep.next.next != null) {
            oneStep = oneStep.next;
            twoStep = twoStep.next.next;
        }
        return oneStep;
    }

    private ListNode reverse(ListNode node) {

        ListNode prev = null;

        while (node != null) {

            ListNode next = node.next;
            node.next = prev;
            prev = node;
            node = next;
        }

        return prev;

    }


}
