package cn.lee.question;

/**
 * 
 * 
 * @since VERSION
 */

public class LC_8_myAtoi {

    public static void main(String[] args) {
        LC_8_myAtoi lc = new LC_8_myAtoi();

//        System.out.println("output: " + lc.myAtoi("+42"));

//        System.out.println("output: " + lc.myAtoi("+-42"));
        System.out.println("output: " + lc.myAtoi("2147483648"));
        System.out.println();
        System.out.println("output: " + lc.myAtoi("2147483646"));
        System.out.println();
        System.out.println("output: " + lc.myAtoi(String.valueOf(Integer.MIN_VALUE + 1)));
        System.out.println();
        System.out.println("output: " + lc.myAtoi(String.valueOf(Integer.MAX_VALUE - 1)));
        System.out.println();
        System.out.println("output: " + lc.myAtoi("-91283472332"));
        System.out.println();
        System.out.println("output: " + lc.myAtoi("   -42"));
        System.out.println();
        System.out.println("output: " + lc.myAtoi("  -0012a42"));

//        System.out.println("output: " + lc.myAtoi("4193 with words"));

//        System.out.println("output: " + lc.myAtoi("words and 987"));
    }

    /**
     * <pre>
     *     先通过一次循环. 找到数字前面的 ' ' 空格.
     *     然后再找是不是存在 +- 符号.
     *     最后通过循环 {@code mark} 指针上的字符串是否是数字.
     *     如果是数字, 则先对结果 * 10 然后增加到结果内.
     *     然后再对结果 /10  判断是否和未进行 * 10 的结果相同. 如果不相同则认为数据溢出.
     *     根据 {@code negative} 判断正负返回 {@code Integer.MIN_VALUE} 还是 {@code Integer.MAX_VALUE}
     * </pre>
     * @param s
     * @return
     */
    public int myAtoi(String s) {
        System.out.println("input: " + s);
        s = s.trim();

        int negative = 1;
        int res = 0;
        int mark = 0;

        while (mark < s.length() && s.charAt(mark) == ' ') {
            mark++;
        }

        char isSymbol = s.charAt(mark);
        if (isSymbol == '+') {
            mark++;
        } else if (isSymbol == '-') {
            mark++;
            negative = -negative;
        }

        int last = 0;
        while (mark < s.length()) {
            char charAt = s.charAt(mark);

            mark++;

            boolean digit = charAt >= '0' && charAt <= '9';
            if (!digit) return res * negative;

            int integer = charAt - '0';
            last = res;
            res = res * 10 + integer;
            if (last != res / 10)
                return (negative == (-1)) ? Integer.MIN_VALUE : Integer.MAX_VALUE;

        }
        return res * negative;
    }

    public int solution(String s) {
        char[] chars = s.toCharArray();
        int len = chars.length;
        //1.去空格
        int index = 0;
        while (index < len && chars[index] == ' ')
            index++;
        //2.排除极端情况 "    "
        if (index == len) return 0;
        //3.设置符号
        int sign = 1;
        char firstChar = chars[index];
        if (firstChar == '-') {
            index++;
            sign = -1;
        } else if (firstChar == '+') {
            index++;
        }
        int res = 0, last = 0; //last 记录上一次的res，以此来判断是否溢出
        while (index < len) {
            char c = chars[index];
            if (c < '0' || c > '9') break;
            int tem = c - '0';
            last = res;
            res = res * 10 + tem;
            if (last != res / 10)  ////如果不相等就是溢出了
                return (sign == (-1)) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            index++;
        }
        return res * sign;
    }

}
