package cn.lee.question;

public class ReverseString {

    public static void main(String[] args) {
        ReverseString reverse = new ReverseString();
//        String string = "hello";
        String string = "123456";

        String hello = reverse.reverse(string);
        System.out.println(hello);
        hello = reverse.reverseV2(string);
        System.out.println(hello);
    }


    public String reverse(String str) {
        char[] array = str.toCharArray();

        int length = str.length();

        int n = length - 1;
        for (int j = (n - 1) >> 1; j >= 0; j--) {
            int k = n - j;
            char cj = array[j];
            char ck = array[k];
            array[j] = ck;
            array[k] = cj;
        }


        return new String(array);
    }

    public String reverseV2(String str) {
        char[] array = str.toCharArray();

        int length = str.length();
        int n = length - 1;

        for (int i = 0; i < n; i++) {
            char from = array[i];
            char to = array[n];
            if (from == to) continue;
            array[i] = to;
            array[n] = from;
            --n;
        }

        return new String(array);
    }

}
