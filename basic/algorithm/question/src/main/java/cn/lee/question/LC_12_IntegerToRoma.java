package cn.lee.question;

public class LC_12_IntegerToRoma {
    private static final int[] ROMAN_NUM = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    private static final String[] ROMAN = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};


    public static void main(String[] args) {
        LC_12_IntegerToRoma program = new LC_12_IntegerToRoma();
        System.out.println(program.intToRoman(27));
        System.out.println(program.intToRoman(10));
        System.out.println(program.intToRoman(900));
    }


    public String intToRoman(int num) {

        StringBuilder builder = new StringBuilder();
        // 判断何时结束
        while (num != 0) {
            // 循环读取 ROMAN_NUM, 获取比当前数值 >= 的 ROMAN_NUM
            // 然后通过指定的数组下标进行替换
            // 因为 ROMAN 数部分数值是对 高位数减1 （ 9 = IX )
            // 程序中很难判断出来， 所以将此类数据直接写入到数组中. 方便后期程序执行.
            for (int i = 0; i < ROMAN_NUM.length; i++) {
                int roman = ROMAN_NUM[i];

                if (num >= roman) {
                    builder.append(ROMAN[i]);
                    num -= roman;
                    break;
                }
            }
        }
        return builder.toString();
    }
}
