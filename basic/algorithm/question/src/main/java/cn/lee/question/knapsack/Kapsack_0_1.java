package cn.lee.question.knapsack;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/7/12 10:33
 * @since VERSION
 */

public class Kapsack_0_1 {

    public static void main(String[] args) {
        Kapsack_0_1 solution = new Kapsack_0_1();

        int[] item = new int[]{3,2,4,7,3,1,7};
        int[] values = new int[]{5,6,3,19,12,4,2};
        int bag = 15;
        System.out.println(solution.solution(item, values,bag));
        System.out.println(solution.dynamicProgram(item, values,bag));
    }


    public int solution(int[] item, int[] valuable, int maxCapacity) {
        return recursive(item, valuable, 0, maxCapacity);
    }

    private int recursive(int[] item, int[] value, int index, int capacity) {
        // mark illegal result
        if (capacity < 0) return -1;
        if (index == item.length) return 0;


        int cap = item[index];

        int v = 0;
        int p1 = recursive(item, value, index + 1, capacity - cap);
        if (p1 != -1) {
            v = p1 + value[index];
        }
        int p2 = recursive(item, value, index + 1, capacity);
        return Math.max(v, p2);
    }

    private int dynamicProgram(int[] item, int[] value, int bag) {
        int[][] dp = new int[item.length + 1][bag + 1];

        for (int i = item.length - 1; i >= 0; i--) {
            for (int j = 0; j <= bag; j++) {
                // 不取当前
                int p1 = dp[ i + 1][j];
                int p2 = 0;

                int next = j - item[i] < 0 ? -1 : dp[i + 1][j - item[i]];
                if (next != -1) {
                    p2 = next + value[i];
                }

                dp[i][j] = Math.max(p1,p2);
            }
        }
        return dp[0][bag];
    }


}
