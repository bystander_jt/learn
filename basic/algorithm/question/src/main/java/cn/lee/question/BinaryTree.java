package cn.lee.question;


import cn.lee.question.basic.BinaryNode;
import netscape.security.UserTarget;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;

/**
 * 给一个二叉树，输入方式为 b{c{a{,g},}d{ef}} 其中{}内表示该节点左右儿子 [,]表示为NULL。要求按中序遍历输出，用例输出为agcbedf。
 * <pre>
 *                  b
 *                /   \
 *               c      d
 *             /       / \
 *            a       e   f
 *              \
 *               g
 *
 * </pre>
 * <pre>
 *     - 何为递归序?
 *     递归序是递归的顺序, 按照上方的二叉树进行递归, 那得到的是 b,c,a,a,g,g,g,a,c,c,b,d,e,e,e,d,f,f,f,d,b.
 *     - 先, 中, 后遍历
 *     先 中 后 则对应节点在何时打印. 我们通过不同时机控制输出即可实现 先 中 后 遍历.
 * </pre>
 */
public class BinaryTree<T> {

    public static void main(String[] args) {
        BinaryTree<Object> algorithm = new BinaryTree<>();

        BinaryNode binaryNode = new BinaryNode(1);
        BinaryNode left = new BinaryNode(2);
        BinaryNode right = new BinaryNode(3);

        left.left = new BinaryNode(4);
        left.right = new BinaryNode(5);

        right.left = new BinaryNode(6);
        right.right = new BinaryNode(7);


        binaryNode.left = left;
        binaryNode.right = right;

        algorithm.middleOrderUnRecursive(binaryNode);
        algorithm.middleOrderUnRecursiveV2(binaryNode);
        algorithm.middleOrderUnRecursivePractices(binaryNode);

        algorithm.widthPrioritySearch(binaryNode);
        algorithm.getMaxWidth(binaryNode);
        algorithm.getMaxWidthV2(binaryNode);

    }


    /**
     * 先序遍历. 先序遍历优先遍历 头-左-右 节点.
     * 打印: b,c,a,g,d,e,f
     *
     * @param node
     */
    private void preOrder(BinaryNode node) {
        if (node == null) return;
        System.out.println(node.val);
        preOrder(node.left);
        preOrder(node.right);
    }

    /**
     * 先序遍历, 不使用递归.
     * 压栈操作由用户手动完成.
     * 利用了栈的 FILO (First In Last Out) 的特性, 我们先将 node 的右侧节点存入栈中, 然后再存放左侧节点.
     *
     * @param node
     */
    private void preOderUnRecursive(BinaryNode node) {
        Stack<BinaryNode> stack = new Stack<>();

        BinaryNode cur = node;
        stack.push(node);

        while (!stack.isEmpty()) {
            BinaryNode pop = stack.pop();
            System.out.println(pop.val);
            if (pop.right != null) stack.push(pop.right);
            if (pop.left != null) stack.push(pop.left);
        }
    }

    /**
     * 中序遍历. 遍历路径 左-中-右 节点. 通过递归序, 可以推断出, 只要在当前 left 节点返回后打印即可实现中序遍历
     * 打印: a,g,c,b,e,d,f
     *
     * @param node
     */
    private void middleOrder(BinaryNode node) {
        if (node == null) return;
        ;

        middleOrder(node.left);
        System.out.println(node.val);
        middleOrder(node.right);
    }

    /**
     * 中序遍历, 不适用轮询机制.
     * <ol>
     *     <li>首先将所有左键点存放到 stack 中</li>
     *     <li>然后取 stack</li>
     *     <li>首先进行输出, 然后查看右侧节点是否有值
     *          <ul>
     *              <li>无: 将当前右节点放入 stack</li>
     *              <li>有: 首先存放当前右节点, 然后循环遍历获取所有左侧节点存放到 stack 中</li>
     *          </ul>
     *     </li>
     *     <li></li>
     * </ol>
     * @param node
     */
    private void middleOrderUnRecursive(BinaryNode node) {
        Stack<BinaryNode> order = new Stack<>();

        order.push(node);

        BinaryNode cur = node;
        // 存放所有的左节点
        while (cur.left != null) {
            order.push(cur.left);
            cur = cur.left;
        }

        // 这里开始循环
        while (!order.isEmpty()) {
            // 首先获取的是最底层的左节点, 然后逐级往上. 如果当前节点存在右节点, 则会遍历到右节点中
            BinaryNode pop = order.pop();

            System.out.print(pop.val + " ");

            // right node
            // 遍历右节点
            if (pop.right != null) {
                // 将右侧节点的所有左侧值存入 stack
                cur = pop.right;
                order.push(cur);
                while (cur.left != null) {
                    cur = cur.left;
                    order.push(cur);
                }
            }
        }
        System.out.println();
    }


    private void middleOrderUnRecursivePractices(BinaryNode node) {

        Stack<BinaryNode> nodes = new Stack<>();

        BinaryNode curv = node;

        // 所有左节点
        while (curv != null) {
            nodes.push(curv);
            curv = curv.left;
        }

        while (!nodes.isEmpty()) {
            BinaryNode nod = nodes.pop();
            System.out.print(nod.val + " ");

            // 如果右节点不为空, 则存放右节点
            if (nod.right != null) {
                curv = nod.right;

                // 存放右侧节点的所有子节点
                while (curv != null) {
                    nodes.push(curv);
                    curv = curv.left;
                }
            }
        }

        System.out.println();



    }

    private void middleOrderUnRecursiveV2(BinaryNode node) {

        if (node == null) return;
        Stack<BinaryNode> order = new Stack<>();
        while (!order.isEmpty() || node != null) {

            if (node != null) {
                order.push(node);
                node = node.left;
            } else {
                node = order.pop();
                System.out.print(node.val + " ");
                node = node.right;
            }

        }
        System.out.println();

    }

    /**
     * 后序遍历. 遍历路径 左-右-中 节点. 通过递归序, 可以推断出, 只要在当前 right 节点返回后打印即可实现中序遍历
     * 打印: g,a,c,b,e,f,d
     *
     * @param node
     */
    private void postOrder(BinaryNode node) {
        if (node == null) return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.println(node.val);
    }

    /**
     * 引入额外的 stack 记录压栈操作.
     * 再使用 order 进行压栈时, 我们通过 头-右-左 的顺序进行压栈. 然后我们在压栈的过程中记录当前栈值 (存放到 collect 中).
     * 在 order 所有处理完成后, 那我们压栈的操作通过 {@param Stack#pop} 进行获取, 就实现了 左-右-头 的后序遍历.
     * 在压 order 操作必须是 左 - 右, 这样压入到 collect 的则是先右再左, 后续可以直接输出后续遍历
     * <pre>
     *  |1| | | --> |3| |1| --> |2| |3| --> | | |2|
     *              |2|             |1|         |3|
     *                                          |1|
     *
     *  </pre>
     *
     * @param node
     */
    private void postOrderUnrecursive(BinaryNode node) {

        Stack<BinaryNode> order = new Stack<>();
        Stack<BinaryNode> collect = new Stack<>();

        order.push(node);

        while (!order.isEmpty()) {
            BinaryNode pop = order.pop();
            collect.push(pop);
            if (pop.left != null) order.push(pop.left);
            if (pop.right != null) order.push(pop.right);
        }

        while (!collect.isEmpty()) {
            System.out.println(collect.pop().val);
        }


    }


    /**
     * 宽度优先遍历, 将元素进行横向的打印过程.
     * @param node
     */
    private void widthPrioritySearch(BinaryNode node) {
        LinkedList<BinaryNode> fifo = new LinkedList<>();

        fifo.add(node);

        while (!fifo.isEmpty()) {
            BinaryNode no = fifo.pop();
            System.out.print(no.val);
            if (no.left != null) fifo.add(no.left);
            if (no.right != null) fifo.add(no.right);

        }

        System.out.println();

    }

    /**
     * 返回最宽的层节点
     * @param node
     * @return
     */
    private int getMaxWidth(BinaryNode node) {

        LinkedList<BinaryNode> fifo = new LinkedList<>();

        Map<BinaryNode, Integer> levelMap = new HashMap<>();

        levelMap.put(node, 0);
        fifo.add(node);

        int curLevel = 0;
        int curCount = 1;
        int max = Integer.MIN_VALUE;

        while (!fifo.isEmpty()) {
            BinaryNode no = fifo.pop();

            Integer level = levelMap.get(no);

            if (level == curLevel) {
                curCount++;
                max = Math.max(curCount, max);
            } else {
                curLevel++;
                curCount = 1;
            }


            if (no.left != null) {
                levelMap.put(no.left, curLevel + 1);
                fifo.add(no.left);
            }

            if (no.right != null){
                levelMap.put(no.right, curLevel + 1);
                fifo.add(no.right);
            }

        }

        System.out.println("max: " + max);
        return max;
    }

    /**
     * 获取二叉树的最大宽度. v2 采用 LinkedList 数组 + 部分变量实现
     * {@param curEnd} 代表当前层节点的末尾
     * {@param nextLevelEnd} 只要当前没有到末尾节点, 每次都将值更新成当前插入到链表中的节点.
     * {@param curLevelCnt} 当前层节点的数量.
     * 通过循环, 将每层节点进行遍历, 当遍历到 {@param curEnd} 时代表当前已到层节点末尾, 然后计算最大值. 并且将最后节点中插入到链表中的数最为下一层的结束节点. 然后清空 {@param curLevelCnt} 和 {@param nextLevelEnd}
     * 最后返回 {@param max} 即可
     * @param node
     * @return
     */
    private int getMaxWidthV2(BinaryNode node) {

        LinkedList<BinaryNode> list = new LinkedList<>();

        list.add(node);

        // 当前层的结束节点
        BinaryNode curEnd = node;

        // 最大值
        int max = Integer.MIN_VALUE;

        BinaryNode nextLevelEnd = null;
        int curLevelCnt = 1;

        while (!list.isEmpty()){

            BinaryNode pop = list.pop();

            if (pop.left != null) {
                nextLevelEnd = pop.left;
                list.add(pop.left);
            }
            if (pop.right != null) {
                nextLevelEnd = pop.right;
                list.add(pop.right);
            }

            ++curLevelCnt;

            if (curEnd == pop) {
                curEnd = nextLevelEnd;
                max = Math.max(curLevelCnt, max);
                curLevelCnt = 0;
                nextLevelEnd = null;
            }

        }

        System.out.println("max v2: " + max);
        return max;
    }
}
