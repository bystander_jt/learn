package cn.lee.question.leetcode;

import cn.lee.question.basic.Node;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/**
 * 将给与的节点进行复制, 复制出一个一摸一样的链路并进行返回
 */

public class $_CopyNodeAndReturnIt {


    public static void main(String[] args) {

        Random random = new Random();
        Node head = new Node(-1);

        Node holder = head;
        int[] placeHolder = new int[10];

        Map<Integer, Node> map = new TreeMap<>();

        for (int i = 0; i < 10; i++) {
            int val = random.nextInt(10);
            int rand = random.nextInt(10);
            while (i != 0 && placeHolder[rand] == 0) {
                rand = random.nextInt(10);
            }

            placeHolder[val] = placeHolder[val] + 1;

            Node node = new Node(val);
            holder.next = node;
            holder.random = map.get(rand);
            holder = node;

            map.put(val, node);
        }


        System.out.println(getString(head.next));
        $_CopyNodeAndReturnIt algorithm = new $_CopyNodeAndReturnIt();

        Node extraSpace = algorithm.withExtraSpace(head.next);
        Node noExtraSpace = algorithm.noExtraSpace(head.next);


        System.out.println("extra space compare result: " + algorithm.compare(head.next, extraSpace));
        System.out.println("no extra space compare result: " + algorithm.compare(head.next, noExtraSpace));


    }

    private boolean compare(Node original, Node copy) {

        Node oCur = original;
        Node cCur = copy;

        while (oCur != null) {
            if (oCur == cCur) return true;
            condi:
            {
                if (oCur.next == cCur.next) {
                    if (oCur.next == null) break condi;
                    return true;
                }

                if (oCur.random == cCur.random) {
                    if (oCur.random == null) break condi;
                    return true;
                }
            }

            oCur = oCur.next;
            cCur = cCur.next;
        }

        return false;

    }

    private static String getString(Node head) {

        StringBuilder builder = new StringBuilder("[");

        while (head != null) {
            builder.append(head.val)
                    .append("(")
                    .append(head.random == null ? "null" : head.random.val)
                    .append(")");
            if (head.next != null) {
                builder.append(", ");
            }
            head = head.next;
        }

        return builder.toString();
    }

    private static String getHex(Node head) {

        StringBuilder builder = new StringBuilder("[");

        while (head != null) {
            builder.append(Integer.toHexString(head.hashCode()))
                    .append("(")
                    .append(head.random == null ? "null" : Integer.toHexString(head.random.hashCode()))
                    .append(")");
            if (head.next != null) {
                builder.append(", ");
            }
            head = head.next;
        }

        return builder.toString();
    }


    private Node withExtraSpace(Node node) {

        Map<Node, Node> twins = new HashMap<>();

        Node cur = node;
        while (cur != null) {
            twins.put(cur, new Node(cur.val));
            cur = cur.next;
        }

        cur = node;
        while (cur != null) {
            twins.get(cur).next = twins.get(cur.next);
            twins.get(cur).random = twins.get(cur.random);
            cur = cur.next;
        }

        return twins.get(node);
    }

    private Node noExtraSpace(Node node) {

        Node next;
        Node cur = node;
        while (cur != null) {
            next = cur.next;
            cur.next = new Node(cur.val);
            cur.next.next = next;
            cur = next;
        }

        cur = node;
        Node res = cur.next;

        while (cur != null) {
            Node copy = cur.next;
            copy.random = cur.random != null ? cur.random.next : null;
            cur = cur.next.next;
        }

        cur = node;
        while (cur != null) {
            Node copy = cur.next;
            cur.next = copy.next != null ? copy.next : null;
            copy.next = copy.next != null ? copy.next.next : null;
            cur = cur.next;
        }

        return res;
    }


}
