package cn.lee.question;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * @since VERSION
 */

public class ZStringConvert {

    public static void main(String[] args) {
        String textString = "PAYPALISHIRING";
        Integer testNum = 3;
        ZStringConvert conver = new ZStringConvert();
        String convert = conver.convert(textString, testNum);
        System.out.println(convert);
    }


    public String convert(String s, int numRows) {
        List<StringBuilder> resultSet = new ArrayList<>(numRows);
        int i = 0, direction = -1;

        for (int j = 0; j < numRows; j++) {
            resultSet.add(new StringBuilder());
        }


        for (char c : s.toCharArray()) {
            StringBuilder builder = resultSet.get(i);
            builder.append(c);
            if (i == 0 || i == numRows - 1) direction = -direction;
            i += direction;
        }

        StringBuilder result = new StringBuilder();
        for (StringBuilder builder : resultSet){
            result.append(builder.toString());
        }

        return result.toString();

    }

}
