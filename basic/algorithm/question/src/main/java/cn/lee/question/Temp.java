package cn.lee.question;

import java.util.Scanner;

public class Temp {

    public static void main(String[] args) {
        System.out.println(encrypt("xy"));
        System.out.println(encrypt("abcde"));
        System.out.println(encrypt("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
        // 1 2 4 7
//
    }
    private static String encrypt(String str) {

        char[] c = str.toCharArray();
        long[] offset = new long[str.length()];
        StringBuilder builder = new StringBuilder();

        for ( int i = 0; i < str.length(); ++i ) {
            long m = 0;
            if (i <= 2) {
                m = (int)Math.pow(2, i);
            } else {
                m = offset[i - 1] + offset[i - 2] + offset[ i - 3];
            }
            offset[i] = m;

            long plus = m % 26;
            long r = c[i] + plus;
            if ( r > 'z'){
               r = r - 'z' + 'a' - 1;
            }


            builder.append((char)r);
        }
//        return "";
        return builder.toString();
    }


}
