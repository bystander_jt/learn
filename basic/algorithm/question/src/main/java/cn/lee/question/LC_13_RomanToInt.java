package cn.lee.question;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/7/12 09:33
 * @since VERSION
 */

public class LC_13_RomanToInt {

    public static void main(String[] args) {
        LC_13_RomanToInt to = new LC_13_RomanToInt();
        System.out.println(to.romanToInt("III"));
        System.out.println(to.romanToInt("MCMXCIV"));
    }


    public int romanToInt(String s) {
//
//        String[] ROMAN_ = new String[]{"I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"};
//        int[] ROMAN_NUM_ = new int[]{1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000};

        String[] ROMAN = new String[]{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] ROMAN_NUM = new int[]{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

        int result = 0;


        while (s.length() > 0) {
            for (int i = 0; i < ROMAN.length; i++) {
                if (s.startsWith(ROMAN[i])) {
                    result += ROMAN_NUM[i];
                    s = s.substring(ROMAN[i].length());
                    break;
                }
            }
        }

        return result;


    }


}
