package cn.lee.question.basic;

public class ListNodeBasic {



    public String getString(ListNode node) {
        StringBuilder builder = new StringBuilder("[");

        while (node != null) {
            builder.append(node.val);
            if (node.next != null) builder.append(", ");
            node = node.next;
        }

        return builder.append("]").toString();
    }


}
