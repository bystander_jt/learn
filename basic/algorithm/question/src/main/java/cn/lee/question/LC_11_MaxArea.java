package cn.lee.question;

import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * 
 * @since VERSION
 */

public class LC_11_MaxArea {

    public static void main(String[] args) {
        LC_11_MaxArea maxArea = new LC_11_MaxArea();
        System.out.println(maxArea.maxArea(new int[]{1,3,2,5,25,24,5}));
    }

    public int maxArea(int[] height){
        int left = 0, right = height.length - 1;
        int maxCapacity = 0;

        int times = 0;

        while (times < height.length ) {
            if (left == right) return maxCapacity;

            int leftLength = height[left];
            int rightLength = height[right];

            int smaller = rightLength < leftLength ? rightLength : leftLength;
            int gap = right - left;
            int capacity = (gap * smaller);

            maxCapacity = capacity > maxCapacity ? capacity : maxCapacity;
            if (leftLength > rightLength) --right;
            if (leftLength <= rightLength) ++left;
            ++times;
        }

        return maxCapacity;
    }


}
