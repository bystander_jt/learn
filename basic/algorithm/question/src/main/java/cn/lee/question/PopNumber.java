package cn.lee.question;

public class PopNumber {

    /**
     * 寻找 {@code arr} 中出现奇数次的数
     *
     * @param arr
     */
    private static void findOne(int[] arr) {

        int eor = 0;
        for (int i : arr) {
            eor |= i;
        }

        System.out.println(eor);

    }


    /**
     * 寻找 {@code arr} 中出现奇数次的2个数
     *
     * @param arr
     */
    public static void printOddTimesNum2(int[] arr) {
        int eor = 0;
        for (int i = 0; i < arr.length; i++) {
            eor ^= arr[i];
        }

        // 上方通过 异或 运算的出了两个出现次数为奇数的异或值 a^b

        int rightOne = eor & (~eor + 1); // 提取出最右的1
        int onlyOne = 0; // eor'
        for (int cur : arr) {
            if ((cur & rightOne) == 1) {
                onlyOne ^= cur;
            }
        }
        System.out.println(onlyOne + " " + (eor ^ onlyOne));
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 2, 3, 5, 5};
        printOddTimesNum2(arr);
    }

}
