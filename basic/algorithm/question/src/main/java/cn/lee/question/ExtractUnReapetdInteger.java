package cn.lee.question;

public class ExtractUnReapetdInteger {

    public static void main(String[] args){

        System.out.println(readUnReapteNumber(9876673));
        System.out.println(readUnReapteNumber(2752771));

    }

    private static Integer readUnReapteNumber(Integer input){
        String in = String.valueOf(input);
        int[] resultArray = new int[in.length()];

        int size = 0;
        int num  = 0;

        W:
        while (input > 0){
            num = input % 10;
            input /= 10;
            for (int i = 0; i < size; ++i){
                if (resultArray[i] == num) continue W;
            }
            resultArray[size] = num;
            ++size;
        }

        int result = 0;
        for (int i = 0; i < size; ++i){
            int plus = (int) Math.pow(10, size - i - 1);
            result += (resultArray[i] * plus);
        }

        return result;
    }


}
