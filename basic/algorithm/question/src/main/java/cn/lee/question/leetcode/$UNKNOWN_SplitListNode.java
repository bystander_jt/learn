package cn.lee.question.leetcode;

import cn.lee.question.basic.ListNode;
import cn.lee.question.basic.ListNodeBasic;

import java.util.Random;

/**
 * 将 ListNode 按照指定值进行拆分, 将 < 的放左边, 将 = 放中间, 将 > 放右边.
 */
public class $UNKNOWN_SplitListNode extends ListNodeBasic {

    public static void main(String[] args) {
        ListNode node = new ListNode(3);
        ListNode insert = node;

        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            ListNode next = new ListNode(random.nextInt(10));
            insert.next = next;
            insert = next;
        }
        $UNKNOWN_SplitListNode algorithm = new $UNKNOWN_SplitListNode();

        System.out.println("before: " + algorithm.getString(node));


        ListNode listNode = algorithm.sixParams(node, 5);

        System.out.println("after: " + algorithm.getString(listNode));
    }


    public ListNode sixParams(ListNode node, int target) {

        ListNode lh = null;
        ListNode lt = null;

        ListNode mh = null;
        ListNode mt = null;

        ListNode gh = null;
        ListNode gt = null;

        while (node != null) {
            ListNode next = node.next;
            node.next = null;

            if (node.val < target) {
                if (lh == null) lh = lt = node;
                else {
                    lt.next = node;
                    lt = node;
                }
            } else if (node.val > target) {
                if (gh == null) gh = gt = node;
                else {
                    gt.next = node;
                    gt = node;
                }
            } else {
                if (mh == null) mh = mt = node;
                else {
                    mt.next = node;
                    mt = node;
                }
            }

            node = next;
        }

        // 如果没有 < = 区域则会报错, 所以我们需要确定边界范围

        // 判断小于区域是否为空
        if (lt != null) {
            // 如果不为空, 则和 = 区域中的头节点进行关联
            lh.next = mh;
            // 更新 = 区域的头节点, 判断是否为空, 如果为空默认使用当前 < 区域的值
            mt = mt == null ? lt : mt;
        }

        // = 区域是否为空
        if (mt != null) {
            // 不为空则将 > 区域的值关联到 = 区域的末尾
            mt.next = mh;
        }

        // 如果 < 区域不为空, 则返回 < 区域. 否则从 = 或 > 区域中获取 != null 的区域
        return lt != null ? lt : (mt != null ? mt : gt);
    }


    private void update(ListNode head, ListNode tail, ListNode src) {
        if (head == null) head = tail = src;
        else {
            tail.next = src;
            tail = src;
        }
    }

}
