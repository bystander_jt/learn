package cn.lee.question;

/**
 * <a href=
 *
 * @since VERSION
 */

public class PrimerNumber {

    public static void main(String[] args) {
        decode(2000000014L);
    }


    private static void decode(Long num) {
        for (long i = 2; i <= num; ++i) {
            if (i == 9) {
                System.out.print(num + " ");
                num /= num;
                break;
            }
            while (num % i == 0) {
                System.out.print(i + " ");
                num /= i;
            }
        }
        System.out.println();
    }


}
