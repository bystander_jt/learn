package cn.lee.question;

import java.util.Arrays;

/**
 *
 */

public class LC_16_ThreeSumClosest {

    public static void main(String[] args) {
        LC_16_ThreeSumClosest resolve = new LC_16_ThreeSumClosest();
        resolve.threeSumClosest(new int[]{-1, 2, 1, -4}, 1);
    }

    public int threeSumClosest(int[] nums, int target) {
        // 排序
        Arrays.sort(nums);
        // 定义另外2个数字下标
        int left, right;
        // 离 target 距离最近的值
        int best = Integer.MAX_VALUE;
        // for 循环代表 A 坐标 (left, right 分别代表 B,C 下标)
        for (int i = 0; i < nums.length - 1; ++i) {
            // 如果这个数和前一个相同, 则跳过
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            // 重新设定 left 为当前 i + 1;
            left = i + 1;
            // 重新设定当前 right 为数组末尾
            right = nums.length - 1;
            // 循环判断获取最接近的数
            while (left < right) {
                // 计算三个下标的总数
                int sum = nums[i] + nums[left] + nums[right];
                // 如果 == target 则直接返回 target
                if (sum == target) return target;
                // 计算出总和和最接近数值 - 目标值 的数, 赋予 best
                if (Math.abs(sum - target) < Math.abs(best - target)) {
                    best = sum;
                }
                //更新 left, right 指针位置
                if (sum < target) {
                    ++left;
                } else {
                    --right;
                }
            }
        }

        return best;


    }

}
