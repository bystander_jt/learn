package cn.lee.multi.thread;


/**
 * 顺序重复输出字符串。
 */
public class OrderedOutputMultiThreadCompete {


        private final int times; // 控制打印次数
        private  volatile int state;   // 当前状态值：保证三个线程之间交替打印
        private final Object lock = new Object();

        public OrderedOutputMultiThreadCompete(int times) {
            this.times = times;
        }

        private void printLetter(String name, int targetNum) {
            for (int i = 0; i < times; ) {
                synchronized (lock) {
                    if (state % 3 == targetNum) {
                        state++;
                        i++;
                        System.out.print(name);
                    }
                }
            }
        }

        public static void main(String[] args) {
            OrderedOutputMultiThreadCompete loopThread = new OrderedOutputMultiThreadCompete(10);

            new Thread(() -> loopThread.printLetter("B", 1)).start();
            new Thread(() -> loopThread.printLetter("A", 0)).start();
            new Thread(() -> loopThread.printLetter("C", 2)).start();

        }
}
