package cn.lee.multi.thread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class OrderedOutputWithReentrantLock {

    private static final ReentrantLock lock = new ReentrantLock();

    private static volatile Integer index = 1;

    private static void print(String outputCharacter, Condition condition, Integer sequence, Integer currentIndex, Integer count) throws InterruptedException {

        while (true) {
            try {
                lock.lock();
                if (index % count != sequence) {
                    condition.await();
                }
                String threadName = Thread.currentThread().getName();
                System.out.println(threadName + " " + outputCharacter);
                index++;
                condition.signalAll();
            } finally {
                lock.unlock();
            }
        }


    }


    public static void main(String[] args) throws InterruptedException {
        Condition condition = lock.newCondition();


        Thread t0 = new Thread(() -> {
            try {
                print("0", condition, 0, index, 4);
            } catch (InterruptedException e) {

            }
        }, "T0");
        Thread t1 = new Thread(() -> {
            try {
                print("A", condition, 1, index, 4);
            } catch (InterruptedException e) {

            }
        }, "T1");
        Thread t2 = new Thread(() -> {
            try {
                print("B", condition, 2, index, 4);
            } catch (InterruptedException e) {

            }
        }, "T2");
        Thread t3 = new Thread(() -> {
            try {
                print("C", condition, 3, index, 4);
            } catch (InterruptedException e) {

            }
        }, "T3");
        Thread t4 = new Thread(() -> {
            try {
                print("D", condition, 4, index, 4);
            } catch (InterruptedException e) {

            }
        }, "T4");

        t0.start();
        t1.start();
        t2.start();
        t3.start();
        t4.start();

//        Thread.sleep(10000);
//
//        t1.interrupt();
//        t2.interrupt();
//        t3.interrupt();
//        t4.interrupt();

    }

}
