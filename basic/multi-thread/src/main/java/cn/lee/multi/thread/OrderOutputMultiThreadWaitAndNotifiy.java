package cn.lee.multi.thread;


/**
 * 顺序重复输出字符串。
 */
public class OrderOutputMultiThreadWaitAndNotifiy {


    private final int times; // 控制打印次数
    private volatile int state;   // 当前状态值：保证三个线程之间交替打印
    private static final Object lock = new Object();

    public OrderOutputMultiThreadWaitAndNotifiy(int times) {
        this.times = times;
    }

    private void printLetter(String name, int targetNum) {
        for (int i = 0; i < times; i++) {
            synchronized (lock) {
                while (state % 3 != targetNum) {
                    try {
                        // wait 会释放当前锁给其他正在争抢的线程
                        lock.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                System.out.println(name);
                state += 1;
                // 通知所有等待队列中的锁线程，并将其唤醒
                lock.notifyAll();
            }
        }
    }

    public static void main(String[] args) {

        OrderOutputMultiThreadWaitAndNotifiy ordered = new OrderOutputMultiThreadWaitAndNotifiy(10);
        new Thread(() -> ordered.printLetter("A", 0)).start();
        new Thread(() -> ordered.printLetter("B", 1)).start();
        new Thread(() -> ordered.printLetter("C", 2)).start();

    }
}
