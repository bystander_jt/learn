package cn.lee.multi.thread;

public class NumberOutput {


    private static Object monitor = new Object();


    private static final int startNum = 0;

    private static final char startChar = 'A';

    private final int count;

    private volatile Object cumulate;

    public NumberOutput(int count, Object cumulate) {
        this.count = count;
        this.cumulate = cumulate;
    }


    private void print() {
        synchronized (monitor) {
            for (int i = 0; i < count; i++) {
                if (cumulate instanceof Integer) {
                    int t = (int) cumulate;
                    System.out.print(t);
                    cumulate = t + 1;
                } else if (cumulate instanceof Character) {
                    char t = (char) cumulate;
                    System.out.print((char) ('A' + i));
                    cumulate = (char) (t + 1);
                }

                monitor.notify();
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            monitor.notifyAll();
        }

        System.out.println();

    }


    public static void main(String[] args) {

        NumberOutput charOutput = new NumberOutput(10, startChar);
        NumberOutput numOutput = new NumberOutput(10, startNum);

        new Thread(charOutput::print).start();
        new Thread(numOutput::print).start();

    }


}
