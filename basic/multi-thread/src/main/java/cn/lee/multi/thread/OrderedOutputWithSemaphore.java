package cn.lee.multi.thread;

import java.util.concurrent.Semaphore;

public class OrderedOutputWithSemaphore {


    //设置信号量，用来控制可以同时访问的线程数量
    public static Semaphore s1 = new Semaphore(1);
    public static Semaphore s2 = new Semaphore(0);
    public static Semaphore s3 = new Semaphore(0);
    private static volatile Integer INDEX = 1;
    public int i = 1;

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(() -> print(s1, s2), "t1");
        Thread t2 = new Thread(() -> print(s2, s3), "t2");
        Thread t3 = new Thread(() -> print(s3, s1), "t3");

        t1.start();
        t2.start();
        t3.start();

        Thread.sleep(1000 * 60);

        t1.interrupt();
        t2.interrupt();
        t3.interrupt();
    }

    private static void print(Semaphore acquire, Semaphore release) {
        try {
            while (true) {
                acquire.acquire();
                System.out.println(Thread.currentThread().getName() + " " + INDEX++);
                Thread.sleep(1000);
                release.release();
            }
        } catch (InterruptedException e) {

        }
    }

}
