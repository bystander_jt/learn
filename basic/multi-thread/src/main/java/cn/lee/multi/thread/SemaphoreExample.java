package cn.lee.multi.thread;

import java.util.concurrent.Semaphore;

public class SemaphoreExample {


    public static void main(String[] args) {
        // Create a semaphore with a count of 1
        Semaphore semaphore = new Semaphore(1);

        // Create two threads that try to access a shared resource
        Thread thread1 = new Thread(new SharedResource(semaphore, "Thread 1"));
        Thread thread2 = new Thread(new SharedResource(semaphore, "Thread 2"));

        // Start the threads
        thread1.start();
        thread2.start();
    }
}

class SharedResource implements Runnable {
    private final Semaphore semaphore;
    private final String threadName;

    public SharedResource(Semaphore semaphore, String threadName) {
        this.semaphore = semaphore;
        this.threadName = threadName;
    }

    public void run() {
        try {
            // Acquire the semaphore
            System.out.println(threadName + " is waiting for a permit.");
            semaphore.acquire();
            System.out.println(threadName + " gets a permit.");

            // Access the shared resource
            for (int i = 0; i < 5; i++) {
                System.out.println(threadName + " is accessing the shared resource.");
                Thread.sleep(1000);
            }

            // Release the semaphore
            semaphore.release();
            System.out.println(threadName + " releases the permit.");
        } catch (InterruptedException e) {

        }
    }
}
