package cn.lee.multi.thread;

import java.util.ConcurrentModificationException;
import java.util.concurrent.*;

public class MultiThreadLearning {

    private static final ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 2, 0, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<>(10));

    public static void main(String[] args) throws InterruptedException {
        MultiThreadLearning multiThreadLearning = new MultiThreadLearning();
//        multiThreadLearning.countdownLatch();
        multiThreadLearning.submit();
        executor.shutdown();

    }


    public void countdownLatch() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);
        for (int i = 0; i < 2; i++) {
            executor.execute(() -> {
                try {
                    Thread.sleep(2000);
                    latch.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        latch.await();
        System.out.println("finish");
    }

    public void submit() {

        Future<Integer> future1 = executor.submit(() -> {
            Thread.sleep(2000);
            return 1;
        });
        Future<Integer> future2 = executor.submit(() -> {
            Thread.sleep(1000);
            return 2;
        });
        Future<Integer> future3 = executor.submit(() -> {
            Thread.sleep(2000);
            throw new ConcurrentModificationException();
//            return 3;
        });

        Integer s1 = 0, s2 = 0, s3 = 0;

        try {

            s3 = future3.get();
            s1 = future1.get();
            s2 = future2.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println(String.format("%d %d %d", s1, s2, s3));
    }


}
