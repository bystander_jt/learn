package cn.lee.multi.thread;


/**
 * 顺序重复输出字符串。
 */
public class OrderedOutput  {


    private int state;
    private int times;

    private final Object lock;
//    private static final Object LOCK = new Object();

    public OrderedOutput(int times, Object lock) {
        this.times = times;
        this.lock = lock;
    }

    private void printLetter(String name, int targetState) {
        for (int i = 0; i < times; i++)
            synchronized (lock) {
                while (state % 3 != targetState) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                state++;
                System.out.print(name);
                lock.notifyAll();
            }
    }

    public static void main(String[] args) {

        OrderedOutput printABC = new OrderedOutput(10, new Object());
        new Thread(() -> {
            printABC.printLetter("A", 0);
        }, "A").start();
        new Thread(() -> {
            printABC.printLetter("B", 1);
        }, "B").start();
        new Thread(() -> {
            printABC.printLetter("C", 2);
        }, "C").start();
    }
}
