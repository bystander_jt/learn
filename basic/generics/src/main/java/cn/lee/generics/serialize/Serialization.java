package cn.lee.generics.serialize;

import java.io.*;

/**
 * Java 序列化和反序列化问题
 * 1. 当一个类被序列化后, 如果 serialVersionUID 不相同, 则会报错</br>
 * 2. 反序列化一个对象时, 如果类中缺少字段, 则会丢失该字段属性</br>
 */

public class Serialization {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // serialize
        String path = "F:\\Download\\serialize";
        SerializeObject a = new SerializeObject("123","321","213");

        serialize(a, path);
        SerializeObject serializeObject = deSerialize(path);

        System.out.println(serializeObject);

    }

    private static void serialize(SerializeObject obj, String path){

        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(obj);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    private static SerializeObject deSerialize(String path) throws IOException, ClassNotFoundException {
        FileInputStream is = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(is);

        return (SerializeObject) ois.readObject();
    }


}
