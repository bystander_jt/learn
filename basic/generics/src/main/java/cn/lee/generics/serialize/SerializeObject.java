package cn.lee.generics.serialize;

import java.io.Serializable;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/8 15:00
 * @since VERSION
 */

public class SerializeObject implements Serializable {

    private static final long serialVersionUID = -2725074996808126163L;

    String a;
    String b;
    String c;

    public SerializeObject() {
    }

    public SerializeObject(String a, String b, String c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public SerializeObject(String a, String b) {
        this.a = a;
        this.b = b;
    }
}
