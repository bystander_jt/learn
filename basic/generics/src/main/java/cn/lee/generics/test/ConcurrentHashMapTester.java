package cn.lee.generics.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapTester {


    public static void main(String[] args) {
//        testConcurrentHashMapPutKV();
        testTableSizeFor();
        integerTest();

    }

    private static void testConcurrentHashMapPutKV() {
        ConcurrentHashMap<Object, Object> map = new ConcurrentHashMap<>();
        map.put(null, 1);
        map.put(1, null);
    }

    private static void testTableSizeFor() {
        ConcurrentHashMapTester tester = new ConcurrentHashMapTester();
        int input = 31 + (31 >>> 1) + 1;
        int output = tester.tableSizeFor(input);
        System.out.println(String.format("tableSizeFor input: %d, output: %d", input, output));
    }

    private static void integerTest(){
        int i = Integer.numberOfLeadingZeros(10);
        System.out.println("10 has " + i + " 0-bits total");
    }


    /**
     * 将 c 进行二进制补全.
     * <p>将 100 补全至 111
     * <p>101010101 -> 111111111 </p>
     * @param c
     * @return
     */
    int tableSizeFor(int c) {
        int n = c - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return n;
    }


}
