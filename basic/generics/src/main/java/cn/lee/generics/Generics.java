package cn.lee.generics;


import cn.lee.generics.pojo.ChildObject;
import cn.lee.generics.pojo.ParentObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 泛型题 一
 */
public class Generics {

    public static void main(String[] args) {

        // 当一个类型修饰了泛型, 则可以存放当前泛型对象或者继承于当前泛型类的对象
        List<ParentObject> parentList = new ArrayList<>();
        parentList.add(new ChildObject());

        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<String> stringList = new ArrayList<>();

        // 因为泛型不相符, 所以无法赋值传递.
//        objectList = stringList;

        // 因为是 Object 类型, 所以可以存放 (所有对象的父级都是 Object)
        objectList.add(Boolean.FALSE);
        Object o = objectList.get(0);


        List<Double> doubleList = new ArrayList<>();

//        int sum = sum(doubleList);
        int sumExtends = sumExtends(doubleList);
    }

    public static int sum(List<Number> list) {
        int sum = 0;
        for (Number number : list) {
            sum += number.intValue();
        }
        return sum;
    }


    public static int sumExtends(List<? extends Number> list) {
        int sum = 0;
        for (Number number : list) {
            sum += number.intValue();
        }
        return sum;
    }

    public static int sumPut(List<? extends Number> list) {
        int sum = 0;
//        list.add(1);
        list.remove(0L);
        for (Number number : list) {
            sum += number.intValue();
        }
        return sum;
    }


    public static void sumSuperPut(List<? super Number> list) {
        list.add(1);
        list.add(1D);
        list.add(1F);
        list.add(1L);
        list.remove(0);
    }

}
