package cn.lee.generics;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class MThread {

    private static volatile Map<Long, Long> map = new HashMap<>(0);

    public static void main(String[] args) throws InterruptedException {

        Thread thread0 = new Thread(new CRunnable(), "t-0");
        Thread thread1 = new Thread(new CRunnable(), "t-1");
        Thread thread2 = new Thread(new CRunnable(), "t-2");
        Thread thread3 = new Thread(new CRunnable(), "t-3");
        Thread thread4 = new Thread(new CRunnable(), "t-4");
        Thread thread5 = new Thread(new CRunnable(), "t-5");
        Thread thread6 = new Thread(new CRunnable(), "t-6");

        thread0.start();
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();

        Thread.sleep(1000 * 6);

        map.put(0L, 0L);
        Thread.sleep(1000 * 6);

    }

    static class CRunnable implements Runnable {


        @Override
        public void run() {
            while (true) {
                if (map.size() > 0) {
                    System.out.println("map changed");
                    break;
                }
            }

        }
    }
}
