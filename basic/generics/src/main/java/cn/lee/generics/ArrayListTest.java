package cn.lee.generics;

import java.util.ArrayList;

public class ArrayListTest {

    public static void main(String[] args) {
        ArrayList<Integer> strings = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            strings.add(i);
        }

        Object[] objects = strings.toArray();

        int start = 5, end = 8;

        System.arraycopy(objects, end, objects, start, objects.length - end);
        System.out.println(objects);
    }

}
