package cn.lee.generics;

import java.util.Objects;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/8/25 13:28
 * @since VERSION
 */

public class TernaryOperation {

    public static void main(String[] args) {

        A a = new A();
        B b = new B();
        // 此处会报空指针, 因为三元运算符的确认条件是原始类型, 而 getData 是装箱类型, 装箱类型拆箱后不能为空
        b.setData(Objects.isNull(a) ? 0 : a.getData());
        b.setData(Objects.isNull(a) ? Integer.valueOf(0) : a.getData());


    }



    static class A {
        private Integer data;

        public Integer getData() {
            return data;
        }

        public void setData(Integer data) {
            this.data = data;
        }
    }

    static class B {
        private Integer data;

        public Integer getData() {
            return data;
        }

        public void setData(Integer data) {
            this.data = data;
        }
    }


}
