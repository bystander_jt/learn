package cn.lee.generics;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 静态绑定和动态绑定的区别 </p>
 * <table>
 *     <tr>
 *         <td>静态绑定</td>
 *         <td>静态绑定是指在类编译时就已经知道方法调用的是哪个类中的哪个方法. 只有当方法被 `static` `final` `private` 构造器 修饰的才会出现</td>
 *     </tr>
 *     <tr>
 *         <td>动态绑定</td>
 *         <td>则是说明这个方法存在多个重名的方法(重写, 复写). 当一个类被继承, 且重写了父类的方法, 那会在运行时绑定一个虚拟方法, 通过虚拟方法再调用正确的方法 </td>
 *     </tr>
 * </table>
 */

public class DifferentOfStaticBindAndDynamicBind {

    public static void main(String[] args) {
        Collection c = new HashSet();
        print(c);
    }


    public static void print(Collection c){
        System.out.println("collection");
    }

    public static void print(Set c){
        System.out.println("set");
    }

    public static void print(HashSet c){
        System.out.println("hashset");
    }


    static class Main {
        public static void main(String[] args) {
            // An example of static and dynamic binding in Java
            Insurance current = new CarInsurance();

            // dynamic binding based upon object
            int premium = current.premium();

            // static binding based upon class
            String category = current.category();

            System.out.println("premium : " + premium);
            System.out.println("category : " + category);

        }

        static class Insurance{
            public static final int LOW = 100;
            public int premium(){
                return LOW;
            }
            public static String category(){
                return "Insurance";
            }
        }

        static class CarInsurance extends Insurance{
            public static final int HIGH = 200;
            public int premium(){
                return HIGH;
            }
            public static String category(){
                return "Car Insurance";
            }
        }

        //Output premium : 200
        //category : Insurance

    }

}
