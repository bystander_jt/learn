package com.example.eurekaclient.controller;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lijiangtao
 * 
 */

@RestController
@RequestMapping("/")
public class IndexController {

    @Value("${server.port}")
    private Integer port;

    @GetMapping("/index")
    public JsonObject index(){
        JsonObject objec = new JsonObject();
        return objec;
    }

}
