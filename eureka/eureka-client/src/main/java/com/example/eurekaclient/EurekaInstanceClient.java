package com.example.eurekaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class EurekaInstanceClient {

    public static void main(String[] args) {
        SpringApplication.run(EurekaInstanceClient.class, args);
    }

}
