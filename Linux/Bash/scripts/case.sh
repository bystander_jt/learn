#!/bin/bash
# 第一行用于告诉系统是使用哪个解释器执行下面的命令

a="c"

function example_case(){

	case ${a} in
	"a")
		echo "Value is a"
	;;
	"b")
		echo "Value is b"
	;;
	"c")
		echo "Value is c"
	;;
	esac

}

example_case

unset a