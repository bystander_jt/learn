#!/bin/bash
# 第一行用于告诉系统是使用哪个解释器执行下面的命令

function example_for_filename(){

	for FILE_NAME in $(ls | grep .sh); do
		echo "filename : ${FILE_NAME}"
	done
}

function example_for_array(){

	ARRAY=(a b c d e f g)
	
	for letter in ${ARRAY[@]}; do
		echo "letter in array : ${letter}"
	done
}
function example_for_remove_array(){

	ARRAY=(a b c d e f g)
	
	echo " before remove : ${ARRAY[*]}"
	for letter in ${ARRAY[@]}; do
		#echo "remove letter in array : ${letter}"
		ARRAY=( "${ARRAY[@]/${letter}}" )
	done
	echo " after remove : ${ARRAY[*]}"
	
}


example_for_filename

example_for_array

example_for_remove_array