#!/bin/bash


function add_array(){

	ARRAY=()
	
	ARRAY+="a "
	ARRAY+="b "
	ARRAY+="c "
	
	#不加空格就连在一起了
	
	echo "${ARRAY[@]}"

}

function remove_element(){

	# 变量在任意位置申明, 则整个shell环境都可用
	
	REMOVE_ELEMENT=a
	ARRAY=( "${ARRAY[@]/${REMOVE_ELEMENT}}" )
	
	echo "ARRAY : ${ARRAY[*]}"
}

function loop_remove(){

	echo "ARRAY befor remove: ${ARRAY[*]}"
	for target in "${ARRAY[@]}"; do
		  unset 'ARRAY[i]'
	done

	echo "ARRAY after remove: ${ARRAY[*]}"
}


add_array

#remove_element

loop_remove