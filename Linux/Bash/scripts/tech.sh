#!/bin/bash
# 第一行用于告诉系统是使用哪个解释器执行下面的命令


echo_command first
function echo_command(){

	NUM=$1
	# 将指令传递的首个参数赋值于 NUM
    
	echo "This is ${NUM:-UNKNOWN} echo command"
	# 指令 当前是向控制台输出信息

}

function echo_num(){
	echo "current NUM:${NUM}"
}

echo_command second
echo_command third
