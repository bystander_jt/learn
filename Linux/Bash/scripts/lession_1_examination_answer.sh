#!/bin/bash

# 给定以下内容, 请将其在控制台打印出所属领域 后端 前端 android
# 例子: Java -> 后端
ARRAY=(Java Javascript Vue html lambad android layout)

function execute_if(){

	for ITEM in ${ARRAY[@]}; do
		AREA=
		if [ ${ITEM} == "Java" ]; then
			AREA="后端"
		elif [ ${ITEM} == "Javascript" ]; then
			AREA="前端"
		elif [ ${ITEM} == "Vue" ]; then
			AREA="前端"
		elif [ ${ITEM} == "html" ]; then
			AREA="前端"
		elif [ ${ITEM} == "lambad" ]; then
			AREA="后端"
		elif [ ${ITEM} == "android" ]; then
			AREA="安卓"
		elif [ ${ITEM} == "layout" ]; then
			AREA="安卓"
		fi
		
		echo "${ITEM} -> ${AREA}"
	done


}

function execute_case(){

	for ITEM in ${ARRAY[@]}; do
		AREA=
		case ${ITEM} in
		"Java")
			AREA="后端"
		;;			
		"Javascript")
			AREA="前端"
		;;			
		"Vue")
			AREA="前端"
		;;			
		"html")
			AREA="前端"
		;;			
		"lambad")
			AREA="后端"
		;;			
		"android")
			AREA="安卓"
		;;			
		"layout")
			AREA="安卓"
		;;
		esac
		
		echo "${ITEM} -> ${AREA}"
	done


}

echo "print using if"
execute_if
echo ""

echo "print using case"
execute_case