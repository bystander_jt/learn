#!/bin/bash
# 第一行用于告诉系统是使用哪个解释器执行下面的命令

a=1

function example_if(){

	if [ ${a} -eq 1 ];
	then
		echo "a = 1"
	fi
}

function example_if_else(){

	
	if [ ${a} -gt 0 ];
	then
		echo " a is bigger  than 0 or a is lower than 9"
	else
		echo " a is less than 0"
	fi
}

function example_if_or(){


	if [[ ${a} -gt 0 || ${a} -gt 9 ]];
	then
		echo " a is bigger than 0 or a is lower than 9"
	fi


	if [ ${a} -gt 0 ] || [ ${a} -gt 9 ];
	then
		echo " a is bigger than 0 or a is lower than 9"
	fi

}

function example_if_and(){

	if [[ ${a} -gt 0 && ${a} -lt 9 ]];
	then
		echo " a is bigger than 0 and a is lower than 9"
	fi


	if [ ${a} -gt 0 ] && [ ${a} -lt 9 ];
	then
		echo " a is bigger than 0 and a is lower than 9"
	fi

}

function example_if_not(){

	if [[ ! ${a} -lt 0 && ! ${a} -gt 9 ]];
	then
		echo " a is bigger than 0 and a is lower than 9"
	fi


	if ! [[ ${a} -lt 0 && ${a} -gt 9 ]];
	then
		echo " a is bigger than 0 and a is lower than 9"
	fi

}


function example_if_elif(){

	if [[ ${a} -lt 0 ]];then
		echo "a is less or equal 0"
	elif ! [[ ${a} -lt 0 ]];then
		echo "a is not less or equal 0"
	fi

}

example_if_elif

unset a