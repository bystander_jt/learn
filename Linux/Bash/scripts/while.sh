#!/bin/bash
# 第一行用于告诉系统是使用哪个解释器执行下面的命令

a="a"

function example_while(){
	
	COUNT=0
	
	while [ ${a} == "a" ]; do
		echo "${a} equals a"
		
		COUNT=$((COUNT+1))
		# 自增
		
		if [[ ${COUNT} -gt 10 ]]; then
			a="b"
		fi
		
		sleep 1s
		# 休眠1秒钟
		
	done
	
	echo "a = ${a}"
}

example_while

unset a