package cn.lee.middleware.emxq;

/**
 * @author <a href=mailto:lijiangtao@tpson.cn> lijiangtao</a>
 * @date 2022/9/6 14:30
 * @since VERSION
 */

public class MqttCodec {


    public static void main(String[] args) {
        encodeLength();
        decodeLength();
    }

    private static int decodeLength(){
        int multiplier = 1;
        int value = 0;
        String encodeByte = "Hello World";

        while ((encodeByte.length() & 0x80) != 0){
            value += (encodeByte.length() & 0x80 ) * multiplier;
            multiplier *= 128;
            if (multiplier > Math.pow(128, 3)){
                throw new IllegalArgumentException("编解码错误");
            }
        }

        System.out.println(value);
        return value;
    }


    private static int encodeLength(){

        int X = 10000;
        int encodedByte = 0;

        while ( X > 0 ){
            encodedByte = X % 128;
            X = X / 128;
            if ( X > 0 )
                encodedByte = encodedByte | 128;
            else
                System.out.println(encodedByte);
        }
        return encodedByte;
    }


}
