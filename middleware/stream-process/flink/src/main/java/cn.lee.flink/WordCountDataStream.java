package cn.lee.flink;

import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.ExecutionOptions;
import org.apache.flink.connector.file.src.FileSource;
import org.apache.flink.connector.file.src.reader.TextLineInputFormat;
import org.apache.flink.core.fs.Path;
import org.apache.flink.shaded.netty4.io.netty.util.internal.logging.InternalLogger;
import org.apache.flink.shaded.netty4.io.netty.util.internal.logging.InternalLoggerFactory;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

import java.time.Duration;


public class WordCountDataStream {

    private static final InternalLogger log = InternalLoggerFactory.getInstance(WordCountDataStream.class);
    private static final String FILE_PATH =
            "/Users/bystander/Documents/developer/Git_Library/learn/middleware/stream-process/flink/src/main/resources/stream-data/word-count.txt";

    public static void main(String[] args) throws Exception {


        var environment = StreamExecutionEnvironment.getExecutionEnvironment();
        Configuration config = new Configuration();
        config.set(ExecutionOptions.RUNTIME_MODE, RuntimeExecutionMode.STREAMING);
        environment.configure(config, WordCountDataStream.class.getClassLoader());
        environment.enableCheckpointing(Duration.ofSeconds(10).getSeconds(), CheckpointingMode.EXACTLY_ONCE);


        final var source = FileSource.forRecordStreamFormat(
                new TextLineInputFormat(),
                new Path(FILE_PATH)
        ).build();

        var datasource = environment.fromSource(source, WatermarkStrategy.noWatermarks(), "file-source");

        var sumOperator = datasource.flatMap(
                new FlatMapFunction<String, Tuple2<String, Integer>>() {
                    @Override
                    public void flatMap(String word, Collector<Tuple2<String, Integer>> out) throws Exception {

                        var words = word.split(" ");
                        log.info("input [{}]", words);
                        for (String countW : words) {
                            out.collect(Tuple2.of(countW, 1));
                        }
                    }
                }
        ).keyBy((KeySelector<Tuple2<String, Integer>, String>) value -> value.f0).sum(1);


        sumOperator.print("print-sinker");

//        在 DataStream API 中必须调用 execute 方法 !!!
        JobExecutionResult result = environment.execute();
        log.info("job execute result: {}", result);

    }


}
