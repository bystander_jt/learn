package cn.lee.flink;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.shaded.netty4.io.netty.util.internal.logging.InternalLogger;
import org.apache.flink.shaded.netty4.io.netty.util.internal.logging.InternalLoggerFactory;
import org.apache.flink.util.Collector;


public class WordCountDataSet {

    private static final InternalLogger log = InternalLoggerFactory.getInstance(WordCountDataSet.class);

    public static void main(String[] args) throws Exception {


        var environment = ExecutionEnvironment.getExecutionEnvironment();
        var datasource = environment.readTextFile("/Users/bystander/Documents/developer/Git_Library/learn/middleware/stream-process/flink/src/main/resources/stream-data/word-count.txt");

        datasource.flatMap(
                new FlatMapFunction<String, Tuple2<String, Integer>>() {
                    @Override
                    public void flatMap(String word, Collector<Tuple2<String, Integer>> out) throws Exception {
                        var words = word.split(" ");
                        log.info("input [{}]", words);
                        for (String countW : words) {
                            out.collect(Tuple2.of(countW, 1));
                        }
                    }
                }
        ).groupBy(0).sum(1).print();

    }


}
