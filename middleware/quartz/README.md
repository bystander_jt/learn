# Quartz

## 介紹

`Quartz` 是一个开源且特征丰富的任务调度库， 能够集成于任何的 `java` 应用程序。

## 核心

`Quartz` 的核心分为 3 个, 分别是 `Job` `Trigger` `Scheduler`

### `Job` 任务

`Job` 描述了一个需要调度的任务, 每个需要调度的任务都需要实现 `org.quartz.Job` 这个接口.

```java
public class JobDemo implements org.quartz.Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        // TODO 做任务
        System.out.println("--- Job 执行 ---");
    }
}
```

### `Trigger` 触发器

触发器用于在满足条件的情况下执行或者调度上方定义的 `Job`。

```java
public class TriggerDemo {

    public static void main(String[] args) {
        TriggerBuilder.newTrigger()
                // 执行什么任务， 填入 Job 对应的 JobDetail 或者对应 JobKey.
                .forJob()
                // 如何调度, SimpleScheduler 用于简单循环调度， CronScheduler 用于复杂的 Cron 调度
                .withSchedule()
                // 用于标识当前 trigger 的过期时间， 超过该时间点就不会根据 scheduler 执行
                .endAt()
                // 标识开始时间， 只有在这个时间后才会执行任务.
                .startAt()
                // 定义当前 trigger 所在的组和当前key
                .withIdentity("triggerKey", "defaultTriggerGroup")
                .build();
    }
}
```

`Scheduler` 分为

- `SimpleScheduler` 简易循环调用
    - 简易的触发器，用于设置循环执行等简易的触发器。 其中就有
    - `repeatMinutelyForever` - 每分钟执行一次，直到永远
    - `repeatSecondlyForever` - 每秒执行一次， 直到永远
- `CronScheduler`
    - 使用 `Cron` 表达式来执行特定时间任务. 具体如何执行就看 `cron` 表达式怎么写了.
    - 例如在 1:00AM 执行数据备份，或者数据计算等任务。 这种就适用于 `CronScheduler`
