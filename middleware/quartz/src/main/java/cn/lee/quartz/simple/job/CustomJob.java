package cn.lee.quartz.simple.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

public class CustomJob implements Job {

    /**
     * @param jobExecutionContext job 上下文
     */

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        System.out.println("--- Job 执行 ---");
    }


}
