package cn.lee.quartz.simple;


import cn.lee.quartz.simple.job.CustomJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws SchedulerException {
//        SpringApplication.run(Application.class).start();

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        JobDetail jobDetail = JobBuilder.newJob(CustomJob.class)
                .withIdentity(JobKey.jobKey("CustomKey", "defaultJobGroup"))
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("CustomKey", "defaultTriggerGroup")
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever())
                .build();

        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();

    }

}
