package cn.lee.queue.rocketmq.services;

import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class RocketProducerService {

    @Autowired
    private ApplicationEventPublisher eventMulticaster;

    private static final Logger log = LoggerFactory.getLogger(RocketProducerService.class);

    @Resource
    private RocketMQTemplate mqTemplate;

    @Value("${rocketmq.producer.destination:message}")
    private String destination;

    public void produce() {
        Message<String> payload = MessageBuilder.withPayload("simple syncSend Message").build();

        SendResult sendResult = mqTemplate.syncSend(destination, payload);
        printSendResult("syncSend", sendResult);
    }

    private void printSendResult(String sendType, SendResult result) {
        log.info("TX | type : {}, result : {}", sendType, result);
    }

}
