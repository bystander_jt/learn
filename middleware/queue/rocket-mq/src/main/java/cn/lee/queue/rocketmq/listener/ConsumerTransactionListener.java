package cn.lee.queue.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

@RocketMQTransactionListener(corePoolSize = 8, maximumPoolSize = 12, keepAliveTime = 10)
public class ConsumerTransactionListener implements RocketMQLocalTransactionListener {

    private final static Logger log = LoggerFactory.getLogger(ConsumerTransactionListener.class);


    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message msg, Object arg) {
        Object payload = msg.getPayload();
        if (arg != null){
            log.warn("arg : {}", arg);
        }
        log.info("consumer message : {}", payload);
        return RocketMQLocalTransactionState.COMMIT;
    }

    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message msg) {
        return null;
    }
}
