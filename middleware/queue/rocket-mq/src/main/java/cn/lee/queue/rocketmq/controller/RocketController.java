package cn.lee.queue.rocketmq.controller;

import cn.lee.queue.rocketmq.services.RocketProducerService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.lee.util.Result;

@RestController
public class RocketController {

    @Autowired
    private RocketProducerService producerService;

    @GetMapping("produce")
    public Result produce(){
        producerService.produce();
        return Result.ok();
    }

}
