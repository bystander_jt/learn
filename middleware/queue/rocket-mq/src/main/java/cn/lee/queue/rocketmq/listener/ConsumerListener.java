package cn.lee.queue.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RocketMQMessageListener(
        accessKey = "${rocketmq.consumer.ak}",
        enableMsgTrace = true,
        topic = "${rocketmq.consumer.destination}",
        consumerGroup = "${rocketmq.consumer.group}"
)
public class ConsumerListener implements RocketMQListener {

    private static final Logger log = LoggerFactory.getLogger(ConsumerListener.class);

    @Override
    public void onMessage(Object message) {
        log.info("RX | : {}", message);
    }
}
