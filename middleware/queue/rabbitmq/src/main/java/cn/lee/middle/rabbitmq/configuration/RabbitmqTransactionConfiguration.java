package cn.lee.middle.rabbitmq.configuration;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.transaction.RabbitTransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqTransactionConfiguration {


    @Bean
    public RabbitTransactionManager txManager(ConnectionFactory connection) {
        RabbitTransactionManager manager = new RabbitTransactionManager(connection);
        return manager;
    }


}
