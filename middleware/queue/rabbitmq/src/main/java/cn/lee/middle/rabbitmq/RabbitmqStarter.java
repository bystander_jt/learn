package cn.lee.middle.rabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableRabbit
@SpringBootApplication
public class RabbitmqStarter {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RabbitmqStarter.class);
        application.setWebApplicationType(WebApplicationType.SERVLET);
        application.run(args).start();
    }

}
