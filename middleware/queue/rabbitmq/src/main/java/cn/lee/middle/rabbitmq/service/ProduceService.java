package cn.lee.middle.rabbitmq.service;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static cn.lee.middle.rabbitmq.configuration.NormalChannelConfiguration.DEFAULT_DIRECT_EXCHANGE;
import static cn.lee.middle.rabbitmq.configuration.NormalChannelConfiguration.DEFAULT_ROUTING_KEY;
import static cn.lee.middle.rabbitmq.configuration.RabbitmqChannelConfiguration.DEFAULT_CHANNEL_EXCHANGE;
import static cn.lee.middle.rabbitmq.configuration.RabbitmqChannelConfiguration.DEFAULT_CHANNEL_ROUTING_KEY;

@Slf4j
@Service
public class ProduceService {

    private final RabbitTemplate template;
    private final Channel channel;


    public static final MessagePostProcessor processor = message -> {
        MessageProperties properties = message.getMessageProperties();
        properties.setHeader("retry-time", 0);
        return message;
    };

    public ProduceService(Channel channel, RabbitTemplate template) {
        this.channel = channel;
        this.template = template;
        // 设置 template 开始事务控制
        this.template.setChannelTransacted(true);
    }

    public void templateProduce(String message) {
        template.convertAndSend(DEFAULT_DIRECT_EXCHANGE, DEFAULT_ROUTING_KEY, message, processor);
    }

    public void channelProduce(String message) {
        try {
            channel.txSelect();
            template.convertAndSend(DEFAULT_CHANNEL_EXCHANGE, DEFAULT_CHANNEL_ROUTING_KEY, message, processor);
            channel.txCommit();
        } catch (IOException e) {
            log.error("tx failure to open");
            try {
                channel.txRollback();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
