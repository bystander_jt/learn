package cn.lee.middle.rabbitmq.configuration;


import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@SuppressWarnings("unused")
@Configuration
public class NormalChannelConfiguration {

    public static final String DEFAULT_DIRECT_EXCHANGE = "default-direct-exchange";
    public static final String DEFAULT_DIRECT_QUEUE = "default-direct-exchange";
    public static final String DEFAULT_ROUTING_KEY = "default-routing-key";

    @Bean
    public Exchange normalExchange(){
        return ExchangeBuilder.directExchange(DEFAULT_DIRECT_EXCHANGE)
                .durable(true).build();
    }

    @Bean
    public Queue normalQueue(){
        return QueueBuilder.durable(DEFAULT_DIRECT_QUEUE).lazy().deliveryLimit(1).build();
    }

    @Bean
    public Binding normalBinding(Exchange normalExchange, Queue normalQueue){
        return BindingBuilder.bind(normalQueue).to(normalExchange).with(DEFAULT_ROUTING_KEY).noargs();
    }


    //    @PostConstruct
    public void defaultExchange(Channel channel) throws IOException {
        channel.exchangeDeclare("default-direct-exchange", BuiltinExchangeType.DIRECT);
        channel.exchangeDeclare("default-topic-exchange", BuiltinExchangeType.TOPIC);
    }


    @Bean
    public DeliverCallback defaultCallBack() {
        return ((consumerTag, delivery) -> {
            String msg = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Received '" + msg + "'");
        });
    }


}
