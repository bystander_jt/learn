package cn.lee.middle.rabbitmq.controller;

import cn.lee.middle.rabbitmq.service.ProduceService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rabbitmq")
public class RabbitmqController {

    private final ProduceService produce;

    public RabbitmqController(ProduceService produce) {
        this.produce = produce;
    }

    @GetMapping("/send")
    public void send(@RequestParam String msg) {
        produce.templateProduce(msg);
    }

    @GetMapping("/send_tx")
    public void sendTx(@RequestParam String msg) {
        produce.channelProduce(msg);
    }
}
