package cn.lee.middle.rabbitmq.listener;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static cn.lee.middle.rabbitmq.configuration.RabbitmqChannelConfiguration.*;

@Component
public class AfterApplicationRunner implements ApplicationRunner {

    @Autowired
    private Channel channel;

    @Override
    public void run(ApplicationArguments args) {
        try {
            channel.exchangeDeclare(DEFAULT_CHANNEL_EXCHANGE, BuiltinExchangeType.DIRECT, true);
            channel.queueDeclare(DEFAULT_CHANNEL_QUEUE, true, true, false, null);
            channel.queueBind(DEFAULT_CHANNEL_QUEUE, DEFAULT_CHANNEL_EXCHANGE, DEFAULT_CHANNEL_ROUTING_KEY);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
