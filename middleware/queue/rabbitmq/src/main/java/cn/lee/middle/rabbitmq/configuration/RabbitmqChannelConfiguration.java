package cn.lee.middle.rabbitmq.configuration;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Configuration
public class RabbitmqChannelConfiguration {

    public static final String DEFAULT_CHANNEL_EXCHANGE = "default-channel-exchange";
    public static final String DEFAULT_CHANNEL_QUEUE = "default-channel-queue";
    public static final String DEFAULT_CHANNEL_ROUTING_KEY = "default-channel-routing-key";

    @Value("${spring.rabbitmq.host:localhost}")
    private String host;

    @Value("${spring.rabbitmq.port:5672}")
    private Integer port;

    @Value("${spring.rabbitmq.username:guest}")
    private String username;

    @Value("${spring.rabbitmq.password:pass}")
    private String password;

    @Value("${spring.rabbitmq.virtual-host:/}")
    private String vhost;




    @Bean
    public Connection connection() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(username);
        factory.setPassword(password);
        factory.setHost(host);
        factory.setPort(port);
        factory.setVirtualHost(vhost);
        try {
            return factory.newConnection();
        } catch (IOException | TimeoutException ignored) {

        }
        return null;
    }

    @Bean
    public Channel channel(Connection connection) throws IOException {
        return connection.createChannel(1);
    }


}
