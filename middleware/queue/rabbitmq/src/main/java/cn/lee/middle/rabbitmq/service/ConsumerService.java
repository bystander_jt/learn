package cn.lee.middle.rabbitmq.service;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static cn.lee.middle.rabbitmq.configuration.NormalChannelConfiguration.DEFAULT_DIRECT_QUEUE;

@Component
@Slf4j(topic = "rabbit-consumer")
public class ConsumerService {

    private static final Gson gson = new Gson();

    @RabbitListener(
            ackMode = "MANUAL",
            queues = {
                    DEFAULT_DIRECT_QUEUE,
//                    DEFAULT_CHANNEL_QUEUE
            }
//            queuesToDeclare = {
//                    @Queue(name = DEFAULT_DIRECT_QUEUE, durable = "true", autoDelete = "true"),
//                    @Queue(name = DEFAULT_CHANNEL_QUEUE, durable = "true", autoDelete = "true")
//            },
//            bindings = {
//                    @QueueBinding(
//                            value = @Queue(name = DEFAULT_DIRECT_QUEUE),
//                            exchange = @Exchange(name = DEFAULT_DIRECT_EXCHANGE)
//                    ),
//                    @QueueBinding(
//                            value = @Queue(name = DEFAULT_CHANNEL_QUEUE),
//                            exchange = @Exchange(name = DEFAULT_CHANNEL_EXCHANGE)
//                    )
//            }
    )
    public void consumer(Channel ack, Message message, MessageHeaders headers) {
        long deliveryTag = message.getMessageProperties().getDeliveryTag();

        Integer retryTime = message.getMessageProperties().<Integer>getHeader("retry-time");
        try {
            log.info("==================== RECEIVE MESSAGE ====================");
            String messageBody = new String(message.getBody());
            log.info("consumer message : {}, retryTime: {}", messageBody, retryTime);
            log.info("");

            String headerJson = gson.toJson(headers);
            log.info("consumer header : {} ", headerJson);
            log.info("");

            ack.basicAck(deliveryTag, false);
            log.info("consumer ack");
            log.info("==================== MESSAGE PROCESSED ====================");
        } catch (Exception e) {
            try {
                if (retryTime < 3) {
                    ack.basicNack(deliveryTag, false, true);
                } else {
                    // TODO 丢入死信队列
                    ack.basicAck(deliveryTag, false);
                }
            } catch (IOException ex) {
                log.error("mq nack failure");
            }
        }
    }
}
