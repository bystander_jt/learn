package cn.lee.middle.rabbitmq.service;

import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static cn.lee.middle.rabbitmq.configuration.NormalChannelConfiguration.DEFAULT_DIRECT_EXCHANGE;
import static cn.lee.middle.rabbitmq.configuration.NormalChannelConfiguration.DEFAULT_ROUTING_KEY;

@Service
public class TxProduceService {


    private final RabbitTemplate template;

    public TxProduceService(RabbitTemplate template) {
        this.template = template;
    }

    public static final MessagePostProcessor processor = message -> {
        MessageProperties properties = message.getMessageProperties();
        properties.setHeader("retry-time", 0);
        return message;
    };


    @Transactional
    public void produce(){
        template.convertAndSend(DEFAULT_DIRECT_EXCHANGE, DEFAULT_ROUTING_KEY, new Object(), processor);

    }


}
