package cn.lee.middleware.log.configuration;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.Configurator;
import ch.qos.logback.core.spi.ContextAwareBase;

public class LogbackCustomConfiguration extends ContextAwareBase implements Configurator {


    @Override
    public void configure(LoggerContext loggerContext) {
        // TODO customize logback application context
    }
}
d