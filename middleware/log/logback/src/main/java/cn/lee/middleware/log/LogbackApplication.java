package cn.lee.middleware.log;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.util.UUID;


@Slf4j
public class LogbackApplication {

    private static final String MDC_TRACE_ID_KEY = "traceId";

    public static void main(String[] args) {
        MDC.put(MDC_TRACE_ID_KEY, UUID.randomUUID().toString());

        log.info("template log.");

        MDC.remove(MDC_TRACE_ID_KEY);

        log.info("template log.");


    }

}
