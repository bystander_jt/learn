package cn.lee.middleware.sentinel.distribution.service.impl;

import org.apache.dubbo.config.annotation.DubboService;
import service.api.IDistributionService;

/**
 * @since VERSION
 */

@DubboService
public class DistributionServiceImpl implements IDistributionService {


    private static String pid;

    static {
        pid = System.getProperty("PID");
    }

    @Override
    public String server() {
        return pid;
    }


}
