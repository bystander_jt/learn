package cn.lee.middleware.sentinel.distribution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Bystander
 */

@SpringBootApplication
public class DistributionServiceApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(DistributionServiceApplication.class);
        springApplication.setWebApplicationType(WebApplicationType.NONE);
        springApplication.run(args);
    }

}
