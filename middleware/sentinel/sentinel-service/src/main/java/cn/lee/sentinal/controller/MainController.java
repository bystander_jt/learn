package cn.lee.sentinal.controller;

import cn.lee.util.Result;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import service.api.IDistributionService;

/**
 * @since VERSION
 */

@Slf4j
@RestController
@RequestMapping("/")
@SentinelResource(value = "provider")
public class MainController {

    @DubboReference
    private IDistributionService distributionService;

    @GetMapping("get")
    public String get(@RequestParam String name) {
        return name;
    }


    @GetMapping("provider")
    @SentinelResource(value = "provider", blockHandler = "exceptionHandler")
    public Result provider() {
        return Result.ok(distributionService.server());
    }

    // Block 异常处理函数，参数最后多一个 BlockException，其余与原函数一致.
    public Result exceptionHandler(BlockException ex) {
        log.info(Thread.currentThread().getName() + "\t" + "...exceptionHandler");
        return Result.fail(403, null, "limited request");
    }

}
