package cn.lee.redission.configuration;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.TransportMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class RedisConfiguration {


    @Bean
    public RedissonClient client() {
        Config config = new Config();
//        config.setTransportMode(TransportMode.EPOLL)
//                .setCodec()
//                .setAddressResolverGroupFactory()
//                .setCheckLockSyncedSlaves()
//                .setConnectionListener()
//                .setCleanUpKeysAmount()
//                .setEventLoopGroup()
//                .setExecutor()
//                .setKeepPubSubOrder()
//                .setLockWatchdogTimeout()
//                .setMaxCleanUpDelay()
//                .setMinCleanUpDelay()
//                .setNettyHook()
//                .setNettyThreads()
//                .setReliableTopicWatchdogTimeout()
//                .setThreads()
//                .setUseScriptCache()
//                .setUseThreadClassLoader()
//                .setReferenceEnabled();
//
        config.useSingleServer()
//                .setAddress()
//                .setUsername()
//                .setPassword()
//                .setDatabase()
//
//                .setConnectionMinimumIdleSize()
//                .setConnectionPoolSize()
//
//                .setDnsMonitoringInterval()
//
//                .setSubscriptionConnectionPoolSize()
//                .setSubscriptionsPerConnection()
//                .setSubscriptionConnectionMinimumIdleSize()
//
//                .setClientName()
//
//                .setTimeout()
//                .setKeepAlive()
//                .setTransportMode()
//                .setTcpNoDelay()
//                .setConnectTimeout()
//                .setIdleConnectionTimeout()
//                .setPingConnectionInterval()
//
//                .setRetryAttempts()
//                .setRetryInterval()
//
//                .setSslEnableEndpointIdentification()
//                .setSslKeystore()
//                .setSslKeystorePassword()
//                .setSslProtocols()
//                .setSslProvider()
//                .setSslTruststore()
//                .setSslTruststorePassword()
//                .setNameMapper()
                .setAddress("redis://leejt.top:6379")
                .setPassword("leejt.top")
//                .setDatabase(0)
//                .setPingConnectionInterval(10)
//                .setConnectTimeout(10)
//                .setKeepAlive(true)
//                .setIdleConnectionTimeout((int) Duration.ofDays(1).getSeconds())
        ;
        return Redisson.create(config);
    }

}
