package cn.lee.redisson;

import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.concurrent.TimeUnit;

public class RedissonLockTest {


    @Test
    public void test() throws InterruptedException {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://10.0.0.10:6379");
        RedissonClient client = Redisson.create(config);

        RLock a = client.getLock("A");
        a.lock();

        TimeUnit.MINUTES.sleep(10);
    }

    @Test
    public void test2() throws InterruptedException {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://10.0.0.10:6379");
        RedissonClient client = Redisson.create(config);

        RLock a = client.getLock("b");
        Thread thread = new Thread(() -> {
            a.lock();
            try {
                TimeUnit.MINUTES.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            a.unlock();
            a.forceUnlock();

        });

        thread.start();

        TimeUnit.SECONDS.sleep(30);
        thread.interrupt();

        TimeUnit.MINUTES.sleep(1);

    }
}
