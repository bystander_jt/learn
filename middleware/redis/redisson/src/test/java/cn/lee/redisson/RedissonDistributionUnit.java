package cn.lee.redisson;

import cn.lee.redission.RedissonApplication;
import org.junit.jupiter.api.Test;
import org.redisson.api.RFuture;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = RedissonApplication.class)
public class RedissonDistributionUnit {

    private static final Logger log = LoggerFactory.getLogger(RedissonDistributionUnit.class);
    private static final TimeUnit timeUnit = TimeUnit.MILLISECONDS;

    @Autowired
    private RedissonClient client;

    @Test
    public void redissonDistributionLockTest() {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 4; i++) {
            executor.execute(() -> {
                while (true) {
                    try {
                        distributionLock();
                    } catch (InterruptedException e) {
                        break;
                    }
                }
                log.error("thread {} quit", Thread.currentThread().getName());
            });
        }
        while (!executor.isShutdown()) {

        }
    }

    private void distributionLock() throws InterruptedException {
        RLock lock = client.getLock("distribution");
        boolean resources = false;
        try {

            //resources = lock.tryLock(10, TimeUnit.MILLISECONDS);
            resources = lock.tryLock(100, 10, timeUnit);

            if (resources) {
                log.info("thread : {} got resources", Thread.currentThread().getName());

                RLock reentryLock = client.getLock("distribution");
                reentryLock.lock();
                log.info("thread : {} reentry got resources", Thread.currentThread().getName());
            } else
                log.error("thread : {} didn't got resources", Thread.currentThread().getName());
        } catch (InterruptedException e) {
            log.error("thread : {} has interrupted exception", Thread.currentThread().getName());
        } finally {
            if (resources) {
                lock.unlock();
                RFuture<Void> voidRFuture = lock.unlockAsync(10);
                log.info("thread : {} release resources", Thread.currentThread().getName());
            }
        }
    }

}
