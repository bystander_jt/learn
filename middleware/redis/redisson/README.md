# Redisson
分布式组件

> Redisson是一个在Redis的基础上实现的Java驻内存数据网格（In-Memory Data Grid）。 它不仅提供了一系列的分布式的Java常用对象，还提供了许多分布式服务。其中包括(BitSet, Set, Multimap, SortedSet, Map, List, Queue, BlockingQueue, Deque, BlockingDeque, Semaphore, Lock, AtomicLong, CountDownLatch, Publish / Subscribe, Bloom filter, Remote service, Spring cache, Executor service, Live Object service, Scheduler service) Redisson提供了使用Redis的最简单和最便捷的方法。Redisson的宗旨是促进使用者对Redis的关注分离（Separation of Concern），从而让使用者能够将精力更集中地放在处理业务逻辑上。

[项目地址](https://github.com/redisson/redisson)

[项目文档](https://github.com/redisson/redisson/wiki/%E7%9B%AE%E5%BD%95)
### 添加进项目
#### Maven
```xml
<dependency>
    <groupId>org.redisson</groupId>
    <artifactId>redisson</artifactId>
    <version>3.16.3</version>
</dependency>  
```

#### Gradle
```text
compile 'org.redisson:redisson:3.16.3'
```  

#### SBT
```text
libraryDependencies += "org.redisson" % "redisson" % "3.16.3"
```