package cn.lee.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cache.CacheProperties;


@SpringBootApplication
public class RedisTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisTemplateApplication.class, args);
    }

}
