package cn.lee.redis;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@SpringBootTest(classes = RedisTemplateApplication.class)
public class RedisTestUnit {

    private final static Logger log = LoggerFactory.getLogger(RedisTestUnit.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Test
    public void redisDistributionKey() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        String key = "redis:distribution";
        for (int i = 0; i < 4; i++) {
            executorService.execute(() -> setRedisKey(key, "aaaa"));
        }
        Thread.sleep(10000);
        executorService.shutdown();
    }

    /**
     * 设置分布式 key， 设置成功返回 true, 失败返回 false.
     * @param key
     * @param value
     * @return
     */
    private boolean setRedisKey(String key, String value) {
        Boolean result = redisTemplate.opsForValue().setIfAbsent(key, value, Duration.ofMinutes(1));
        log.info("thread : {}, setDistributionKey : {}", Thread.currentThread().getId(), result);
        return result;
    }

}
