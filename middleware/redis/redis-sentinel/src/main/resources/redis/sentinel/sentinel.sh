#! /bin/bash



function D_RUN(){

	sudo docker run -d --name redis-sentinel-${DOCKER_NAME} \
		--network host \
		-v $PWD/:/etc/redis/conf \
		--restart on-failure \
		--privileged \
		redis redis-sentinel /etc/redis/conf/sentinel-${DOCKER_NAME}.conf

}

function D_REMOVE(){
	sudo docker rm -f redis-sentinel-${DOCKER_NAME}
}

function main(){
	DOCKER_NAME=master
	D_REMOVE
	D_RUN

	for i in $(seq 1 4);
	do
		DOCKER_NAME=slave-${i}
		sed -i -r "s/port ./port 2638${i}/g" sentinel-slave-${i}.conf
		D_REMOVE
		D_RUN
	done
}


main


