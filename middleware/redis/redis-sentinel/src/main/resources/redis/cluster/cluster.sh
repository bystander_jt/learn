#!/bin/bash

cluster_master(){

	for CLUSTER_OFFSET in $(seq 1 6)
	do

		sudo docker run -d \
			--restart on-failure \
			--privileged \
			--name redis-cluster-${CLUSTER_OFFSET} \
			-v $PWD:/etc/redis/conf \
			--network host \
			redis redis-server /etc/redis/conf/cluster-${CLUSTER_OFFSET}.conf
		#cluster_node

	done

}


cluster_node(){

	for NODE_OFFSET in $(seq 1 3)
	do
		sudo docker run -d \
			--restart on-failure \
			--privileged \
			--name redis-cluster-${CLUSTER_OFFSET}-node-${NODE_OFFSET} \
			-v $PWD:/etc/redis/conf \
			--network host \
			redis redis-server /etc/redis/conf/cluster-${CLUSTER_OFFSET}/cluster-${CLUSTER_OFFSET}-${NODE_OFFSET}.conf
	done
}


main(){

	sudo docker rm -f $(sudo docker ps -aq --filter ancestor=redis)
	cluster_master
}

main
