#! /bin/bash

function D_RUN(){

	sudo docker run -d --name redis-"${DOCKER_NAME}" \
		--network host \
		-v "$PWD"/:/etc/redis/conf \
		--restart on-failure \
		--privileged \
		redis redis-server /etc/redis/conf/redis-"${DOCKER_NAME}".conf

}

function D_REMOVE(){
	sudo docker rm -f redis-"${DOCKER_NAME}"
}

function main(){
	DOCKER_NAME=master
	D_REMOVE
	D_RUN

	for i in $(seq 1 4);
	do
		DOCKER_NAME=slave-${i}
		sed -i -r "s/port ./port 2638${i}/g" sentinel-slave-"${i}".conf
		D_REMOVE
		D_RUN
	done
}


main



