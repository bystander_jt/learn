package cn.lee.redis.sentinel.service;


import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 
 * 
 * @since VERSION
 */

@Service
public class RedisService {

    private final StringRedisTemplate stringRedisTemplate;
    private final Thread thread;
    private final AtomicBoolean threadRunning;

    public RedisService(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
        thread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                ThreadLocalRandom random =  ThreadLocalRandom.current();
                putCache(random.nextInt(100000));
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    return;
                }
            }
        });
        threadRunning = new AtomicBoolean(false);
    }

    public void startPut() {
        if (threadRunning.get()) return;
        synchronized (thread) {
            thread.isInterrupted();
            thread.start();
        }
    }

    public void stopPut() {
        if (threadRunning.get()) return;
        synchronized (thread) {
            thread.interrupt();
        }
    }


    public void putCache(Object cache) {
        stringRedisTemplate.opsForValue().set("TMP:CACHE:" + (Math.abs((Integer) cache)), String.valueOf(cache));
    }

}
