package cn.lee.redis.sentinel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * 
 * @since VERSION
 */

@SpringBootApplication
public class RedisSentinelApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RedisSentinelApplication.class);
        application.setWebApplicationType(WebApplicationType.SERVLET);
        application.run(args);
    }

}
