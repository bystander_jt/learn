package cn.lee.redis.sentinel.controller;

import cn.lee.redis.sentinel.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * 
 * @since VERSION
 */

@RestController
@RequestMapping("/redis")
public class RedisController {


    @Autowired
    private RedisService redisService;

    @PostMapping("/start_put")
    public void startPut(){
        redisService.startPut();
    }

    @PostMapping("/stop_put")
    public void stopPut(){
        redisService.stopPut();
    }

}
