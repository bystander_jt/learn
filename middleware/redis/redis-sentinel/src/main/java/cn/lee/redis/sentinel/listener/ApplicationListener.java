package cn.lee.redis.sentinel.listener;

import cn.lee.redis.sentinel.service.RedisService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class ApplicationListener implements ApplicationRunner {

    private StringRedisTemplate template;
    private RedisService service;

    public ApplicationListener(StringRedisTemplate template, RedisService service) {
        this.template = template;
        this.service = service;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        template.opsForValue().set("hello", "world");
        service.startPut();
    }
}
