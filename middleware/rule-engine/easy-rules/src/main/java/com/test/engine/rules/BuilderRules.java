package com.test.engine.rules;

import org.jeasy.rules.api.Rule;
import org.jeasy.rules.core.RuleBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @since VERSION
 */

public class BuilderRules {

    private static final Logger log = LoggerFactory.getLogger(BuilderRules.class);

    public static Rule buildRule() {
        return new RuleBuilder()
                .name("myRule")
                .description("myRuleDescription")
                .priority(3)
                .when(facts -> {
                    log.info("build rule condition");
                    return true;
                })
                .then(facts -> log.info("build rule action"))
                .build();
    }
}
