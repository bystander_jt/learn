package com.test.engine.listener;

import lombok.extern.slf4j.Slf4j;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;

/**
 * 
 * 
 * @since VERSION
 */

@Slf4j
public class RuleListener implements org.jeasy.rules.api.RuleListener {

    @Override
    public boolean beforeEvaluate(Rule rule, Facts facts) {
        // 触发前操作, 如果返回 false 代表跳过
        log.debug("before rule evaluate");
        return true;
    }

    @Override
    public void afterEvaluate(Rule rule, Facts facts, boolean evaluationResult) {
        log.debug("after evaluate");
    }

    @Override
    public void onEvaluationError(Rule rule, Facts facts, Exception exception) {

    }

    @Override
    public void beforeExecute(Rule rule, Facts facts) {

    }

    @Override
    public void onSuccess(Rule rule, Facts facts) {

    }

    @Override
    public void onFailure(Rule rule, Facts facts, Exception exception) {

    }
}
