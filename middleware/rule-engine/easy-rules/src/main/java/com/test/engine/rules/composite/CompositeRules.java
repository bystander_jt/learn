package com.test.engine.rules.composite;

import lombok.extern.slf4j.Slf4j;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Rule;
import org.jeasy.rules.support.composite.UnitRuleGroup;

/**
 * 
 * 
 * @since VERSION
 */

@Slf4j
public class CompositeRules {

    public static void evaluation(UnitRuleGroup group) {
        boolean evaluate = group.evaluate(null);
        if (evaluate) {
            try {
                group.execute(null);
            } catch (Exception ignored) {

            }
        }

    }

    public UnitRuleGroup getUnit() {
        UnitRuleGroup group = new UnitRuleGroup("ruleGroup", "rule group");
        group.setPriority(0);
        group.addRule(new Rule1());
        group.addRule(new Rule2());

        return group;
    }

    @Rule(name = "rule 1", priority = 1)
    public class Rule1 {

        @Condition
        public boolean condition() {
            return true;
        }

        @Action
        public void action() {
            log.info("rule 1 action");
        }

    }

    @Rule(name = "rule 2", priority = 2)
    public class Rule2 {

        @Condition
        public boolean condition() {
            return true;
        }

        @Action
        public void action() {
            log.info("rule 2 action");
        }


    }

}
