package com.test.engine.listener;

import lombok.extern.slf4j.Slf4j;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngineListener;

/**
 * 
 * 
 * @since VERSION
 */

@Slf4j
public class CustomEngineListener implements RulesEngineListener {

    @Override
    public void beforeEvaluate(Rules rules, Facts facts) {
        log.info("before evaluate rules");
    }

    @Override
    public void afterExecute(Rules rules, Facts facts) {
        log.info("after execute rules");
    }
}
