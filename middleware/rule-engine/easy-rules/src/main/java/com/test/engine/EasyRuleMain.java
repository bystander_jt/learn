package com.test.engine;

import com.test.engine.listener.CustomEngineListener;
import com.test.engine.listener.RuleListener;
import com.test.engine.rules.AnnotationRuleBuild;
import com.test.engine.rules.BuilderRules;
import com.test.engine.rules.composite.CompositeRules;
import org.jeasy.rules.api.Fact;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.InferenceRulesEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @since VERSION
 */

public class EasyRuleMain {
    private static final Logger logger = LoggerFactory.getLogger(EasyRuleMain.class);
    private static final DefaultRulesEngine engine = new DefaultRulesEngine();

    private static final RulesEngine rulesEngine = new InferenceRulesEngine();


    public static void main(String[] args) {
        Facts facts = new Facts();
        Rules rules = new Rules();

        engine.registerRuleListener(new RuleListener());
        engine.registerRulesEngineListener(new CustomEngineListener());

//        annotationRules(facts, rules);
//        buildRules(facts, rules);
        unitGroupRules(facts, rules);

        engine.fire(rules, facts);

    }

    public static void annotationRules(Facts facts,
                                       Rules rules) {
        facts.add(new Fact<>("Require", "Require"));
        facts.add(new Fact<>("Require2", "Require2"));

        rules.register(new AnnotationRuleBuild());

    }


    public static void buildRules(Facts facts,
                                  Rules rules) {
        rules.register(new BuilderRules().buildRule());
    }

    public static void unitGroupRules(Facts facts,
                                      Rules rules) {
        CompositeRules compositeRules = new CompositeRules();
        rules.register(compositeRules.getUnit());
    }
}
