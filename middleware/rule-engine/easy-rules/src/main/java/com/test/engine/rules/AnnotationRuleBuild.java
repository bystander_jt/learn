package com.test.engine.rules;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jeasy.rules.api.Facts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @since VERSION
 */

@Rule(name = "test-rules", priority = 1)
public class AnnotationRuleBuild {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Condition
    public boolean condition(@Fact("Require") String factName,
                             @Fact("Require2") String factName2) {
        log.info("rule engine condition operation, current fact : {} " , factName);
        return true;
    }

    @Action(order = 1)
    public void then(Facts facts) throws Exception {
        log.info("rule engine then operation");
    }

    @Action(order = 2)
    public void afterAction() throws Exception {
        log.info("rule engine afterAction operation");
    }

}
