package com.test.drools.pojo.dto.chapter.chapter_one;

import com.test.drools.pojo.dto.chapter.Application;
import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.junit.Test;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;

import java.util.Map;

/**
 * 
 * 
 * @since VERSION
 */

public class RuleRunner {

    @Test
    public void chapterOneTest() {
        KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        builder.add(ResourceFactory.newClassPathResource("chapter.test/rules/linceseApplication.drl"), ResourceType.DRL);

        if (builder.hasErrors()){
            System.out.println(builder.getErrors());
        }


        InternalKnowledgeBase base = KnowledgeBaseFactory.newKnowledgeBase();
        base.addPackages(builder.getKnowledgePackages());

        Map<String, Class<?>> globals = base.getGlobals();

        KieSession kieSession = base.newKieSession();
        Application application = new Application();
        application.setAge(10);
        application.setName("aaa");

        kieSession.insert(application);

    }

}
