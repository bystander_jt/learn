package com.test.drools;

import com.test.drools.service.DroolsService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
class DroolsApplicationTests {
    @Autowired
    private DroolsService droolsService;

    @Test
    void contextLoads() {
        droolsService.service();

    }

}
