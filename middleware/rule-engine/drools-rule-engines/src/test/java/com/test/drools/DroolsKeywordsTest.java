package com.test.drools;

import com.test.drools.pojo.dto.rules.Person;
import lombok.extern.slf4j.Slf4j;
import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.junit.Test;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.EntryPoint;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * 
 * @since VERSION
 */
@Slf4j
public class DroolsKeywordsTest {


    @Test
    public void forallTest() {

        InternalKnowledgeBase kbase = readClassPath("chapter/rules/forall.drl");

        KieSession session = kbase.newKieSession();

        Random random = new Random(0);
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            person.setAge(random.nextInt(50));
            person.setName("aaa");
            session.insert(person);
        }

        session.fireAllRules();

        session.dispose();
        session.destroy();
    }

    @Test
    public void fromTest() {
        InternalKnowledgeBase kbase = readClassPath("chapter/rules/fromRule.drl");

        KieSession session = kbase.newKieSession();

        Random random = new Random(0);
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            person.setAge(random.nextInt(50));
            person.setName("aaa");
            session.insert(person);
            log.info("insert person parameter  age : {}", person.getAge());
        }

        session.fireAllRules();

        session.dispose();
        session.destroy();
    }

    @Test
    public void entryPointTest() {
        InternalKnowledgeBase kbase = readClassPath("chapter/rules/entryPointRule.drl");

        KieSession session = kbase.newKieSession();

        List<Person> personList = new ArrayList<>();
        Random random = new Random(0);
        EntryPoint point = session.getEntryPoint("point");
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            person.setAge(random.nextInt(50));
            person.setName("aaa");
            personList.add(person);
            point.insert(person);
            log.info("insert person parameter  age : {}", person.getAge());
        }


        session.fireAllRules();

        session.dispose();
        session.destroy();
    }

    @Test
    public void collectionTest() {
        InternalKnowledgeBase kbase = readClassPath("chapter/rules/collectRule.drl");

        KieSession session = kbase.newKieSession();

        List<Person> personList = new ArrayList<>();
        Random random = new Random(0);
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            person.setAge(random.nextInt(50));
            person.setName("aaa");
            session.insert(person);

            log.info("insert person parameter  age : {}", person.getAge());
        }

        session.fireAllRules();

        session.dispose();
        session.destroy();
    }

    @Test
    public void accumulateTest() {
        InternalKnowledgeBase kbase = readClassPath("chapter/rules/accumulateRule.drl");

        KieSession session = kbase.newKieSession();

        List<Person> personList = new ArrayList<>();
        Random random = new Random(0);
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            person.setAge(random.nextInt(50));
            person.setName("aaa");
            session.insert(person);

            log.info("insert person parameter  age : {}", person.getAge());
        }

        session.insert("aaaaaaa");

        // 直接运行所有规则
        session.fireAllRules();

        // 运行规则, 如果有调用 halt(), 则会立即中断
        session.fireUntilHalt();
        session.halt();

        session.dispose();
        session.destroy();
    }

    private InternalKnowledgeBase readClassPath(String drlFilePath) {
        KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        knowledgeBuilder.add(ResourceFactory.newClassPathResource(drlFilePath), ResourceType.DRL);
        if (knowledgeBuilder.hasErrors()) {
            for (KnowledgeBuilderError error : knowledgeBuilder.getErrors()) {
                log.error("knowledgeBuilder has error line: {} , message : {}", error.getLines(), error.getMessage());
            }
        }
        InternalKnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addPackages(knowledgeBuilder.getKnowledgePackages());
        return kbase;
    }

}
