package com.test.drools.interfaces.accumulate;

import org.drools.core.base.accumulators.AverageAccumulateFunction;
import org.kie.api.runtime.rule.AccumulateFunction;

import java.io.*;

/**
 * 
 * 
 * @since VERSION
 */

public class PersonAccumulateExtraFunction implements AccumulateFunction {
    @Override
    public Serializable createContext() {
        return null;
    }

    @Override
    public void init(Serializable serializable) throws Exception {

    }

    @Override
    public void accumulate(Serializable serializable, Object o) {

    }

    @Override
    public void reverse(Serializable serializable, Object o) throws Exception {

    }

    @Override
    public Object getResult(Serializable serializable) throws Exception {
        return null;
    }

    @Override
    public boolean supportsReverse() {
        return false;
    }

    @Override
    public Class<?> getResultType() {
        return null;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }

    public class ExtraFunction implements Externalizable {

        @Override
        public void writeExternal(ObjectOutput out) throws IOException {

        }

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

        }
    }
}
