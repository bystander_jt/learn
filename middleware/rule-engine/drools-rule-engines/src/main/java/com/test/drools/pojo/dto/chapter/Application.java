package com.test.drools.pojo.dto.chapter;

/**
 * 
 * 
 * @since VERSION
 */

public class Application {

    private String name;

    private int age;

    private boolean valid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
