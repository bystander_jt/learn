package com.test.drools.configuration;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.ScheduledExecutorService;

/**
 * 
 * 
 * @since VERSION
 */

@Configuration
public class DroolsInitialize {
    private final KieServices kieServices = KieServices.Factory.get();
    @Bean
    public KieContainer kieContainer(){

        return kieServices.getKieClasspathContainer();
    }



}
