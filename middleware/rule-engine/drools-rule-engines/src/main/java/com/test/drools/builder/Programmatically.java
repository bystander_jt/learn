package com.test.drools.builder;

import org.drools.core.event.DebugAgendaEventListener;
import org.drools.core.event.DebugRuleRuntimeEventListener;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.conf.*;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.internal.command.CommandFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * @since VERSION
 */

public class Programmatically {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.Factory.get();
        Programmatically programmatically = new Programmatically();
//        programmatically.builingManunnly(kieServices);
        programmatically.command(kieServices);
    }

    public void builingManunnly(KieServices service) {
        KieFileSystem fileSystem = service.newKieFileSystem();

        KieModuleModel module = service.newKieModuleModel();

        KieBaseModel baseModel = module.newKieBaseModel("base-model")
                .setDefault(false)
                .setEventProcessingMode(EventProcessingOption.CLOUD)
                .setEqualsBehavior(EqualityBehaviorOption.EQUALITY)
                .setMutability(KieBaseMutabilityOption.ALLOWED)
                .setDeclarativeAgenda(DeclarativeAgendaOption.DISABLED)
                .setSequential(SequentialOption.YES)
                .setSessionsPool(SessionsPoolOption.NO);


        KieSessionModel baseSession1 = baseModel.newKieSessionModel("base-session-1")
                .setDefault(false)
                .setType(KieSessionModel.KieSessionType.STATEFUL)
                .setClockType(ClockTypeOption.REALTIME)
//                .setBeliefSystem()
                .setThreadSafe(true);

        fileSystem.writeKModuleXML(module.toXML());

        KieBuilder kieBuilder = service.newKieBuilder(fileSystem);
        kieBuilder.buildAll();

    }

    public void newLogger(KieServices services) {
        KieContainer container = services.getKieClasspathContainer();
        KieBase kieBase = container.getKieBase();
        KieSession session = kieBase.newKieSession();

        session.execute(CommandFactory.newSetGlobal("a", "a", true));
        KieRuntimeLogger filename = services.getLoggers().newFileLogger(session, "filename");


        // ...
        filename.close();

    }

    public void command(KieServices services) {
        KieContainer container = services.getKieClasspathContainer();
        KieBase kbase = container.getKieBase();

        KieSession ksession = kbase.newKieSession();

        List cmds = new ArrayList();
        cmds.add(CommandFactory.newInsert("this is object", "stilton"));
        cmds.add(CommandFactory.newStartProcess("process cheeses"));
        cmds.add(CommandFactory.newQuery("cheeses", "cheeses"));
        ExecutionResults bresults = ksession.execute(CommandFactory.newBatchExecution(cmds));
        bresults.getValue("stilton");
        QueryResults qresults = (QueryResults) bresults.getValue("cheeses");
    }

}
