package com.test.drools.service;

import com.test.drools.pojo.dto.rules.Person;
import org.kie.api.definition.rule.Rule;
import org.kie.api.event.rule.ObjectDeletedEvent;
import org.kie.api.event.rule.ObjectInsertedEvent;
import org.kie.api.event.rule.ObjectUpdatedEvent;
import org.kie.api.event.rule.RuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * 
 * @since VERSION
 */

@Service
public class DroolsService {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private STDService stdService;

    public void service() {
        KieSession kieSession = kieContainer.newKieSession();

        kieSession.setGlobal("stdService", stdService);

        Person person = new Person();
        person.setAge(100);
        kieSession.insert(person);
        kieSession.removeEventListener(new RuleRuntimeEventListener() {
            @Override
            public void objectInserted(ObjectInsertedEvent event) {
                Rule rule = event.getRule();
                rule.getName();
            }

            @Override
            public void objectUpdated(ObjectUpdatedEvent event) {

            }

            @Override
            public void objectDeleted(ObjectDeletedEvent event) {

            }
        });

        kieSession.fireAllRules();
        kieSession.dispose();
    }

}
