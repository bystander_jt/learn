package com.test.drools.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 
 * 
 * @since VERSION
 */
@Slf4j
@Service
public class STDService {

    public void invoke(){
      log.info("something invoked this method.");
    }
}
