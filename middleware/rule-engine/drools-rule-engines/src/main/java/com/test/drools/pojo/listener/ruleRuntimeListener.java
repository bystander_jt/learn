package com.test.drools.pojo.listener;

import org.kie.api.event.rule.ObjectDeletedEvent;
import org.kie.api.event.rule.ObjectInsertedEvent;
import org.kie.api.event.rule.ObjectUpdatedEvent;
import org.kie.api.event.rule.RuleRuntimeEventListener;

/**
 * 
 * 
 * @since VERSION
 */

public class ruleRuntimeListener implements RuleRuntimeEventListener {

    @Override
    public void objectInserted(ObjectInsertedEvent event) {

    }

    @Override
    public void objectUpdated(ObjectUpdatedEvent event) {

    }

    @Override
    public void objectDeleted(ObjectDeletedEvent event) {

    }
}
