package com.test.drools.configuration;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 
 * 
 * @since VERSION
 */

@Data
@ConfigurationProperties(prefix = "custom")
public class CustomProperties {

    private String name;

}
