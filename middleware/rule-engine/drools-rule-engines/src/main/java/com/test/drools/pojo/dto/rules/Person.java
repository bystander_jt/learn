package com.test.drools.pojo.dto.rules;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 
 * @since VERSION
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String name;

    private Integer age;

    public static Person getFrom(Integer age){
        return new Person("randomName", age);
    }

}
