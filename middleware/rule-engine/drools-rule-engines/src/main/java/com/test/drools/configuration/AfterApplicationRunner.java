package com.test.drools.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * 
 * @since VERSION
 */

@Slf4j
@Configuration
public class AfterApplicationRunner implements ApplicationRunner {

    @Autowired
    private CustomProperties customProperties;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info(customProperties.getName());
    }

}
