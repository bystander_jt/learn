package cn.lee.es;

import cn.lee.es.service.ESService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;


@SpringBootTest(classes = Main.class)
public class IndicesTester {


    @Autowired
    private ESService esService;

    @Test
    public void testCreateIndex(){
        esService.createIndex();
    }

    @Test
    public void testInsertDocument(){
        try {
            esService.index();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testDeleteIndex(){
        esService.deleteIndex();
    }


}
