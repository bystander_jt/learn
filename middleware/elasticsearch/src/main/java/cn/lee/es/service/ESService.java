package cn.lee.es.service;

import cn.lee.es.pojo.dto.Product;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.mapping.DynamicMapping;
import co.elastic.clients.elasticsearch._types.mapping.IndexOptions;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.indices.CreateIndexRequest;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import co.elastic.clients.elasticsearch.indices.DeleteIndexResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class ESService {

    @Autowired
    private ElasticsearchClient client;

    public void index() throws IOException {
        Product product = new Product();
        product.setSku("bike-1");
        product.setName("bike");
        product.setPrice(2000.0);
        IndexResponse response = client.index(index ->
                index.index("product")
                        .id(product.getSku())
                        .document(product)
        );

        log.info("Index version: {}", response.version());

    }

    public void createIndex() {

        CreateIndexRequest.Builder builder = new CreateIndexRequest.Builder();

        // https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-params.html
        CreateIndexRequest build = builder.mappings(typeBuilder ->
                typeBuilder.properties("sku", pro ->
                        pro.text(tex ->
                                tex.indexOptions(IndexOptions.Docs)
//                                        .dynamic(DynamicMapping.True)
                        )
                )
        ).index("product").build();

        try {
            CreateIndexResponse response = client.indices().create(build);
            log.info("success: {}, raw response: {}", response.acknowledged(), response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteIndex(){
        try {
            DeleteIndexResponse response = client.indices().delete(delete ->
                    delete.index("product").allowNoIndices(true));
            log.info("delete index product with [{}] response", response.acknowledged());
        } catch (IOException e) {
            log.error("delete index product err {}[{}]", e.getClass().getSimpleName(), e.getMessage());
        }
    }

}

