package cn.lee.es.configuration;

import co.elastic.clients.transport.TransportUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.NodeSelector;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.sniff.Sniffer;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchProperties;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchRestClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

@Slf4j
@Configuration
public class ElasticSearchConfiguration {

    private static RestClient officialESLowLevelRESTClient;
    private static Sniffer sniffer;

    @Bean()
    public BasicCredentialsProvider officialESLowLevelRESTCredential(ElasticsearchProperties properties) {
        BasicCredentialsProvider credentialProvider = new BasicCredentialsProvider();
        credentialProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(properties.getUsername(), properties.getPassword()));
        return credentialProvider;
    }

    public SSLContext createTLS(String path) throws RuntimeException {
        try {
            return TransportUtils.sslContextFromHttpCaCrt(new File(path));
        } catch (IOException e) {
        }
            return  null;
    }


    @Bean(name = {"esLLRC"})
    public RestClient officialESLowLevelRESTClient(ElasticsearchProperties properties) {
        officialESLowLevelRESTClient =  RestClient.builder(new HttpHost("10.0.0.157", 9200, "https"))
                .setPathPrefix(properties.getPathPrefix())
                .setCompressionEnabled(true)
                .setHttpClientConfigCallback(builder -> builder.setDefaultCredentialsProvider(officialESLowLevelRESTCredential(properties))
                        .setSSLContext(createTLS("E:\\Users\\developer\\code\\learn\\middleware\\elasticsearch\\src\\main\\resources\\es\\tls\\elastic.crt"))
                        // ignored ssl hostname verify
                        .setSSLHostnameVerifier((s, sslSession) -> true))
                .setFailureListener(new RestClient.FailureListener() {

                    @Override
                    public void onFailure(Node node) {
                        log.error("node initialize failure {}:{}", node.getHost(), node.getVersion());
                    }
                })
                .setNodeSelector(NodeSelector.SKIP_DEDICATED_MASTERS)
                .build();
        return officialESLowLevelRESTClient;
    }

    @Bean
    public Sniffer createSniffer(RestClient officialESLowLevelRESTClient) {
        return Sniffer.builder(officialESLowLevelRESTClient).build();
    }


    @PreDestroy
    public void preDestroy() {
        try {
            if (officialESLowLevelRESTClient != null)
                officialESLowLevelRESTClient.close();
            if (sniffer != null)
                sniffer.close();
        } catch (IOException e) {

        }
    }

}
