package cn.lee.es.configuration;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.elasticsearch.client.RestClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EsClientConfiguration {

    @Bean
    public ElasticsearchClient create( RestClient officialESLowLevelRESTClient){
        RestClientTransport restClientTransport = new RestClientTransport(officialESLowLevelRESTClient, new JacksonJsonpMapper());
        return new ElasticsearchClient(restClientTransport);
    }

}
