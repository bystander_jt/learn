package cn.lee.kafka.stream.stateful.group;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.WindowStore;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import static cn.lee.kafka.stream.StreamsUtils.loadProperties;

@Slf4j
public class StreamWindowsExample {

    private final String APP_ID = "STREAM-WINDOW";

    public static void main(String[] args) throws IOException, InterruptedException {
        KafkaStreams window = new StreamWindowsExample().window();

        CountDownLatch latch = new CountDownLatch(1);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            window.close();
            latch.countDown();
        }));

        latch.await();
    }

    public KafkaStreams window() throws IOException {

        Properties properties = loadProperties("kafka-stream.properties");
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, APP_ID);
        properties.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, APP_ID);
        properties.setProperty(StreamsConfig.STATE_DIR_CONFIG, "E:\\Users\\developer\\code\\learn\\middleware\\kafka\\store\\" + APP_ID);
        String topic = "window";


        StreamsBuilder builder = new StreamsBuilder();
        builder.stream(topic,
                        Consumed.with(Serdes.String(), Serdes.String())
                                .withOffsetResetPolicy(Topology.AutoOffsetReset.LATEST)
                                .withName("source-processor")
                )
                .peek(((key, value) -> log.info("input [{}:{}]", key, value)))
                .groupByKey()
//                .windowedBy(TimeWindows.ofSizeAndGrace(Duration.ofSeconds(10), Duration.ofSeconds(1)))
                .windowedBy(SlidingWindows.ofTimeDifferenceWithNoGrace(Duration.ofSeconds(10)))
                .count(Named.as("sliding-window-count"),
                        Materialized.<String, Long, WindowStore<Bytes, byte[]>>as("sliding-window-state-store")
                                .withValueSerde(Serdes.Long())
                                .withKeySerde(Serdes.String())
                ).toStream()
                .peek(((key, value) -> log.info("time [{}-{}]: {} count: {}", key.window().start(), key.window().end(), key.key(), value)))
        ;


        Topology topology = builder.build(properties);

        KafkaStreams stream = new KafkaStreams(topology, properties);
        stream.start();
        return stream;

    }
}
