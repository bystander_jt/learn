package cn.lee.kafka.stream.serializer;

import cn.lee.kafka.stream.StreamObject;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class StreamObjectSerde implements Serde<StreamObject> {

    public static Serde<StreamObject> serde() {
        return new StreamObjectSerde();
    }


    @Override
    public Serializer<StreamObject> serializer() {
        return new StreamObjectSerializer();
    }

    @Override
    public Deserializer<StreamObject> deserializer() {
        return new StreamObjectDeSerializer();
    }
}
