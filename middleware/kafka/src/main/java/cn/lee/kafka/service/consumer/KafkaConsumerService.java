package cn.lee.kafka.service.consumer;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.*;


@Component
public class KafkaConsumerService implements ApplicationRunner {


    @Autowired
    private KafkaConsumer<String, String> consumer;

    private final Map<TopicPartition, OffsetAndMetadata> currentOffset = new HashMap<>();

    private class ConsumerListener implements OffsetCommitCallback {

        @Override
        public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, Exception exception) {
            Set<TopicPartition> topicPartitions = offsets.keySet();
            topicPartitions.forEach(currentOffset::remove);
        }
    }

    /**
     * 防止在 "再均衡" 期间所即将失去当前分区的操作权限时, 提交当前已经处理过的消息数据偏移量.
     * 让获得该分区的消费者知道从何开始处理, 减少消息重复处理的情况.
     */
    private class HandlerBalancer implements ConsumerRebalanceListener {

        @Override
        public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            consumer.commitSync(currentOffset, null);
            currentOffset.clear();
        }

        @Override
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) {

        }
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<String> subscribeTopics = new ArrayList<>();

        ConsumerListener consumerListener = new ConsumerListener();

        consumer.subscribe(subscribeTopics, new HandlerBalancer());

        ConsumerRecords<String, String> poll;
        try {
            while ((poll = consumer.poll(Duration.ofMillis(100))) != null) {
                // consumer messages;

                for (ConsumerRecord<String, String> record : poll) {
                    // service logic;

                    currentOffset.put(new TopicPartition(record.topic(), record.partition()), new OffsetAndMetadata(record.offset(), null));
                }


                // 异步提交, 加速业务消费速率, 但存在
                consumer.commitAsync(currentOffset, consumerListener);
                consumer.commitAsync();

                // 同步执行, 会阻塞业务消费速率
//                consumer.commitSync();
            }
        } finally {
            consumer.commitSync(currentOffset);
            currentOffset.clear();
            consumer.close();
        }
    }


}
