package cn.lee.kafka.stream.stateful;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Aggregator;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Reducer;
import org.apache.kafka.streams.state.KeyValueStore;

public class KafkaStatefulDemonstrate {

    public void reduce() {

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, Long> stream = builder.stream("topic");

        // 这里的 value1 是首个事件， value 2 则是后续收到的事件.
        Reducer<Long> reducer = ((value1, value2) -> value1 + value2);

        // 这种状态需要进行持久化，所以我们需要声明一个 materialized
        Materialized<String, Long, KeyValueStore<Bytes, byte[]>> stateStore = Materialized.with(Serdes.String(), Serdes.Long());

        stream.groupByKey()
                // 通过 reduce 操作后, 会将 stream 转换为 ktable.
                .reduce(reducer, stateStore)
                // 将 ktable 转换为 kstream 形成事件
                .toStream()
                // 然后指定输出的 topic 中。
                .to("topic-b");
    }

    /**
     * {@link org.apache.kafka.streams.kstream.KGroupedStream#aggregate} 可以将使用不同的数据类型. 而
     * {@link  org.apache.kafka.streams.kstream.KGroupedStream#reduce reduce} 则必须是相同类型才可以。
     */
    public void aggregation() {
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> stream = builder.stream("topic");

        Aggregator<String, String, Long> charCnt = ((key, value, aggregate) ->
                aggregate + (key.length() + value.length())
        );

        // 这种状态需要进行持久化，所以我们需要声明一个 materialized
        Materialized<String, Long, KeyValueStore<Bytes, byte[]>> stateStore = Materialized.with(Serdes.String(), Serdes.Long());

        stream.groupByKey()
                .aggregate(() -> 0L, charCnt)
                // 将 ktable 转换为 kstream 形成事件
                .toStream()
                // 然后指定输出的 topic 中。
                .to("topic-b");


    }
}
