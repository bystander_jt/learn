package cn.lee.kafka.stream.exception;

import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;

public class StreamHandler implements StreamsUncaughtExceptionHandler {


    @Override
    public StreamThreadExceptionResponse handle(Throwable exception) {
//        return StreamThreadExceptionResponse.REPLACE_THREAD;
//        return StreamThreadExceptionResponse.SHUTDOWN_CLIENT;
        return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
    }

}
