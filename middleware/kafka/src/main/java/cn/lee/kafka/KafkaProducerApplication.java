package cn.lee.kafka;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.locks.LockSupport;

public class KafkaProducerApplication {


    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("kafka-producer.xml");

        LockSupport.park();
    }
}
