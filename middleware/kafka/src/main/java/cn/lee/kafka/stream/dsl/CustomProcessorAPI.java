package cn.lee.kafka.stream.dsl;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.processor.api.Processor;
import org.apache.kafka.streams.processor.api.ProcessorContext;
import org.apache.kafka.streams.processor.api.ProcessorSupplier;
import org.apache.kafka.streams.processor.api.Record;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;

import java.time.Duration;

public class CustomProcessorAPI implements ProcessorSupplier<String, String, String, Long> {

    private final String stateStoreName;

    public CustomProcessorAPI(String stateStoreName) {
        this.stateStoreName = stateStoreName;
    }


    @Override
    public Processor<String, String, String, Long> get() {
        return new Processor<String, String, String, Long>() {


            private ProcessorContext<String, Long> context;
            private KeyValueStore<String, Long> store;

            @Override
            public void init(ProcessorContext context) {
                this.context = context;
                this.store = (KeyValueStore<String, Long>) context.getStateStore(stateStoreName);
                this.context.schedule(Duration.ofMillis(300), PunctuationType.WALL_CLOCK_TIME, this::forward);
            }

            private void forward(Long timestamp) {
                try (KeyValueIterator<String, Long> iterator = store.all()) {
                    while (iterator.hasNext()) {
                        KeyValue<String, Long> kv = iterator.next();
                        Record<String, Long> record = new Record<>(kv.key, kv.value, timestamp);
                        context.forward(record);
                    }
                }
            }

            @Override
            public void process(Record<String, String> record) {
                String key = record.key();
                Long currentLength = store.get(key);
                if (currentLength == null) currentLength = 0L;
                currentLength += record.value().length();
                store.put(key, currentLength);
            }
        };
    }


}
