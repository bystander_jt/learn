package cn.lee.kafka.stream.stateful;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.errors.UnknownStateStoreException;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Slf4j()
public class WordCount {


    public static void main(String[] args) throws InterruptedException {


        WordCount streamProgram = new WordCount();

        KafkaStreams stream = streamProgram.stream();

        CountDownLatch latch = new CountDownLatch(1);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            stream.close();
            latch.countDown();
        }));


        stream.start();
        latch.await();

    }


    public KafkaStreams stream() {
        Properties properties = loadProperties();

        StoreBuilder<KeyValueStore<String, Integer>> stateStoreBuilder = stateStore(properties);
        String stateStoreName = stateStoreBuilder.name();

        String sourceTopic = properties.getProperty("stream.topic.income");
        String outputTopic = properties.getProperty("stream.topic.output");

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> kStream = createKStream(builder, sourceTopic);


        // key: null, value: hello world
        // flatmap -> key: hello, value: hello
        //         -> key: world, value: world
        kStream
                .peek((key, value) -> log.info("key: {}, value: {}", key, value), Named.as("before-flat-map-peek"))
                .flatMap((k, v) -> Arrays.stream(v.split("\\s+"))
                        .map(d -> KeyValue.pair(d, d))
                        .collect(Collectors.toList())
                ).peek((key, value) -> log.info("key: {}, value: {}", key, value), Named.as("before-transform-peek"))
                .repartition(Repartitioned.streamPartitioner((StreamPartitioner<String, String>) (topic, key, value, numPartitions) -> null)
                        .withName("repartition")
                )
                .transform(() -> new Transformer<String, String, KeyValue<String, Integer>>() {
                    KeyValueStore<String, Integer> stateStore = null;

                    @Override
                    public void init(ProcessorContext context) {
                        this.stateStore = context.getStateStore(stateStoreName);
                        if (stateStore == null)
                            throw new UnknownStateStoreException("can not find [" + stateStoreName + "] state store");
                    }

                    @Override
                    public KeyValue<String, Integer> transform(String key, String value) {
                        Integer count = stateStore.get(key);
                        if (count == null) count = 0;
                        ++count;

                        stateStore.put(key, count);
                        return KeyValue.pair(key, count);
                    }

                    @Override
                    public void close() {
                        stateStore.close();
                    }
                }, Named.as("transform-processor"))
                .peek(((key, value) -> log.info("{} count: {}", key, value)), Named.as("after-transform-processor"))
                .to(outputTopic, Produced.with(Serdes.String(), Serdes.Integer()));

        Topology topology = createTopology(builder, properties);
        topology.addStateStore(stateStoreBuilder, "transform-processor");
        return new KafkaStreams(topology, properties);
    }

    public KStream<String, String> createKStream(StreamsBuilder builder, String sourceTopic) {
        return builder.stream(sourceTopic, Consumed.with(Serdes.String(), Serdes.String())
                .withName("source-processor")
                .withOffsetResetPolicy(Topology.AutoOffsetReset.LATEST)
        );
    }

    public Topology createTopology(StreamsBuilder builder, Properties properties) {
        return builder.build(properties);
    }

    public Properties loadProperties() {
        Properties properties = new Properties();
        try {
            URL resource = this.getClass().getClassLoader().getResource("kafka-word-count.properties");
            properties.load(resource.openStream());
        } catch (IOException ignored) {

        }
        return properties;
    }

    public StoreBuilder<KeyValueStore<String, Integer>> stateStore(Properties properties) {
        return Stores.keyValueStoreBuilder(
                Stores.inMemoryKeyValueStore(properties.getProperty("state.name")),
                Serdes.String(), Serdes.Integer()
        );
    }


}
