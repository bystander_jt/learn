package cn.lee.kafka.stream.materializer;

import cn.lee.kafka.stream.StreamObject;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.internals.RocksDBStore;

public class StreamObjectMaterialize extends Materialized<String, StreamObject, RocksDBStore> {

    /**
     * Copy constructor.
     *
     * @param materialized the {@link Materialized} instance to copy.
     */
    protected StreamObjectMaterialize(Materialized<String, StreamObject, RocksDBStore> materialized) {
        super(materialized);
    }


}
