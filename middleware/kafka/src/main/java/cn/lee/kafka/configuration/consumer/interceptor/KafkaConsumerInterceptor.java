package cn.lee.kafka.configuration.consumer.interceptor;

import org.apache.kafka.clients.consumer.ConsumerInterceptor;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.util.Map;

public class KafkaConsumerInterceptor implements ConsumerInterceptor<String, String> {

    /**
     * 用故意处理在 {@code poll()} 方法钱进行消息筛查. 返回期望或者过滤无效消息.
     * 也可以通过其实现类似 TTL(Time-To-Live) 消息过期机制
     * @param records records to be consumed by the client or records returned by the previous interceptors in the list.
     * @return
     */
    @Override
    public ConsumerRecords<String, String> onConsume(ConsumerRecords<String, String> records) {

        // CUSTOM LOGIC;

        return records;
    }

    @Override
    public void onCommit(Map<TopicPartition, OffsetAndMetadata> offsets) {

    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
