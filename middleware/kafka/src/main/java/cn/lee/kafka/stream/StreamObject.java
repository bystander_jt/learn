package cn.lee.kafka.stream;

import com.alibaba.fastjson2.JSONObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class StreamObject implements Serializable {


    private String id;

    private LocalDateTime timestamp;

    private Integer max;

    private JSONObject keyValMap;


}
