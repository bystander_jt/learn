package cn.lee.kafka.service.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

@Slf4j
@Component
public class KafkaConsumerPointOffsetService {


    @Autowired
    private KafkaConsumer<String, String> consumer;

    private static final ExecutorService executor = Executors.newFixedThreadPool(1);


    @PostConstruct
    public void postConstruct() {
        executor.submit(() -> {

            consumer.subscribe(Pattern.compile("^*"));

            // 当前订阅的 topic 和 partitions
            Set<TopicPartition> topicPartitions = consumer.assignment();

            if (CollectionUtils.isEmpty(topicPartitions)) {
                log.warn("current consumer subscribe noting. consumer thread exist!");
                return;
            }


            // 从最开始的消息进行读取
            consumer.seekToBeginning(topicPartitions);

            // 直接订阅最末尾的 offset, 从末尾开始读取
            consumer.seekToEnd(topicPartitions);

            ConsumerRecords<String, String> records;

            try {
                while ((records = consumer.poll(Duration.ofMillis(100))) != null) {
                    for (ConsumerRecord<String, String> record : records) {


                    }
                }

            } catch (WakeupException e) {
                log.warn("consumer thread exist, because other thread has invoke wakeup() method of this consumer.");
            } finally {
                consumer.close();
            }
        });


    }

}
