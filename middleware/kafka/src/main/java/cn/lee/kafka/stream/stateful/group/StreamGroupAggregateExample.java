package cn.lee.kafka.stream.stateful.group;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Named;
import org.apache.kafka.streams.state.KeyValueStore;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

@Slf4j
public class StreamGroupAggregateExample {

    private static final String APP_ID = "STREAM_GROUP_AGGREGATE";

    public static void main(String[] args) {
        StreamGroupAggregateExample group = new StreamGroupAggregateExample();
        group.groupStream();
    }

    private Properties loadProperties() {
        Properties properties = new Properties();
        try {
            URL resource = this.getClass().getClassLoader().getResource("kafka-stream-group.properties");
            assert resource != null;
            properties.load(resource.openStream());
        } catch (IOException ignored) {

        }
        return properties;
    }

    /**
     * <p>测验 aggregate</p>
     * <pre>
     *     输入测试:
     *     - 1:Mike
     *     输出:
     *     - input [1:Mike]
     *     - after aggregate [1:Mike]
     *
     *     再次输入:
     *     - 1:Mike
     *     输出:
     *     - input [1:Mike]
     *     - after aggregate [1:MikeMike]
     * </pre>
     */

    public void groupStream() {
        Properties properties = loadProperties();
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, APP_ID);
        properties.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, APP_ID);
        properties.setProperty(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");


        String topic = properties.getProperty("stream.topic.group.input");
        properties.setProperty(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, String.valueOf(100));


        StreamsBuilder builder = new StreamsBuilder();
        builder.stream(topic,
                        Consumed.with(Serdes.String(), Serdes.String())
                                .withOffsetResetPolicy(Topology.AutoOffsetReset.LATEST)
                                .withName("source-processor")
                )
                .peek(((key, value) -> log.info("input [{}:{}]", key, value)))
                .groupByKey()
                .aggregate(
                        // 初始化
                        () -> "",
                        // 和 reduce 一样, 不过返回 aggregate
                        (key, value, aggregate) -> value + aggregate,
                        // 命名当前操作
                        Named.as("aggregate-process"),
                        // 状态存储序列化, 用于同步到 kafka changelog.
                        Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(APP_ID + "-state-store").withKeySerde(Serdes.String()).withValueSerde(Serdes.String()))
                .toStream(Named.as("convert-to-stream"))
                .peek(((key, value) -> log.info("after aggregate [{}:{}]", key, value)))
        ;


        Topology topology = builder.build(properties);

        try (KafkaStreams stream = new KafkaStreams(topology, properties)) {
            stream.start();
        }

    }


}
