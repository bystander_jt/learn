package cn.lee.kafka.stream.stateful.group;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;

@Slf4j
public class StreamGroupExample {

    private static final String APP_ID = "STREAM_GROUP";

    public static void main(String[] args) {
        StreamGroupExample group = new StreamGroupExample();
        group.groupStream();
        ;
    }

    private Properties loadProperties(String filename) {
        Properties properties = new Properties();
        try {
            URL resource = this.getClass().getClassLoader().getResource(filename);
            properties.load(resource.openStream());
        } catch (IOException ignored) {

        }
        return properties;
    }


    public void groupStream() {
        Properties properties = loadProperties("kafka-stream-group.properties");
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, APP_ID);
        properties.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, APP_ID);
        properties.setProperty(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");


        String topic = properties.getProperty("stream.topic.group.input");
        properties.setProperty(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, String.valueOf(100));


        StreamsBuilder builder = new StreamsBuilder();
        builder.stream(topic,
                        Consumed.with(Serdes.String(), Serdes.String())
                                .withOffsetResetPolicy(Topology.AutoOffsetReset.LATEST)
                                .withName("source-processor")
                )
                .peek(((key, value) -> log.info("input [{}:{}]", key, value)))
                .groupByKey()
                .count(Materialized.with(Serdes.String(), Serdes.Long()))
                .toStream()
                .peek(((key, value) -> log.info("after group peek: [{}:{}]", key, value)), Named.as("after-group"))

        ;


        Topology topology = builder.build(properties);

        KafkaStreams stream = new KafkaStreams(topology, properties);
        stream.start();

    }


}
