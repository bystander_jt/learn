package cn.lee.kafka.configuration.producer;

import cn.lee.kafka.configuration.serialize.KafkaObjectSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Configuration
public class KafkaProducerConfiguration {

    @Bean
    public List<String> createInterceptors() {
        ArrayList<String> list = new ArrayList<>(3);
        list.add("cn.lee.kafka.configuration.producer.interceptor.KafkaProduceInterceptor");
        return list;
    }


    @Bean
    public Properties kafkaProperties(List<String> interceptors) {
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "10.0.0.115:9090,10.0.0.115:9093,10.0.0.115:9094");


        properties.put(ProducerConfig.DELIVERY_TIMEOUT_MS_CONFIG, 10_000);
        properties.put(ProducerConfig.LINGER_MS_CONFIG, 1000);
        properties.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 5_000);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        /* 自定义 interceptor */
        properties.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, interceptors);

        /* 自定义序列化和反序列化 */
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaObjectSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaObjectSerializer.class);

//        properties.put(ProducerConfig.BUFFER_MEMORY_CONFIG, )

        /* 自定义 partition */
        properties.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, KafkaPartitionSelector.class);

        return properties;
    }


    @Bean
    public KafkaProducer<String, String> create(Properties kafkaProperties) {
        return new KafkaProducer<String, String>(kafkaProperties);
    }


}
