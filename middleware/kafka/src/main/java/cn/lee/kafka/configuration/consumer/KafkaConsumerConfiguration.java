package cn.lee.kafka.configuration.consumer;

import cn.lee.kafka.configuration.serialize.KafkaObjectSerializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

@Configuration
public class KafkaConsumerConfiguration {


    @Bean
    public List<String> createInterceptors() {
        ArrayList<String> list = new ArrayList<>(3);
        list.add("cn.lee.kafka.interceptor.KafkaProduceInterceptor");
        return list;
    }


    @Bean
    public Properties kafkaProperties(List<String> interceptors) {
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        /* 自定义 interceptor */
        properties.put(ConsumerConfig.INTERCEPTOR_CLASSES_CONFIG, interceptors);
        /* 自定义反序列化 */
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaObjectSerializer.class);

        /* 消费者组 */
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "test-group");
        properties.put(ConsumerConfig.GROUP_INSTANCE_ID_CONFIG, UUID.randomUUID().toString());

//        ConsumerConfig.CLIENT_ID_CONFIG

        return properties;
    }


    @Bean(name = "consumer")
    public KafkaConsumer<String, String> kafkaConsumer(Properties kafkaProperties) {
        return new KafkaConsumer<String, String>(kafkaProperties);
    }


}
