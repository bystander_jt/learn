package cn.lee.kafka.configuration.serialize;

import com.alibaba.fastjson2.JSON;
import org.apache.kafka.common.serialization.Deserializer;

public class KafkaObjectDeserializer implements Deserializer<Object> {

    @Override
    public Object deserialize(String topic, byte[] data) {
        return JSON.parse(new String(data));
    }

}
