package cn.lee.kafka.stream;

import com.alibaba.fastjson2.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;

/**
 * 时间戳提取器， 用于标注事件产生的时间.
 * kafka 将按照该时间进行顺序消费
 */
public class TimestampExtractorDemonstrate {

    static class CustomTimestampExtractor implements TimestampExtractor {

        @Override
        public long extract(ConsumerRecord<Object, Object> record, long partitionTime) {
            long timestamp = record.timestamp();
            Object value = record.value();
            if (value instanceof JSONObject) {
                timestamp = ((JSONObject) record.value()).getLong("timestamp");
            }
            return timestamp;
        }
    }


}
