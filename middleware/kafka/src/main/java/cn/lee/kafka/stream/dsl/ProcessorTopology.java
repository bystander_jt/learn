package cn.lee.kafka.stream.dsl;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;

import java.util.Properties;

public class ProcessorTopology {


    public static void main(String[] args) {

        String stateStore = "store";

        KeyValueStore<String, String> store = Stores.keyValueStoreBuilder(
                        Stores.inMemoryKeyValueStore(stateStore),
                        Serdes.String(), Serdes.String())
                .build();


        Topology topology = new Topology();

        topology.addSource("source", Serdes.String().deserializer(), Serdes.String().deserializer(), "topic-a");

        topology.addSource("source", "topic-a");


        topology.addProcessor("custom-processor", new CustomProcessorAPI(stateStore), "source");

        topology.addSink("sink", "output-topic", Serdes.String().serializer(), Serdes.String().serializer(), "custom-processor");

        topology.addSink("sink-2", "output-topic", (StreamPartitioner<String, String>) (topic, key, value, numPartitions) -> topic.length() % numPartitions, "custom-processor");


        KafkaStreams streams = new KafkaStreams(topology, new Properties());
        streams.start();

    }
}
