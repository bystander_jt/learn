package cn.lee.kafka.configuration.serialize;

import com.alibaba.fastjson2.JSON;
import org.apache.kafka.common.serialization.Serializer;

public class KafkaObjectSerializer implements Serializer<Object> {

    @Override
    public byte[] serialize(String topic, Object data) {
        return JSON.toJSONString(data).getBytes();
    }


}
