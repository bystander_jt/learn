package cn.lee.kafka.stream.stateful.interactive;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Named;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class StreamInteractiveExample {


    private static final String APP_ID = "STREAM_INTERACTIVE_DEMONSTRATE";
    private static final String STATE_STORE_NAME = APP_ID + "_STATE_STORE";

    public static void main(String[] args) throws IOException, InterruptedException {
        StreamInteractiveExample example = new StreamInteractiveExample();
        KafkaStreams stream = example.stream();

        CountDownLatch latch = new CountDownLatch(1);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            stream.start();
            latch.countDown();
        }));

        stream.start();

        example.inputs(example.interactive(stream));


        latch.await();

    }


    public KafkaStreams stream() throws IOException {

        Properties properties = loadProperties();
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, APP_ID);
        properties.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, APP_ID);
        properties.setProperty(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");

        String incomeTopic = "INTERACTIVE";

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> stream = builder.stream(incomeTopic, Consumed.with(Serdes.String(), Serdes.String()));
        stream.peek(((key, value) -> log.info("income data: {}-{}", key, value)))
                .groupByKey()
                .count(Named.as(APP_ID),
                        Materialized.<String, Long, KeyValueStore<Bytes, byte[]>>as(STATE_STORE_NAME)
                                .withKeySerde(Serdes.String())
                                .withValueSerde(Serdes.Long())
                );


        return new KafkaStreams(builder.build(), properties);
    }

    private Properties loadProperties() {
        Properties properties = new Properties();
        try {
            URL resource = this.getClass().getClassLoader().getResource("kafka-stream-interactive.properties");
            assert resource != null;
            properties.load(resource.openStream());
        } catch (IOException ignored) {

        }
        return properties;
    }

    private void inputs(ReadOnlyKeyValueStore<String, Long> store) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {

            String key = scanner.nextLine();
            if (StringUtils.isNotBlank(key)) {
                Long count = store.get(key);
                System.out.printf("%s: %s\n", key, Optional.ofNullable(count).orElse(0L));
            }


        }


    }

    public ReadOnlyKeyValueStore<String, Long> interactive(KafkaStreams streams) {

        StoreQueryParameters<ReadOnlyKeyValueStore<String, Long>> parameters
                = StoreQueryParameters.fromNameAndType(STATE_STORE_NAME, QueryableStoreTypes.keyValueStore());

        return streams.store(parameters);
    }


}
