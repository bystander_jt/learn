package cn.lee.kafka.stream.serializer;

import cn.lee.kafka.stream.StreamObject;
import com.alibaba.fastjson2.JSON;
import org.apache.kafka.common.serialization.Deserializer;

class StreamObjectDeSerializer implements Deserializer<StreamObject> {

    @Override
    public StreamObject deserialize(String topic, byte[] data) {
        return JSON.parseObject(data, StreamObject.class);
    }
}
