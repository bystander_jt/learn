package cn.lee.kafka.service.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Component
public class KafkaConsumerSyncAndAsyncCombination {


    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private final AtomicInteger commitSequence = new AtomicInteger(0);
    @Autowired
    private KafkaConsumer<String, String> consumer;

    @PostConstruct
    public void runConsumer() {

        EXECUTOR_SERVICE.submit(() -> {
            consumer.subscribe(Pattern.compile("."));
            Map<TopicPartition, OffsetAndMetadata> consumeOffsets = new HashMap<>(10);


            ConsumerRecords<String, String> records;
            try {
                while ((records = consumer.poll(Duration.ofMillis(100))) != null) {

                    for (ConsumerRecord<String, String> record : records) {
                        TopicPartition topicPartition = new TopicPartition(record.topic(), record.partition());
                        consumeOffsets.put(topicPartition, new OffsetAndMetadata(record.offset(), String.valueOf(commitSequence.get())));
                        System.out.println("consumer record: " + record.topic() + " " + record.partition() + " " + record.offset());
                    }

                    consumer.commitAsync(consumeOffsets, new SequenceCommitAsync<>(commitSequence, consumer));

                }
            } catch (WakeupException e) {
                log.warn("other thread invoke wakeup() method, current consumer exist!");
            } finally {
                consumer.commitSync();
                consumer.close();
            }
        });

    }


    private static class SequenceCommitAsync<K, V> implements OffsetCommitCallback {

        private final AtomicInteger commitSequence;
        private final KafkaConsumer<K, V> consumer;

        private SequenceCommitAsync(AtomicInteger commitSequence, KafkaConsumer<K, V> consumer) {
            this.commitSequence = commitSequence;
            this.consumer = consumer;
        }


        @Override
        public void onComplete(Map<TopicPartition, OffsetAndMetadata> map, Exception e) {

            String offsets = map.values().stream().map(OffsetAndMetadata::offset).map(String::valueOf).collect(Collectors.joining(","));
            if (e != null) {
                log.error("consumer offset [{}] {}: {}", offsets, e.getClass().getSimpleName(), e.getMessage());

                Map<TopicPartition, OffsetAndMetadata> retryMap = new HashMap<>(map.size());

                map.forEach((k, v) -> {
                    if (v.metadata().equals(String.valueOf(commitSequence.get()))) {
                        retryMap.put(k, v);
                    }
                });

                if (!CollectionUtils.isEmpty(retryMap))
                    consumer.commitSync(retryMap);
            }
        }
    }


}
