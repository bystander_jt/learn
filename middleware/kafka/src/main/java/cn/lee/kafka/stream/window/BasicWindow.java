package cn.lee.kafka.stream.window;

import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.SlidingWindows;
import org.apache.kafka.streams.kstream.TimeWindows;

import java.time.Duration;

public class BasicWindow {


    /**
     * 滚动窗口
     */
    public void tumblingWindow() {

        TimeWindows timeWindows = TimeWindows.ofSizeWithNoGrace(Duration.ofSeconds(5));


    }

    public void hoppingWindow() {
        TimeWindows timeWindows = TimeWindows.ofSizeWithNoGrace(Duration.ofSeconds(30)).advanceBy(Duration.ofSeconds(10));
    }

    /**
     * 会话窗口. 只要事件流入就会将数据进行 merge 操作.
     * 如果在 {@link Duration inactiveGap} 没有事件,就不会进行 merge 操作.
     */
    public void sessionWindow() {
        SessionWindows.ofInactivityGapWithNoGrace(Duration.ofMinutes(5));
    }

    /**
     * 滑动窗口
     */
    public void slidingWindow() {
        SlidingWindows.ofTimeDifferenceAndGrace(Duration.ofSeconds(30), Duration.ofMillis(500));
    }

}
