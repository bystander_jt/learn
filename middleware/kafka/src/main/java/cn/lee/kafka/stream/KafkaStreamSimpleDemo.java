package cn.lee.kafka.stream;

import cn.lee.kafka.stream.serializer.StreamObjectSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

@Slf4j
public class KafkaStreamSimpleDemo {

    public KStream<String, String> buildStream() {
        StreamsBuilder builder = new StreamsBuilder();
        // 定义来源 topic. 和 key value 的类型.
        KStream<String, String> stream = builder.stream("stream-topic", Consumed.with(Serdes.String(), Serdes.String()));
        // 定义数据过滤
        stream = stream.filter((k, v) -> k.contains("keywords"));

        // map 数据转化
        stream.mapValues(v -> v.substring(5))
                .map((key, value) -> {
                    String newKey = key.substring(3);
                    value += " new Value";
                    return new KeyValue<>(newKey, value);
                });

        // 将符合定义条件的 record 发送到指定的 topic 中
        stream.to("new-topic");
        return stream;

    }


    public void createStream() throws IOException {

        Properties properties = new Properties();

        try (FileInputStream fis = new FileInputStream("src/main/resources/kafka-stream.properties")) {
            properties.load(fis);
        }

        String incomeTopic = "stream-income-topic";
        String outTopic = "stream-out-topic";

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> stream = builder.stream(incomeTopic, Consumed.with(Serdes.String(), Serdes.String()));
        stream
                // 改动前打印
                .peek(((key, value) -> System.out.println("key" + key + ", value: " + value)))
                .filter(((key, value) -> key.length() > 100))
                .mapValues(value -> value += "-val")
                // 改动后打印
                .peek(((key, value) -> System.out.println("key" + key + ", value: " + value)))
                .to(outTopic, Produced.with(Serdes.String(), Serdes.String()));
        KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), properties);
        kafkaStreams.start();
    }

    public void createUpdateStream() throws IOException {
        Properties properties = new Properties();

        try (FileInputStream fis = new FileInputStream("src/main/resources/kafka-stream.properties")) {
            properties.load(fis);
        }

        String table = properties.getProperty("kafka.stream.table.topic");
        String outTopic = properties.getProperty("kafka.stream.topic.out");

        StreamsBuilder builder = new StreamsBuilder();
        KTable<String, String> kTable = builder.table(table,
                Consumed.with(Serdes.String(), Serdes.String()),
                Materialized.with(Serdes.String(), Serdes.String()));


        kTable.toStream()
                // 改动前打印
                .peek(((key, value) -> System.out.println("key" + key + ", value: " + value)))
                .filter(((key, value) -> key.length() > 100))
                .mapValues(value -> value += "-val")
                // 改动后打印
                .peek(((key, value) -> System.out.println("key" + key + ", value: " + value)))
                .to(outTopic, Produced.with(Serdes.String(), Serdes.String()));
        KafkaStreams streams = new KafkaStreams(builder.build(), properties);

        streams.start();

        builder.globalTable(table, Materialized.with(Serdes.String(), Serdes.String()));

    }

    public Serde<StreamObject> customSerde() {
        return StreamObjectSerde.serde();
    }

    public void selectKey() {

        StreamsBuilder builder = new StreamsBuilder();
        builder.stream("topic", Consumed.with(Serdes.String(), Serdes.String()))
                .selectKey(($_, v) -> v)
                .print(Printed.<String, String>toSysOut().withLabel("select-key-out"));


    }

    public void forEach() {

        StreamsBuilder builder = new StreamsBuilder();
        builder.stream("topic", Consumed.with(Serdes.String(), Serdes.String()))
                .foreach((k, v) -> {
                    log.debug("{} - {}", k, v);
                });

    }

    public void splitAndMerge() {
        StreamsBuilder builder = new StreamsBuilder();
        Map<String, KStream<String, String>> map = builder.stream("topic", Consumed.with(Serdes.String(), Serdes.String()))
                // 定义后续生成 Map 的 key 前缀
                .split(Named.as("split-prefix-"))
                // 如果包含 String， 则存入 key =  split-prefix-string-stream 的 MAP 中
                .branch((k, v) -> k.contains("string"), Branched.as("string-stream"))
                // 如果包含 Integer， 则存入 key =  split-prefix-Integer-stream 的 MAP 中
                .branch((k, v) -> k.contains("Integer"), Branched.as("Integer-Stream"))
                // 如果包含 Integer， 则存入 key =  split-prefix-Integer-stream 的 MAP 中
                .branch((k, v) -> k.contains("Char"), Branched.as("Char-Stream"))
                // transform
                .branch((k, v) -> k.contains("Char"), Branched.withFunction(ks -> ks.mapValues(d -> d.toUpperCase()), "upper-case-stream"))
                .branch((k, v) -> true, Branched.withConsumer(s -> s.to("true-topic")))
                .defaultBranch(Branched.as("default"));

        map.get("split-prefix-string-stream")
                .merge(map.get("split-prefix-Integer-stream"))
                .print(Printed.<String, String>toSysOut().withLabel("merge-stream"));

    }

}
