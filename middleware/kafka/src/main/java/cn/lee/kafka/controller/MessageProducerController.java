package cn.lee.kafka.controller;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/produce")
@RestController
public class MessageProducerController {


    @Autowired
    private KafkaProducer<String, String> producer;

    @PostMapping
    public void produce(String message){
        ProducerRecord<String, String> record = new ProducerRecord<>("topic", "key", message);
        producer.send(record);
    }

    @PostMapping
    public void produce(String topic, Integer partition, String key, String message) {
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, partition, key, message);
        producer.send(record);
    }
}
