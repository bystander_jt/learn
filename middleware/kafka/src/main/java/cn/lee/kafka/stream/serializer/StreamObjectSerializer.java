package cn.lee.kafka.stream.serializer;

import cn.lee.kafka.stream.StreamObject;
import com.alibaba.fastjson2.JSON;
import org.apache.kafka.common.serialization.Serializer;

class StreamObjectSerializer implements Serializer<StreamObject> {


    @Override
    public byte[] serialize(String topic, StreamObject data) {
        return JSON.toJSONBytes(data);
    }
}
