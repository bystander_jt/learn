package cn.lee.kafka.stream.stateful.join;

import com.alibaba.fastjson2.JSON;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class StreamInnerJoinExample {

    private static final String APP_ID = "STREAM_INNER_JOIN";


    public static void main(String[] args) throws InterruptedException {

        StreamInnerJoinExample example = new StreamInnerJoinExample();
        example.createStreamJoin();

        CountDownLatch latch = new CountDownLatch(1);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            example.stop();
            latch.countDown();
        }));


        example.start();
        latch.await();

    }


    private Properties loadProperties(String filename) {
        Properties properties = new Properties();
        try {
            URL resource = this.getClass().getClassLoader().getResource(filename);
            properties.load(resource.openStream());
        } catch (IOException ignored) {

        }
        return properties;
    }

    @Getter
    @Setter
    @ToString
    static class User {

        private String state;

        private String name;

        public User(String state, String name) {
            this.state = state;
            this.name = name;
        }
    }


    private KafkaStreams lStream;
    private KafkaStreams rStream;


    /**
     * JOIN 操作示范程序.<p>
     * 创建2个 topic, 分别为 kafka-stream-join.properties 中声明的 left-join.input 和 right-join.input.<p>
     * left-join.input 输入数据为: <1, Alex> <p>
     * right-join.input 输入数据为: <1, CN> <p>
     * 数据可以修改, 但是 JOIN 操作时, 在所设定的 {@link JoinWindows} 时间窗口内都有相同 key 数据进入才行.<p>
     * 创建 topic 指令:
     * <ul>
     *     <li> bin/kafka-topics.sh --bootstrap-server <server>:9092 --create --topic <left-join.input> --partitions 3</li>
     *     <li> bin/kafka-topics.sh --bootstrap-server <server>:9092 --create --topic <right-join.input> --partitions 3</li>
     * </ul>
     * 输入测试数据指令:
     *  <ul>
     *      <li> bin/kafka-console-producer.sh --bootstrap-server <server>:9092 --topic <left-join.input></li>
     *  </ul>
     *  控制台示例输出:
     */
    public void createStreamJoin() {

        Properties properties = loadProperties("kafka-stream-join.properties");
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, APP_ID);
        properties.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, APP_ID);


        String leftTopic = properties.getProperty("stream.topic.group.input");
        String rightTopic = properties.getProperty("stream.topic.right-join.input");
        properties.setProperty(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, String.valueOf(100));

        // 左侧 Stream
        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, String> lKStream = builder.stream(
                leftTopic,
                Consumed.with(Serdes.String(), Serdes.String())
                        .withName("left-join-stream-source")
        ).peek(((key, value) -> log.info(leftTopic + " income data: [{}-{}]", key, value)), Named.as("left-join-peek"));

        KStream<String, String> rKStream = builder.stream(
                rightTopic,
                Consumed.with(Serdes.String(), Serdes.String())
                        .withName("right-join-stream-source")
        ).peek(((key, value) -> log.info(rightTopic + " income data: [{}-{}]", key, value)), Named.as("right-join-peek"));


        // JOIN 操作
        lKStream.join(
                rKStream,
                User::new,
                JoinWindows.ofTimeDifferenceWithNoGrace(Duration.ofMinutes(1)),
                // 定义输入的数据类型, 方便 Join 数据入到内部 Topic 的序列化和反序列化.
                StreamJoined.with(Serdes.String(), Serdes.String(), Serdes.String())
        ).peek((key, user) -> log.info("join result: [{},{}]", key, JSON.toJSONString(user)));


        Topology lTopology = builder.build(properties);
        lStream = new KafkaStreams(lTopology, properties);
        log.info("stream join create success.");
    }

    public void start() {
        lStream.start();
        log.info("stream join start success.");

    }

    public void stop() {
        lStream.close();
    }


}
