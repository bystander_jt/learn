#!/bin/bash


bin/kafka-console-producer.sh \
--bootstrap-server 10.0.0.157:9092 \
--topic user.state \
--property parse.key=true \
--property key.separator=","


bin/kafka-console-producer.sh \
--bootstrap-server 10.0.0.157:9092 \
--topic user.info \
--property parse.key=true \
--property key.separator=","