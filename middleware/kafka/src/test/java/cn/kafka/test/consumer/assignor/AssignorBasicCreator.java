package cn.kafka.test.consumer.assignor;

import org.apache.kafka.clients.consumer.ConsumerPartitionAssignor;
import org.apache.kafka.clients.consumer.internals.AbstractPartitionAssignor;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AssignorBasicCreator {

    private int topicCount = 3;
    private List<String> topics;
    private Map<String, Integer> topicsOfPartitionCount;

    @Test
    public void testAssignor() {
        AbstractPartitionAssignor assignor = getAssignor();

        topicCount = topicCount();
        String prefix = topicPrefix();
        int[] eachPartitionOfTopicCount = eachPartitionOfTopicCount(1, 2, 3);

        topics = createTopics(topicCount, prefix);


        topicsOfPartitionCount = createPartitionOfEachTopic(topics, eachPartitionOfTopicCount);
        Map<String, ConsumerPartitionAssignor.Subscription> subscriptions = createSubscription(topicsOfPartitionCount);

        Map<String, List<TopicPartition>> result = assignor.assign(topicsOfPartitionCount, subscriptions);
        output(result);
    }

    public void output(Map<String, List<TopicPartition>> result) {
        result.forEach((k, v) -> {
            String output = String.format("topic: %s, partition assign: %s", k, v.stream().map(TopicPartition::toString).collect(Collectors.joining(" ")));
            System.out.println(output);
        });
    }

    public int topicCount() {
        return 3;
    }

    public String topicPrefix() {
        return "test-prefix";
    }

    public int[] eachPartitionOfTopicCount(int... count) {
        return count;
    }

    abstract AbstractPartitionAssignor getAssignor();


    List<String> createTopics(int topicCount, String topicPrefix) {
        String prefix = topicPrefix + "-";

        List<String> topics = new ArrayList<>(topicCount);
        for (int i = 0; i < topicCount; i++) {
            topics.add(prefix + (i + 1));
        }
        return topics;
    }


    Map<String, Integer> createPartitionOfEachTopic(List<String> topics, int... partitionCount) {

        int numCount = partitionCount.length;

        HashMap<String, Integer> result = new HashMap<>(topics.size());

        int index = 0;
        for (String topic : topics) {
            result.put(topic, getPartitionCount(index++, partitionCount));
        }
        return result;
    }

    private int getPartitionCount(int index, int... partitionCount) {
        if (partitionCount.length == 1) return 0;
        if (partitionCount.length - 1 < index) return partitionCount[partitionCount.length - 1];
        return partitionCount[index];
    }

    Map<String, ConsumerPartitionAssignor.Subscription> createSubscription(Map<String, Integer> topicsOfPartitionCount) {

        Map<String, ConsumerPartitionAssignor.Subscription> result = new HashMap<>(topics.size());
        topicsOfPartitionCount.forEach((topic, count) -> {
            List<TopicPartition> partitions = new ArrayList<>(topics.size());
            while (count > 0) {
                partitions.add(createPartitions(topic, count--));
            }
            ConsumerPartitionAssignor.Subscription s = new ConsumerPartitionAssignor.Subscription(Collections.singletonList(topic), null, partitions);
            result.put(topic, s);
        });
        return result;
    }

    private TopicPartition createPartitions(String topic, int partition) {
        TopicPartition p = new TopicPartition(topic, partition);
        return p;
    }

}
