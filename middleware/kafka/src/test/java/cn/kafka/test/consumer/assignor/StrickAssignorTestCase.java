package cn.kafka.test.consumer.assignor;

import org.apache.kafka.clients.consumer.CooperativeStickyAssignor;
import org.apache.kafka.clients.consumer.RoundRobinAssignor;
import org.apache.kafka.clients.consumer.internals.AbstractPartitionAssignor;


public class StrickAssignorTestCase extends AssignorBasicCreator {


    @Override
    public int topicCount() {
        return 3;
    }

    @Override
    AbstractPartitionAssignor getAssignor() {
        return new CooperativeStickyAssignor();
    }
}
