package cn.kafka.test.consumer.assignor;

import org.apache.kafka.clients.consumer.RoundRobinAssignor;
import org.apache.kafka.clients.consumer.internals.AbstractPartitionAssignor;


public class RoundRobinAssignorTestCase extends AssignorBasicCreator {


    @Override
    public int topicCount() {
        return 3;
    }

    @Override
    AbstractPartitionAssignor getAssignor() {
        return new RoundRobinAssignor();
    }
}
