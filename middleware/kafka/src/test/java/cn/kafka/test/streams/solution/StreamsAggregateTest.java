package cn.kafka.test.streams.solution;

import cn.lee.kafka.stream.StreamsUtils;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StreamsAggregateTest {

    @Test
    public void shouldAggregateRecords() {

        final Properties streamsProps = new Properties();
        streamsProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "aggregate-test");
        streamsProps.put("schema.registry.url", "mock://aggregation-test");

        final String inputTopicName = "input";
        final String outputTopicName = "output";
        final Map<String, Object> configMap =
                StreamsUtils.propertiesToMap(streamsProps);

        final Serde<String> stringSerde = Serdes.String();
        final Serde<Double> doubleSerde = Serdes.Double();

        final StreamsBuilder builder = new StreamsBuilder();
        final KStream<String, String> electronicStream =
                builder.stream(inputTopicName, Consumed.with(Serdes.String(), Serdes.String()));

        electronicStream.groupByKey().aggregate(() -> 0.0,
                        (key, order, total) -> total + order.length(),
                        Materialized.with(stringSerde, doubleSerde))
                .toStream().to(outputTopicName, Produced.with(Serdes.String(), Serdes.Double()));

        try (final TopologyTestDriver testDriver = new TopologyTestDriver(builder.build(), streamsProps)) {
            final TestInputTopic<String, String> inputTopic =
                    testDriver.createInputTopic(inputTopicName,
                            stringSerde.serializer(),
                            stringSerde.serializer());
            final TestOutputTopic<String, Double> outputTopic =
                    testDriver.createOutputTopic(outputTopicName,
                            stringSerde.deserializer(),
                            doubleSerde.deserializer());

            final List<String> orders = new ArrayList<>();
            orders.add("312");
            orders.add("qwe");
            orders.add("adsf");

            List<Double> expectedValues = Stream.of(5.0, 20.0, 45.0).collect(Collectors.toList());
            orders.forEach(order -> inputTopic.pipeInput(order, order));
            List<Double> actualValues = outputTopic.readValuesToList();
            assertEquals(expectedValues, actualValues);
        }

    }

}
