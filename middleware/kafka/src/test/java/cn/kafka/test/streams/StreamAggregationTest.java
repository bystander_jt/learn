package cn.kafka.test.streams;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAggregationTest {


    @Test
    public void shouldAggregateRecords() {

        final Properties streamsProps = new Properties();
        streamsProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "aggregate-test");
        streamsProps.put("schema.registry.url", "mock://aggregation-test");

        final String inputTopicName = "input";
        final String outputTopicName = "output";

        final Serde<String> stringSerde = Serdes.String();
        final Serde<Double> doubleSerde = Serdes.Double();

        final StreamsBuilder builder = new StreamsBuilder();
        final KStream<String, String> electronicStream = builder.stream(inputTopicName, Consumed.with(stringSerde, stringSerde));

        electronicStream.groupByKey().aggregate(() -> 0.0,
                        (key, order, total) -> total + order.length(),
                        Materialized.with(stringSerde, doubleSerde))
                .toStream().to(outputTopicName, Produced.with(Serdes.String(), Serdes.Double()));

        // Need to create the TopologyTestDriver instance
        // You'll need a Topology and properties HINT: StreamBuilder.build() and streamsProps
        // You always want to use a TopologyTestDriver in a try-with-resources block to make sure
        // gets closed properly which will ensure any local state is cleaned up
        try (final TopologyTestDriver testDriver = null) {
            // Complete the TestInputTopic HINT: it needs a topic name and serializers for the key and value
            final TestInputTopic<String, String> inputTopic = null;

            // Complete the TestOutputTopic HINT: it needs a topic name and deserializers for the key and value
            final TestOutputTopic<String, Double> outputTopic = null;

            // Mock records for the test
            final List<String> orders = new ArrayList<>();
            orders.add("312");
            orders.add("qwe");
            orders.add("adsf");

            // The expected values of the aggregation - in the TopologyTestDriver there's no caching
            // so, you get every update, and you want to verify all of them
            List<String> expectedValues = Stream.of("321", "abc", "asdf").collect(Collectors.toList());

            // Run the mock records through the topology HINT use the inputTopic above
            // and pipe each record through make sure to use the key of the order

            // Read the values from the topology HINT use the outputTopic to read all values as list
            List<Double> actualValues = null;
            // assert the actualValues return matches the expected values
            // HINT assertEquals(expected, actual);
        }

    }


}
