package cn.kafka.test;

public class ThreadInterruptedTest {


    public static void main(String[] args) throws InterruptedException {

        Thread t = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            try {
                while (true) {
                    System.out.println("sleep");
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                boolean isInterrupted = Thread.currentThread().isInterrupted();
                System.out.println("current thread " + threadName + " interrupt status is " + isInterrupted);

                Thread.currentThread().interrupt();
                isInterrupted = Thread.currentThread().isInterrupted();
                System.out.println("recheck current thread " + threadName + " interrupt status is " + isInterrupted);

            }

        });

        t.start();

        Thread.sleep(1500);

        t.interrupt();
        System.out.println("t thread status: " + t.isInterrupted());

    }


}
