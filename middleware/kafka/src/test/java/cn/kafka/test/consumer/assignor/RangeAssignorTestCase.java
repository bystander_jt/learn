package cn.kafka.test.consumer.assignor;

import org.apache.kafka.clients.consumer.RangeAssignor;
import org.apache.kafka.clients.consumer.internals.AbstractPartitionAssignor;


public class RangeAssignorTestCase extends AssignorBasicCreator {


    @Override
    public int topicCount() {
        return 3;
    }

    @Override
    AbstractPartitionAssignor getAssignor() {
        return new RangeAssignor();
    }
}
