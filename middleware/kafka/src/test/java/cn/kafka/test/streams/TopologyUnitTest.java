package cn.kafka.test.streams;

import cn.lee.kafka.stream.dsl.CustomProcessorAPI;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.TestRecord;
import org.junit.jupiter.api.Test;

import java.time.Duration;

/**
 * Kafka 自定义测试单元
 */
public class TopologyUnitTest {

    @Test
    public void unitTest() {

        String stateStoreName = "redis";

        Topology topology;
        topology = usingProcessor(stateStoreName);
        topology = usingDSL(stateStoreName);


        // create test driver
        TopologyTestDriver testDriver = new TopologyTestDriver(topology);


        TestInputTopic<String, String> inputs =
                testDriver.createInputTopic("topic", Serdes.String().serializer(), Serdes.String().serializer());

        // pipeInput 可以触发 Stream-Time punctuation. 不提供基于时钟的 punctuation
        inputs.pipeInput("123");
        inputs.pipeInput("key", "value");
        inputs.pipeInput(new TestRecord<String, String>("test-key", "test-value"));
        testDriver.advanceWallClockTime(Duration.ofMillis(30));
    }

    public Topology usingProcessor(String stateStoreName) {
        Topology topology = new Topology();
        topology.addSource("sourceProcessor", "input-topic");
        topology.addProcessor("processor", new CustomProcessorAPI(stateStoreName), "sourceProcessor");
        topology.addSink("sinkProcessor", "output-topic", "processor");
        return topology;
    }

    /**
     * DSL (Domain Specific Language) 也可以说是 Stream.
     *
     * @param stateStoreName
     * @return
     */
    public Topology usingDSL(String stateStoreName) {

        StreamsBuilder builder = new StreamsBuilder();
        builder.stream("input-topic")
                // DO STH

                .to("output-topic");


        return builder.build();
    }
}
