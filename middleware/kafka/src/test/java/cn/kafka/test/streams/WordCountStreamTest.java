package cn.kafka.test.streams;

import cn.lee.kafka.stream.stateful.WordCount;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.errors.UnknownStateStoreException;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Named;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;


@Slf4j
public class WordCountStreamTest {


    private static final String[] inputs = new String[]{"hello kafka", "hello spark", "hello scale"};

    @Test
    public void wordCountTest() {

        WordCount count = new WordCount();
        Properties properties = count.loadProperties();

        String sourceTopic = properties.getProperty("stream.topic.income");
        String outputTopic = properties.getProperty("stream.topic.output");

        StoreBuilder<KeyValueStore<String, Integer>> stateStoreBuilder = count.stateStore(properties);
        String stateStoreName = stateStoreBuilder.name();


        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> kStream = count.createKStream(builder, sourceTopic);


        kStream.flatMap((k, v) -> Arrays.stream(v.split("\\s+"))
                        .map(d -> KeyValue.pair(d, d))
                        .collect(Collectors.toList())
                ).peek((key, value) -> log.debug("key: {}, value: {}", key, value), Named.as("before-transform-peek"))
                .transform(() -> new Transformer<String, String, KeyValue<String, Integer>>() {
                    KeyValueStore<String, Integer> stateStore = null;

                    @Override
                    public void init(ProcessorContext context) {
                        this.stateStore = context.getStateStore(stateStoreName);
                        if (stateStore == null)
                            throw new UnknownStateStoreException("can not find [" + stateStoreName + "] state store");
                    }

                    @Override
                    public KeyValue<String, Integer> transform(String key, String value) {
                        Integer count = stateStore.get(key);
                        if (count == null) count = 0;
                        ++count;

                        stateStore.put(key, count);

                        return KeyValue.pair(key, count);

                    }

                    @Override
                    public void close() {
                        stateStore.close();
                    }
                }, Named.as("transform-processor"))
                .peek(((key, value) -> log.debug("{} count: {}", key, value)), Named.as("after-transform-processor"))
                .to(outputTopic, Produced.with(Serdes.String(), Serdes.Integer()));

        builder.addStateStore(stateStoreBuilder);
        Topology topology = count.createTopology(builder, properties);
        topology.addStateStore(stateStoreBuilder, "transform-processor");

        TopologyTestDriver driver = new TopologyTestDriver(topology, properties);

        Serde<String> stringSerde = Serdes.String();


        TestInputTopic<String, String> input = driver.createInputTopic(sourceTopic, stringSerde.serializer(), stringSerde.serializer());
        TestOutputTopic<String, Integer> output = driver.createOutputTopic(outputTopic, stringSerde.deserializer(), Serdes.Integer().deserializer());

        Set<String> topics = driver.producedTopicNames();
        input.advanceTime(Duration.ofMillis(1));
        for (String in : inputs) {
            input.pipeInput(in);
        }


    }


}
