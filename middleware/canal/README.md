# Canal

`canal` 是纯 `Java` 开发。基于数据库增量日志解析，提供增量数据订阅&消费，目前主要支持了 `MySQL`

![img.png](img/img.png)

> `canal` 模拟 MySQL slave 的交互协议，伪装自己为 MySQL slave ，向 MySQL master 发送dump 协议

## `canal` 服务
在 `canal` 项目仓库的 [release](https://github.com/alibaba/canal/releases) 中会发现有好几包.

- `canal.adapter` : 用于适配其他客户端, 类似 `ES` `Hbase` 等.
- `canal.admin` : `canal` 的 Web-UI 管理界面, 通过界面对 `canal-deployer` 的监听进行配置.
- `canal.deployer` : 部署与服务器, 用于监听 `MySQL` 的 `binlog` 日志服务.
- `canal.example` : 相关示例代码






## 部署
`canal` 由 `server` + `client` 模式组成.

- `server` : 部署在服务器中用于监听绑定的数据库 `binlog` 日志
- `client` : 用于在业务中监听来自 `server` 端上报的 `binlog` 信息, 然后根据业务进行处理.

### `MySQL` 配置
在使用 `canal` 时我们需要将 `MySQL` 的 `binlog` 开启, 并将记录模式 `binlog_format` 更改为 `row` 模式.

```properties
[mysqld]

# 开启 binlog
log-bin=mysql-bin

# 选择 ROW 模式
binlog-format=ROW

# 配置 MySQL replicate 需要定义，不要和 canal 的 slaveId 重复
server_id=1

```
> 在此将不会讨论如何安装 `MySQL`, 关于如何安装 `MySQL` 请移步[搜索引擎]("https://bing.com")

创建 `canal` 用户, 并赋予部分权限.

```sql
CREATE USER 'canal'@'%' identified by '<password>' PASSWORD EXPIRE NEVER;
GRANT SELECT, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'canal'@'%';
FLUSH PRIVILEGES;
```




### 部署 `canal-deployer`
将 `canal.deployer-x.x.x.tar.gz` 下载至服务器.

自动下载解压脚本
```bash
# 填写你要安装的版本
_CANAL_VERSION=1.1.6

# 链接为 Github 代理下载.
curl -Lso "https://download.yzuu.cf/alibaba/canal/releases/download/canal-${_CANAL_VERSION}/canal.deployer-${_CANAL_VERSION}.tar.gz"

gzip -d *.tar.gz

mkdir -p canal_${_CANAL_VERSION}_deployer
tar -xf canal.deployer-${_CANAL_VERSION}.tar -C canal_${_CANAL_VERSION}_deployer

echo "download and unzip completed!"

```

下载完成后会有以下目录, 我们需要先对 `/conf` 目录中的配置进行修改.
```bash
[root@localhost canal]# ls -lha canal_${_CANAL_VERSION}_deployer
total 4.0K
drwxr-xr-x. 7 root root   66 Jan  4 15:10 .
dr-xr-x---. 5 root root  278 Jan  4 18:36 ..
drwxr-xr-x. 2 root root   93 Jan  5 09:32 bin
drwxr-xr-x. 7 root root  150 Jan  4 18:39 conf
drwxr-xr-x. 2 root root 4.0K Jan  4 15:10 lib
drwxrwxrwx. 5 root root   46 Jan  4 18:38 logs
drwxrwxrwx. 2 root root  271 Jan  4 14:00 plugin


[root@localhost canal]# ls conf/ -lah
total 20K
drwxr-xr-x. 7 root root  150 Jan  4 18:39 .
drwxr-xr-x. 7 root root   66 Jan  4 15:10 ..
-rwxrwxrwx. 1 root root  319 Nov 16 10:25 canal_local.properties
-rwxrwxrwx. 1 root root 6.6K Nov 16 10:25 canal.properties
drwxr-xr-x. 2 root root   38 Jan  4 16:48 default
drwxrwxrwx. 2 root root   33 Jan  4 18:36 example
-rwxrwxrwx. 1 root root 4.4K Nov 16 10:25 logback.xml
drwxrwxrwx. 2 root root   39 Jan  4 15:10 metrics
drwxrwxrwx. 3 root root  149 Jan  4 15:10 spring
drwxr-xr-x. 2 root root   38 Jan  4 18:39 test
```

- `canal_local.properties`

    简要配置, 使用此配置则需要安装 `canal-admin` UI 对 `deployer` 进行配置. 在该配置中只填写了最简单的 `canal-admin` 的连接信息.

    所有配置信息都将由 `canal-admin` 通过页面的方式进行填写.

    > 使用此配置则需要在使用启动脚本额外增加配置
    >
    > ```bash
    > sh bin/startup.sh local
    > ```

    ```bash
    [root@localhost canal]# cat conf/canal_local.properties
    # register ip
    canal.register.ip =
    
    # canal admin 配置信息
    # canal admin 管理地址
    canal.admin.manager = 127.0.0.1:8089
    # 用于给 admin 托管控制的端口
    canal.admin.port = 11110
    # canal admin 用户信息
    canal.admin.user = admin
    # 默认密码 123456
    canal.admin.passwd = 4ACFE3202A5FF5CF467898FC58AAB1D615029441
    
    
    # admin auto register
    canal.admin.register.auto = true
    canal.admin.register.cluster =
    canal.admin.register.name =
    ```

- `canal.properties` 

    本地配置, 相比较上方的 `canal_local.properties` 配置文件, 首次填写的信息要更多 (实际相同, 只是服务启动时)

    ```properties
    #############################################################
    #########               common argument         #############
    #############################################################
    # tcp bind ip
    canal.ip =
    
    # register ip to zookeeper
    canal.register.ip =
    canal.port = 11111
    canal.metrics.pull.port = 11112
    # canal instance user/passwd
    # canal.user = canal
    # canal.passwd = E3619321C1A937C46A0D8BD1DAC39F93B27D4458
    
    # canal admin 配置信息
    #canal.admin.manager = 127.0.0.1:8089
    canal.admin.port = 11110
    canal.admin.user = admin
    canal.admin.passwd = 4ACFE3202A5FF5CF467898FC58AAB1D615029441
    # admin auto register
    #canal.admin.register.auto = true
    #canal.admin.register.cluster =
    #canal.admin.register.name =
    
    canal.zkServers =
    # flush data to zk
    canal.zookeeper.flush.period = 1000
    canal.withoutNetty = false
    
    # 传输方式, 选择非 TCP 则需要到指定的配置区域补全配置
    # tcp, kafka, rocketMQ, rabbitMQ, pulsarMQ
    canal.serverMode = tcp
    # flush meta cursor/parse position to file
    canal.file.data.dir = ${canal.conf.dir}
    canal.file.flush.period = 1000
    ## memory store RingBuffer size, should be Math.pow(2,n)
    canal.instance.memory.buffer.size = 16384
    ## memory store RingBuffer used memory unit size , default 1kb
    canal.instance.memory.buffer.memunit = 1024
    ## meory store gets mode used MEMSIZE or ITEMSIZE
    canal.instance.memory.batch.mode = MEMSIZE
    canal.instance.memory.rawEntry = true
    
    ## detecing config
    canal.instance.detecting.enable = false
    #canal.instance.detecting.sql = insert into retl.xdual values(1,now()) on duplicate key update x=now()
    canal.instance.detecting.sql = select 1
    canal.instance.detecting.interval.time = 3
    canal.instance.detecting.retry.threshold = 3
    canal.instance.detecting.heartbeatHaEnable = false
    
    # support maximum transaction size, more than the size of the transaction will be cut into multiple transactions delivery
    canal.instance.transaction.size =  1024
    # mysql fallback connected to new master should fallback times
    canal.instance.fallbackIntervalInSeconds = 60
    
    # network config
    canal.instance.network.receiveBufferSize = 16384
    canal.instance.network.sendBufferSize = 16384
    canal.instance.network.soTimeout = 30
    
    # binlog filter config
    canal.instance.filter.druid.ddl = true
    canal.instance.filter.query.dcl = false
    canal.instance.filter.query.dml = false
    canal.instance.filter.query.ddl = false
    canal.instance.filter.table.error = false
    canal.instance.filter.rows = false
    canal.instance.filter.transaction.entry = false
    canal.instance.filter.dml.insert = false
    canal.instance.filter.dml.update = false
    canal.instance.filter.dml.delete = false
    
    # binlog format/image check
    canal.instance.binlog.format = ROW,STATEMENT,MIXED
    canal.instance.binlog.image = FULL,MINIMAL,NOBLOB
    
    # binlog ddl isolation
    canal.instance.get.ddl.isolation = false
    
    # parallel parser config
    canal.instance.parser.parallel = true
    ## concurrent thread number, default 60% available processors, suggest not to exceed Runtime.getRuntime().availableProcessors()
    #canal.instance.parser.parallelThreadSize = 16
    ## disruptor ringbuffer size, must be power of 2
    canal.instance.parser.parallelBufferSize = 256
    
    # table meta tsdb info
    canal.instance.tsdb.enable = true
    canal.instance.tsdb.dir = ${canal.file.data.dir:../conf}/${canal.instance.destination:}
    canal.instance.tsdb.url = jdbc:h2:${canal.instance.tsdb.dir}/h2;CACHE_SIZE=1000;MODE=MYSQL;
    canal.instance.tsdb.dbUsername = canal
    canal.instance.tsdb.dbPassword = canal
    # dump snapshot interval, default 24 hour
    canal.instance.tsdb.snapshot.interval = 24
    # purge snapshot expire , default 360 hour(15 days)
    canal.instance.tsdb.snapshot.expire = 360
    
    #############################################################
    #########               destinations            #############
    #############################################################
    # 这里用于定义传输目标, 传输目标可以选择多个 a1,a2,a3. 这里的传输目标有多个, 那在 conf/ 目录下就要有多个 {destination}/instance.properties 
    # 如果是使用 canal-admin 管理, 则选择左侧导航最后一个, 添加并绑定.
    # 如果选择的 RabbitMQ, 那 exchange 的 routingKey 就是当前名称!
    canal.destinations = example
    # conf root dir
    canal.conf.dir = ../conf
    # auto scan instance dir add/remove and start/stop instance
    canal.auto.scan = true
    canal.auto.scan.interval = 5
    # set this value to 'true' means that when binlog pos not found, skip to latest.
    # WARN: pls keep 'false' in production env, or if you know what you want.
    canal.auto.reset.latest.pos.mode = false
    
    canal.instance.tsdb.spring.xml = classpath:spring/tsdb/h2-tsdb.xml
    #canal.instance.tsdb.spring.xml = classpath:spring/tsdb/mysql-tsdb.xml
    
    canal.instance.global.mode = spring
    canal.instance.global.lazy = false
    canal.instance.global.manager.address = ${canal.admin.manager}
    #canal.instance.global.spring.xml = classpath:spring/memory-instance.xml
    canal.instance.global.spring.xml = classpath:spring/file-instance.xml
    #canal.instance.global.spring.xml = classpath:spring/default-instance.xml
    
    ######################################################
    #########             MQ Properties      #############
    ######################################################
    # aliyun ak/sk , support rds/mq
    canal.aliyun.accessKey =
    canal.aliyun.secretKey =
    canal.aliyun.uid=
    
    canal.mq.flatMessage = true
    canal.mq.canalBatchSize = 50
    canal.mq.canalGetTimeout = 100
    # Set this value to "cloud", if you want open message trace feature in aliyun.
    canal.mq.accessChannel = local
    
    canal.mq.database.hash = true
    canal.mq.send.thread.size = 30
    canal.mq.build.thread.size = 8
    
    ##################################################
    #########                    Kafka                   #############
    ##################################################
    kafka.bootstrap.servers = 127.0.0.1:9092
    kafka.acks = all
    kafka.compression.type = none
    kafka.batch.size = 16384
    kafka.linger.ms = 1
    kafka.max.request.size = 1048576
    kafka.buffer.memory = 33554432
    kafka.max.in.flight.requests.per.connection = 1
    kafka.retries = 0
    
    kafka.kerberos.enable = false
    kafka.kerberos.krb5.file = ../conf/kerberos/krb5.conf
    kafka.kerberos.jaas.file = ../conf/kerberos/jaas.conf
    
    # sasl demo
    # kafka.sasl.jaas.config = org.apache.kafka.common.security.scram.ScramLoginModule required \\n username=\"alice\" \\npassword="alice-secret\";
    # kafka.sasl.mechanism = SCRAM-SHA-512
    # kafka.security.protocol = SASL_PLAINTEXT
    
    ##################################################
    #########                   RocketMQ         #############
    ##################################################
    rocketmq.producer.group = test
    rocketmq.enable.message.trace = false
    rocketmq.customized.trace.topic =
    rocketmq.namespace =
    rocketmq.namesrv.addr = 127.0.0.1:9876
    rocketmq.retry.times.when.send.failed = 0
    rocketmq.vip.channel.enabled = false
    rocketmq.tag =
    
    ##################################################
    #########                   RabbitMQ         #############
    ##################################################
    rabbitmq.host =
    rabbitmq.virtual.host =
    rabbitmq.exchange =
    rabbitmq.username =
    rabbitmq.password =
    rabbitmq.deliveryMode = persistent
    
    
    ##################################################
    #########                     Pulsar         #############
    ##################################################
    pulsarmq.serverUrl =
    pulsarmq.roleToken =
    pulsarmq.topicTenantPrefix =
    ```

    > 如果选择 `RabbitMQ` 那 `queue` 和 `routingKey` 则都是 `canal.destinations` 的配置信息中的 `canal.mq.topic` 值.
  > ```java
  > private void send(MQDestination canalDestination, String topicName, Message messageSub) {
  >     if (!mqProperties.isFlatMessage()) {
  >       	// BA LA BA LA
  >         sendMessage(topicName, message);
  >     } else {
  >         // CODE
  >         for (FlatMessage flatMessage : flatMessages) {
  >             // CODE
  >             sendMessage(topicName, message);
  >         }
  >     }
  > 
  > }
  > 
  > private void sendMessage(String queueName, byte[] message) {
  >     // tips: 目前逻辑中暂不处理对exchange处理，请在Console后台绑定 才可使用routekey
  >     try {
  >         RabbitMQProducerConfig rabbitMQProperties = (RabbitMQProducerConfig) this.mqProperties;
  >         channel.basicPublish(rabbitMQProperties.getExchange(), queueName, null, message);
  >     } catch (Throwable e) {
  >         throw new RuntimeException(e);
  >     }
  > }
  
  

