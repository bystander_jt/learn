package cn.lee.middleware;

import com.alibaba.otter.canal.client.rabbitmq.RabbitMQCanalConnector;
import com.alibaba.otter.canal.protocol.Message;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static cn.lee.middleware.CanalSupportUtils.printEntry;

public class CanalRabbitMQConsumer {

    public static void main(String args[]) {

        RabbitMQCanalConnector connector = new RabbitMQCanalConnector("ip or host",
                "canal", "canal",
                "", "",
                "", "",
                null, false);

        try {

            connector.connect();
            connector.subscribe(".*\\..*");
            while (true) {
                List<Message> messages = connector.getList(10L, TimeUnit.SECONDS);
                if (CollectionUtils.isEmpty(messages)) {
                    System.out.println("message is empty !");
                    continue;
                }
                for (Message message : messages) {
                    printEntry(message.getEntries());
                    connector.ack(message.getId());
                }

            }

        } finally {
            connector.disconnect();
        }
    }


}